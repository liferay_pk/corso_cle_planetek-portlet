

<%@ include file="/html/libro/init.jsp" %>


<portlet:renderURL var="backURL" windowState="<%=WindowState.NORMAL.toString()%>">
	<portlet:param name="mvcPath" value="/html/libro/view.jsp"/>
</portlet:renderURL>

<%
Libro entry = (Libro) request.getAttribute(WebKeys.LIBRO_DEFINITION);

long entryId = 0;

if (entry != null ) {
	entryId = entry.getLibroId();
}

%>

<liferay-ui:success key="the-selected-entry-has-been-updated" message="the-selected-entry-has-been-updated" />

<liferay-ui:header
	backURL="<%= backURL %>"
	title='<%= (entry != null) ? entry.getNome() : "new-entry" %>'
/>

<portlet:actionURL var="editLibroEntryURL" name="insertUpdateEntry"/>

<aui:form action="<%= editLibroEntryURL %>" method="post" name="fm" onSubmit='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "saveCurrencyInstance();" %>' >
	<aui:input name="<%= Constants.CMD %>" type="hidden" />
	<aui:input name="entryId" type="hidden" value="<%= entryId %>" />
		
	<aui:model-context bean="<%= entry %>" model="<%= Libro.class %>" />

	<aui:fieldset>
	
		<c:if test="<%= (entry != null) %>">
			<aui:field-wrapper label="id">
				<%= entryId %>
			</aui:field-wrapper>
		</c:if>

		<aui:input name="nome" />
		<aui:input name="descrizione" />
		<aui:input name="pubblicato" />
		<aui:input name="numeroPagine" />
		<aui:input name="dataPubblicazione" />
		<aui:input name="isbn" />

		<c:if test="<%= entry == null %>">
			<aui:field-wrapper label="permissions">
				<liferay-ui:input-permissions
					modelName="<%= Libro.class.getName() %>"
				/>
			</aui:field-wrapper>
		</c:if>

		<!--  Custom fields -->
		<h3>
			<liferay-ui:message key="custom-fields" />
		</h3>
		<aui:fieldset>
			<liferay-ui:custom-attribute-list
				className="it.planetek.liferay.service.builder.model.Libro"
				classPK="<%=(entry != null) ? entry.getPrimaryKey() : 0%>"
				editable="<%=true%>" label="<%=true%>" />
		</aui:fieldset>

		<h3>Tag & Category</h3>
        <!--  Assets -->
        <aui:input name="assetTags" type="assetTags"/>
        <aui:input name="assetCategories" type="assetCategories"/>


		<aui:button-row>
			<aui:button type="submit" />			
			<aui:button href="<%=backURL%>" name="cancelButton" type="cancel" />
		</aui:button-row>
	</aui:fieldset>
</aui:form>

<c:if test="<%= windowState.equals(WindowState.MAXIMIZED) %>">
	<aui:script>
		Liferay.Util.focusFormField(document.<portlet:namespace />fm.<portlet:namespace />nome);
	</aui:script>
</c:if>

<aui:script>
function <portlet:namespace />saveCurrencyInstance(){
	document.<portlet:namespace />fm.<portlet:namespace /><%= Constants.CMD %>.value = "<%= (entry == null) ? Constants.ADD : Constants.UPDATE %>";

	submitForm(document.<portlet:namespace />fm);
}
</aui:script>
