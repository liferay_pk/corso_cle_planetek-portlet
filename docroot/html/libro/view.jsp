<%@ include file="/html/libro/init.jsp" %>

<%@page import="com.liferay.portal.kernel.util.DateFormatFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.LocaleUtil"%>
<%@page import="it.planetek.liferay.service.builder.service.LibroLocalServiceUtil"%>

<%@page import="com.liferay.portal.kernel.search.Sort"%>
<%@page import="com.liferay.util.portlet.PortletProps"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.search.SortFactoryUtil" %>

<%@taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@taglib uri="http://liferay.com/tld/security" prefix="liferay-security" %>
<%@taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%
PortletURL portletURL = renderResponse.createRenderURL();

%>

<liferay-ui:error exception="<%= NoSuchLibroException.class %>" message="the-requested-resource-was-not-found" />
<liferay-ui:success key="the-selected-entry-has-been-updated" message="the-selected-entry-has-been-updated" />
<liferay-ui:success key="the-selected-entry-has-been-deleted" message="the-selected-entry-has-been-deleted" />


<h1><%= PropsUtil.MY_CUSTOM_PROPERTY  %></h1>
<portlet:renderURL var="searchURL">
  <portlet:param name="mvcPath" value="/html/libro/view.jsp" />
</portlet:renderURL>
 
<aui:form action="<%= searchURL.toString() %>" method="post" name="fm">
 <liferay-ui:search-container emptyResultsMessage="no-results-found">

		<aui:nav-bar>
			<aui:nav>
			<c:if test="<%=ModulePermission.contains(permissionChecker, themeDisplay.getScopeGroupId(), ActionKeys.ADD_ENTRY) %>">
			
				<portlet:actionURL name="editEntry" var="addEntryURL"
					windowState="<%=WindowState.MAXIMIZED.toString()%>" />
				<aui:nav-item href="<%=addEntryURL%>" iconCssClass="icon-plus"
					label="add-entry" />	
			</c:if>
			</aui:nav>
		</aui:nav-bar>

		<liferay-ui:search-container-results
		results="<%=LibroLocalServiceUtil.findByGroup(scopeGroupId,
							searchContainer.getStart(),
							searchContainer.getEnd())%>"
		total="<%= LibroLocalServiceUtil.countByGroup(scopeGroupId) %>"
	/>

	<liferay-ui:search-container-row
		className="it.planetek.liferay.service.builder.model.Libro"
		modelVar="aLibro"
	>	
		<liferay-ui:search-container-column-text property="nome" />
		<liferay-ui:search-container-column-text property="numeroPagine" name="numero-pagine"/>
		<liferay-ui:search-container-column-date property="dataPubblicazione" name="data-pubblicazione"/>
		<liferay-ui:search-container-column-text property="isbn" />
		<liferay-ui:search-container-column-text name="note" buffer="buffer">
			<% buffer.append(aLibro.getExpandoBridge().getAttribute("note")); %>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="data-pubblicazione" 
			value="<%= DateFormatFactoryUtil.getDate(locale).format(aLibro.getDataPubblicazione()) %>"/>
        <!--liferay-ui:search-container-column-status name="status" /-->

		<liferay-ui:search-container-column-jsp align="right" path="/html/libro/entry_action.jsp" />

		</liferay-ui:search-container-row>

	<liferay-ui:search-iterator />
</liferay-ui:search-container>
</aui:form>
