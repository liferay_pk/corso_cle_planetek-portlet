<%@ include file="/html/init.jsp" %>

<%@ page import="com.liferay.portal.security.permission.ActionKeys"%>
<%@ page import="com.liferay.portal.kernel.search.Indexer"%>
<%@ page import="com.liferay.portal.kernel.search.IndexerRegistryUtil" %>

<%@ page import="com.liferay.portal.kernel.search.SearchContext" %>
<%@ page import="com.liferay.portal.kernel.search.SearchContextFactory" %>
<%@ page import="com.liferay.portal.kernel.search.Hits" %>
<%@ page import="com.liferay.portal.kernel.search.Document" %>
<%@ page import="com.liferay.portal.kernel.log.Log" %>
<%@ page import="com.liferay.portal.kernel.log.LogFactoryUtil" %>
<%@ page import="com.liferay.portal.kernel.search.Field" %>
<%@ page import="com.liferay.portal.kernel.search.DocumentImpl" %>

<%@ page import="it.planetek.liferay.util.WebKeys"%>
<%@page import="it.planetek.liferay.service.builder.model.Libro"%>
<%@page import="it.planetek.liferay.service.builder.service.permission.ModulePermission"%>
<%@page import="it.planetek.liferay.service.builder.service.permission.LibroPermission"%>
<%@page import="it.planetek.liferay.service.builder.NoSuchLibroException"%>
<%@page import="it.planetek.liferay.util.PropsUtil"%>
