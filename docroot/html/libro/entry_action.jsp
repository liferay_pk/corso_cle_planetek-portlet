<%@ include file="/html/libro/init.jsp" %>

<%
ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
Libro entry = (Libro) row.getObject();
%>

<liferay-ui:icon-menu>
<c:if test="<%= LibroPermission.contains(permissionChecker, entry, ActionKeys.UPDATE) %>">
    <liferay-portlet:actionURL var="editLibroURL" name="editLibro" windowState="<%= WindowState.MAXIMIZED.toString() %>">
    	<portlet:param name="entryId" value="<%= String.valueOf(entry.getLibroId()) %>" />
    </liferay-portlet:actionURL>
   
	<liferay-ui:icon
		image="edit"
		method="get"
		url="<%= editLibroURL %>"
	/>
</c:if>

<c:if test="<%= LibroPermission.contains(permissionChecker, entry, ActionKeys.PERMISSIONS) %>">
		<liferay-security:permissionsURL
			modelResource="<%= Libro.class.getName() %>"
			modelResourceDescription="<%= entry.getNome() %>"
			resourcePrimKey="<%= String.valueOf(entry.getLibroId()) %>"
			var="permissionsURL"
			windowState="<%= LiferayWindowState.POP_UP.toString() %>"
		/>

		<liferay-ui:icon
			image="permissions"
			method="get"
			url="<%= permissionsURL %>"
		/>
</c:if>

<c:if test="<%=LibroPermission.contains(permissionChecker, entry, ActionKeys.DELETE) %>">
	<liferay-portlet:actionURL name="deleteEntry" var="deleteURL">
		<portlet:param name="entryId" value="<%= String.valueOf(entry.getLibroId()) %>" />
	</liferay-portlet:actionURL>

	<liferay-ui:icon-delete
		url="<%= deleteURL %>"
	/>
</c:if>	
</liferay-ui:icon-menu>
