/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.planetek.liferay.service.builder.model.Libreria;

import java.util.List;

/**
 * The persistence utility for the libreria service. This utility wraps {@link LibreriaPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author jfrancia
 * @see LibreriaPersistence
 * @see LibreriaPersistenceImpl
 * @generated
 */
public class LibreriaUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Libreria libreria) {
		getPersistence().clearCache(libreria);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Libreria> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Libreria> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Libreria> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Libreria update(Libreria libreria) throws SystemException {
		return getPersistence().update(libreria);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Libreria update(Libreria libreria,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(libreria, serviceContext);
	}

	/**
	* Returns all the librerias where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.planetek.liferay.service.builder.model.Libreria> findByGroup(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByGroup(groupId);
	}

	/**
	* Returns a range of all the librerias where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of librerias
	* @param end the upper bound of the range of librerias (not inclusive)
	* @return the range of matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.planetek.liferay.service.builder.model.Libreria> findByGroup(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByGroup(groupId, start, end);
	}

	/**
	* Returns an ordered range of all the librerias where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of librerias
	* @param end the upper bound of the range of librerias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.planetek.liferay.service.builder.model.Libreria> findByGroup(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByGroup(groupId, start, end, orderByComparator);
	}

	/**
	* Returns the first libreria in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching libreria
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libreria findByGroup_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException {
		return getPersistence().findByGroup_First(groupId, orderByComparator);
	}

	/**
	* Returns the first libreria in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching libreria, or <code>null</code> if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libreria fetchByGroup_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByGroup_First(groupId, orderByComparator);
	}

	/**
	* Returns the last libreria in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching libreria
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libreria findByGroup_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException {
		return getPersistence().findByGroup_Last(groupId, orderByComparator);
	}

	/**
	* Returns the last libreria in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching libreria, or <code>null</code> if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libreria fetchByGroup_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByGroup_Last(groupId, orderByComparator);
	}

	/**
	* Returns the librerias before and after the current libreria in the ordered set where groupId = &#63;.
	*
	* @param libreriaId the primary key of the current libreria
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next libreria
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a libreria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libreria[] findByGroup_PrevAndNext(
		long libreriaId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException {
		return getPersistence()
				   .findByGroup_PrevAndNext(libreriaId, groupId,
			orderByComparator);
	}

	/**
	* Removes all the librerias where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByGroup(groupId);
	}

	/**
	* Returns the number of librerias where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public static int countByGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByGroup(groupId);
	}

	/**
	* Returns all the librerias where groupId = &#63; and nome = &#63;.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @return the matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.planetek.liferay.service.builder.model.Libreria> findByG_N(
		long groupId, java.lang.String nome)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByG_N(groupId, nome);
	}

	/**
	* Returns a range of all the librerias where groupId = &#63; and nome = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param nome the nome
	* @param start the lower bound of the range of librerias
	* @param end the upper bound of the range of librerias (not inclusive)
	* @return the range of matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.planetek.liferay.service.builder.model.Libreria> findByG_N(
		long groupId, java.lang.String nome, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByG_N(groupId, nome, start, end);
	}

	/**
	* Returns an ordered range of all the librerias where groupId = &#63; and nome = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param nome the nome
	* @param start the lower bound of the range of librerias
	* @param end the upper bound of the range of librerias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.planetek.liferay.service.builder.model.Libreria> findByG_N(
		long groupId, java.lang.String nome, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByG_N(groupId, nome, start, end, orderByComparator);
	}

	/**
	* Returns the first libreria in the ordered set where groupId = &#63; and nome = &#63;.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching libreria
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libreria findByG_N_First(
		long groupId, java.lang.String nome,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException {
		return getPersistence().findByG_N_First(groupId, nome, orderByComparator);
	}

	/**
	* Returns the first libreria in the ordered set where groupId = &#63; and nome = &#63;.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching libreria, or <code>null</code> if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libreria fetchByG_N_First(
		long groupId, java.lang.String nome,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByG_N_First(groupId, nome, orderByComparator);
	}

	/**
	* Returns the last libreria in the ordered set where groupId = &#63; and nome = &#63;.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching libreria
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libreria findByG_N_Last(
		long groupId, java.lang.String nome,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException {
		return getPersistence().findByG_N_Last(groupId, nome, orderByComparator);
	}

	/**
	* Returns the last libreria in the ordered set where groupId = &#63; and nome = &#63;.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching libreria, or <code>null</code> if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libreria fetchByG_N_Last(
		long groupId, java.lang.String nome,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByG_N_Last(groupId, nome, orderByComparator);
	}

	/**
	* Returns the librerias before and after the current libreria in the ordered set where groupId = &#63; and nome = &#63;.
	*
	* @param libreriaId the primary key of the current libreria
	* @param groupId the group ID
	* @param nome the nome
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next libreria
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a libreria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libreria[] findByG_N_PrevAndNext(
		long libreriaId, long groupId, java.lang.String nome,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException {
		return getPersistence()
				   .findByG_N_PrevAndNext(libreriaId, groupId, nome,
			orderByComparator);
	}

	/**
	* Removes all the librerias where groupId = &#63; and nome = &#63; from the database.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByG_N(long groupId, java.lang.String nome)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByG_N(groupId, nome);
	}

	/**
	* Returns the number of librerias where groupId = &#63; and nome = &#63;.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @return the number of matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public static int countByG_N(long groupId, java.lang.String nome)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByG_N(groupId, nome);
	}

	/**
	* Caches the libreria in the entity cache if it is enabled.
	*
	* @param libreria the libreria
	*/
	public static void cacheResult(
		it.planetek.liferay.service.builder.model.Libreria libreria) {
		getPersistence().cacheResult(libreria);
	}

	/**
	* Caches the librerias in the entity cache if it is enabled.
	*
	* @param librerias the librerias
	*/
	public static void cacheResult(
		java.util.List<it.planetek.liferay.service.builder.model.Libreria> librerias) {
		getPersistence().cacheResult(librerias);
	}

	/**
	* Creates a new libreria with the primary key. Does not add the libreria to the database.
	*
	* @param libreriaId the primary key for the new libreria
	* @return the new libreria
	*/
	public static it.planetek.liferay.service.builder.model.Libreria create(
		long libreriaId) {
		return getPersistence().create(libreriaId);
	}

	/**
	* Removes the libreria with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param libreriaId the primary key of the libreria
	* @return the libreria that was removed
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a libreria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libreria remove(
		long libreriaId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException {
		return getPersistence().remove(libreriaId);
	}

	public static it.planetek.liferay.service.builder.model.Libreria updateImpl(
		it.planetek.liferay.service.builder.model.Libreria libreria)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(libreria);
	}

	/**
	* Returns the libreria with the primary key or throws a {@link it.planetek.liferay.service.builder.NoSuchLibreriaException} if it could not be found.
	*
	* @param libreriaId the primary key of the libreria
	* @return the libreria
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a libreria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libreria findByPrimaryKey(
		long libreriaId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException {
		return getPersistence().findByPrimaryKey(libreriaId);
	}

	/**
	* Returns the libreria with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param libreriaId the primary key of the libreria
	* @return the libreria, or <code>null</code> if a libreria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libreria fetchByPrimaryKey(
		long libreriaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(libreriaId);
	}

	/**
	* Returns all the librerias.
	*
	* @return the librerias
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.planetek.liferay.service.builder.model.Libreria> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the librerias.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of librerias
	* @param end the upper bound of the range of librerias (not inclusive)
	* @return the range of librerias
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.planetek.liferay.service.builder.model.Libreria> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the librerias.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of librerias
	* @param end the upper bound of the range of librerias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of librerias
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.planetek.liferay.service.builder.model.Libreria> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the librerias from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of librerias.
	*
	* @return the number of librerias
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static LibreriaPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (LibreriaPersistence)PortletBeanLocatorUtil.locate(it.planetek.liferay.service.builder.service.ClpSerializer.getServletContextName(),
					LibreriaPersistence.class.getName());

			ReferenceRegistry.registerReference(LibreriaUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(LibreriaPersistence persistence) {
	}

	private static LibreriaPersistence _persistence;
}