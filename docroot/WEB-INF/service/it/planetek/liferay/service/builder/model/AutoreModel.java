/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.GroupedModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the Autore service. Represents a row in the &quot;planetek_Autore&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link it.planetek.liferay.service.builder.model.impl.AutoreModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link it.planetek.liferay.service.builder.model.impl.AutoreImpl}.
 * </p>
 *
 * @author jfrancia
 * @see Autore
 * @see it.planetek.liferay.service.builder.model.impl.AutoreImpl
 * @see it.planetek.liferay.service.builder.model.impl.AutoreModelImpl
 * @generated
 */
public interface AutoreModel extends BaseModel<Autore>, GroupedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a autore model instance should use the {@link Autore} interface instead.
	 */

	/**
	 * Returns the primary key of this autore.
	 *
	 * @return the primary key of this autore
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this autore.
	 *
	 * @param primaryKey the primary key of this autore
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the autore ID of this autore.
	 *
	 * @return the autore ID of this autore
	 */
	public long getAutoreId();

	/**
	 * Sets the autore ID of this autore.
	 *
	 * @param autoreId the autore ID of this autore
	 */
	public void setAutoreId(long autoreId);

	/**
	 * Returns the company ID of this autore.
	 *
	 * @return the company ID of this autore
	 */
	@Override
	public long getCompanyId();

	/**
	 * Sets the company ID of this autore.
	 *
	 * @param companyId the company ID of this autore
	 */
	@Override
	public void setCompanyId(long companyId);

	/**
	 * Returns the group ID of this autore.
	 *
	 * @return the group ID of this autore
	 */
	@Override
	public long getGroupId();

	/**
	 * Sets the group ID of this autore.
	 *
	 * @param groupId the group ID of this autore
	 */
	@Override
	public void setGroupId(long groupId);

	/**
	 * Returns the user ID of this autore.
	 *
	 * @return the user ID of this autore
	 */
	@Override
	public long getUserId();

	/**
	 * Sets the user ID of this autore.
	 *
	 * @param userId the user ID of this autore
	 */
	@Override
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this autore.
	 *
	 * @return the user uuid of this autore
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public String getUserUuid() throws SystemException;

	/**
	 * Sets the user uuid of this autore.
	 *
	 * @param userUuid the user uuid of this autore
	 */
	@Override
	public void setUserUuid(String userUuid);

	/**
	 * Returns the user name of this autore.
	 *
	 * @return the user name of this autore
	 */
	@AutoEscape
	@Override
	public String getUserName();

	/**
	 * Sets the user name of this autore.
	 *
	 * @param userName the user name of this autore
	 */
	@Override
	public void setUserName(String userName);

	/**
	 * Returns the create date of this autore.
	 *
	 * @return the create date of this autore
	 */
	@Override
	public Date getCreateDate();

	/**
	 * Sets the create date of this autore.
	 *
	 * @param createDate the create date of this autore
	 */
	@Override
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this autore.
	 *
	 * @return the modified date of this autore
	 */
	@Override
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this autore.
	 *
	 * @param modifiedDate the modified date of this autore
	 */
	@Override
	public void setModifiedDate(Date modifiedDate);

	/**
	 * Returns the nome of this autore.
	 *
	 * @return the nome of this autore
	 */
	@AutoEscape
	public String getNome();

	/**
	 * Sets the nome of this autore.
	 *
	 * @param nome the nome of this autore
	 */
	public void setNome(String nome);

	/**
	 * Returns the cognome of this autore.
	 *
	 * @return the cognome of this autore
	 */
	@AutoEscape
	public String getCognome();

	/**
	 * Sets the cognome of this autore.
	 *
	 * @param cognome the cognome of this autore
	 */
	public void setCognome(String cognome);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(Autore autore);

	@Override
	public int hashCode();

	@Override
	public CacheModel<Autore> toCacheModel();

	@Override
	public Autore toEscapedModel();

	@Override
	public Autore toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}