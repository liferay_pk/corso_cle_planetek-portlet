/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author jfrancia
 * @generated
 */
public class LibreriaSoap implements Serializable {
	public static LibreriaSoap toSoapModel(Libreria model) {
		LibreriaSoap soapModel = new LibreriaSoap();

		soapModel.setLibreriaId(model.getLibreriaId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setNome(model.getNome());

		return soapModel;
	}

	public static LibreriaSoap[] toSoapModels(Libreria[] models) {
		LibreriaSoap[] soapModels = new LibreriaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LibreriaSoap[][] toSoapModels(Libreria[][] models) {
		LibreriaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LibreriaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LibreriaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LibreriaSoap[] toSoapModels(List<Libreria> models) {
		List<LibreriaSoap> soapModels = new ArrayList<LibreriaSoap>(models.size());

		for (Libreria model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LibreriaSoap[soapModels.size()]);
	}

	public LibreriaSoap() {
	}

	public long getPrimaryKey() {
		return _libreriaId;
	}

	public void setPrimaryKey(long pk) {
		setLibreriaId(pk);
	}

	public long getLibreriaId() {
		return _libreriaId;
	}

	public void setLibreriaId(long libreriaId) {
		_libreriaId = libreriaId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getNome() {
		return _nome;
	}

	public void setNome(String nome) {
		_nome = nome;
	}

	private long _libreriaId;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _nome;
}