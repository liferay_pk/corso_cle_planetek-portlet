/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link LibroService}.
 *
 * @author jfrancia
 * @see LibroService
 * @generated
 */
public class LibroServiceWrapper implements LibroService,
	ServiceWrapper<LibroService> {
	public LibroServiceWrapper(LibroService libroService) {
		_libroService = libroService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _libroService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_libroService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _libroService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public it.planetek.liferay.service.builder.model.Libro addLibro(
		long userId, java.lang.String nome, boolean pubblicato,
		int numeroPagine, java.util.Date dataPubblicazione,
		java.lang.String descrizione, java.lang.String isbn,
		com.liferay.portal.service.ServiceContext ctx)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _libroService.addLibro(userId, nome, pubblicato, numeroPagine,
			dataPubblicazione, descrizione, isbn, ctx);
	}

	@Override
	public it.planetek.liferay.service.builder.model.LibroSoap getLibro(
		long libroId) throws java.lang.Exception {
		return _libroService.getLibro(libroId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public LibroService getWrappedLibroService() {
		return _libroService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedLibroService(LibroService libroService) {
		_libroService = libroService;
	}

	@Override
	public LibroService getWrappedService() {
		return _libroService;
	}

	@Override
	public void setWrappedService(LibroService libroService) {
		_libroService = libroService;
	}

	private LibroService _libroService;
}