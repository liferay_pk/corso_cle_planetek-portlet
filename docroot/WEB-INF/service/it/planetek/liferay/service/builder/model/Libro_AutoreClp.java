/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.planetek.liferay.service.builder.service.ClpSerializer;
import it.planetek.liferay.service.builder.service.Libro_AutoreLocalServiceUtil;
import it.planetek.liferay.service.builder.service.persistence.Libro_AutorePK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author jfrancia
 */
public class Libro_AutoreClp extends BaseModelImpl<Libro_Autore>
	implements Libro_Autore {
	public Libro_AutoreClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Libro_Autore.class;
	}

	@Override
	public String getModelClassName() {
		return Libro_Autore.class.getName();
	}

	@Override
	public Libro_AutorePK getPrimaryKey() {
		return new Libro_AutorePK(_libroId, _autoreId);
	}

	@Override
	public void setPrimaryKey(Libro_AutorePK primaryKey) {
		setLibroId(primaryKey.libroId);
		setAutoreId(primaryKey.autoreId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new Libro_AutorePK(_libroId, _autoreId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((Libro_AutorePK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("libroId", getLibroId());
		attributes.put("autoreId", getAutoreId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long libroId = (Long)attributes.get("libroId");

		if (libroId != null) {
			setLibroId(libroId);
		}

		Long autoreId = (Long)attributes.get("autoreId");

		if (autoreId != null) {
			setAutoreId(autoreId);
		}
	}

	@Override
	public long getLibroId() {
		return _libroId;
	}

	@Override
	public void setLibroId(long libroId) {
		_libroId = libroId;

		if (_libro_AutoreRemoteModel != null) {
			try {
				Class<?> clazz = _libro_AutoreRemoteModel.getClass();

				Method method = clazz.getMethod("setLibroId", long.class);

				method.invoke(_libro_AutoreRemoteModel, libroId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getAutoreId() {
		return _autoreId;
	}

	@Override
	public void setAutoreId(long autoreId) {
		_autoreId = autoreId;

		if (_libro_AutoreRemoteModel != null) {
			try {
				Class<?> clazz = _libro_AutoreRemoteModel.getClass();

				Method method = clazz.getMethod("setAutoreId", long.class);

				method.invoke(_libro_AutoreRemoteModel, autoreId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getLibro_AutoreRemoteModel() {
		return _libro_AutoreRemoteModel;
	}

	public void setLibro_AutoreRemoteModel(BaseModel<?> libro_AutoreRemoteModel) {
		_libro_AutoreRemoteModel = libro_AutoreRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _libro_AutoreRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_libro_AutoreRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			Libro_AutoreLocalServiceUtil.addLibro_Autore(this);
		}
		else {
			Libro_AutoreLocalServiceUtil.updateLibro_Autore(this);
		}
	}

	@Override
	public Libro_Autore toEscapedModel() {
		return (Libro_Autore)ProxyUtil.newProxyInstance(Libro_Autore.class.getClassLoader(),
			new Class[] { Libro_Autore.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		Libro_AutoreClp clone = new Libro_AutoreClp();

		clone.setLibroId(getLibroId());
		clone.setAutoreId(getAutoreId());

		return clone;
	}

	@Override
	public int compareTo(Libro_Autore libro_Autore) {
		Libro_AutorePK primaryKey = libro_Autore.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Libro_AutoreClp)) {
			return false;
		}

		Libro_AutoreClp libro_Autore = (Libro_AutoreClp)obj;

		Libro_AutorePK primaryKey = libro_Autore.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{libroId=");
		sb.append(getLibroId());
		sb.append(", autoreId=");
		sb.append(getAutoreId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("it.planetek.liferay.service.builder.model.Libro_Autore");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>libroId</column-name><column-value><![CDATA[");
		sb.append(getLibroId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>autoreId</column-name><column-value><![CDATA[");
		sb.append(getAutoreId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _libroId;
	private long _autoreId;
	private BaseModel<?> _libro_AutoreRemoteModel;
}