/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Libro_Autore}.
 * </p>
 *
 * @author jfrancia
 * @see Libro_Autore
 * @generated
 */
public class Libro_AutoreWrapper implements Libro_Autore,
	ModelWrapper<Libro_Autore> {
	public Libro_AutoreWrapper(Libro_Autore libro_Autore) {
		_libro_Autore = libro_Autore;
	}

	@Override
	public Class<?> getModelClass() {
		return Libro_Autore.class;
	}

	@Override
	public String getModelClassName() {
		return Libro_Autore.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("libroId", getLibroId());
		attributes.put("autoreId", getAutoreId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long libroId = (Long)attributes.get("libroId");

		if (libroId != null) {
			setLibroId(libroId);
		}

		Long autoreId = (Long)attributes.get("autoreId");

		if (autoreId != null) {
			setAutoreId(autoreId);
		}
	}

	/**
	* Returns the primary key of this libro_ autore.
	*
	* @return the primary key of this libro_ autore
	*/
	@Override
	public it.planetek.liferay.service.builder.service.persistence.Libro_AutorePK getPrimaryKey() {
		return _libro_Autore.getPrimaryKey();
	}

	/**
	* Sets the primary key of this libro_ autore.
	*
	* @param primaryKey the primary key of this libro_ autore
	*/
	@Override
	public void setPrimaryKey(
		it.planetek.liferay.service.builder.service.persistence.Libro_AutorePK primaryKey) {
		_libro_Autore.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the libro ID of this libro_ autore.
	*
	* @return the libro ID of this libro_ autore
	*/
	@Override
	public long getLibroId() {
		return _libro_Autore.getLibroId();
	}

	/**
	* Sets the libro ID of this libro_ autore.
	*
	* @param libroId the libro ID of this libro_ autore
	*/
	@Override
	public void setLibroId(long libroId) {
		_libro_Autore.setLibroId(libroId);
	}

	/**
	* Returns the autore ID of this libro_ autore.
	*
	* @return the autore ID of this libro_ autore
	*/
	@Override
	public long getAutoreId() {
		return _libro_Autore.getAutoreId();
	}

	/**
	* Sets the autore ID of this libro_ autore.
	*
	* @param autoreId the autore ID of this libro_ autore
	*/
	@Override
	public void setAutoreId(long autoreId) {
		_libro_Autore.setAutoreId(autoreId);
	}

	@Override
	public boolean isNew() {
		return _libro_Autore.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_libro_Autore.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _libro_Autore.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_libro_Autore.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _libro_Autore.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _libro_Autore.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_libro_Autore.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _libro_Autore.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_libro_Autore.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_libro_Autore.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_libro_Autore.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new Libro_AutoreWrapper((Libro_Autore)_libro_Autore.clone());
	}

	@Override
	public int compareTo(
		it.planetek.liferay.service.builder.model.Libro_Autore libro_Autore) {
		return _libro_Autore.compareTo(libro_Autore);
	}

	@Override
	public int hashCode() {
		return _libro_Autore.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.planetek.liferay.service.builder.model.Libro_Autore> toCacheModel() {
		return _libro_Autore.toCacheModel();
	}

	@Override
	public it.planetek.liferay.service.builder.model.Libro_Autore toEscapedModel() {
		return new Libro_AutoreWrapper(_libro_Autore.toEscapedModel());
	}

	@Override
	public it.planetek.liferay.service.builder.model.Libro_Autore toUnescapedModel() {
		return new Libro_AutoreWrapper(_libro_Autore.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _libro_Autore.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _libro_Autore.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_libro_Autore.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Libro_AutoreWrapper)) {
			return false;
		}

		Libro_AutoreWrapper libro_AutoreWrapper = (Libro_AutoreWrapper)obj;

		if (Validator.equals(_libro_Autore, libro_AutoreWrapper._libro_Autore)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Libro_Autore getWrappedLibro_Autore() {
		return _libro_Autore;
	}

	@Override
	public Libro_Autore getWrappedModel() {
		return _libro_Autore;
	}

	@Override
	public void resetOriginalValues() {
		_libro_Autore.resetOriginalValues();
	}

	private Libro_Autore _libro_Autore;
}