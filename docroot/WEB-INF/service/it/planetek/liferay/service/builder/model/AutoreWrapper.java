/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Autore}.
 * </p>
 *
 * @author jfrancia
 * @see Autore
 * @generated
 */
public class AutoreWrapper implements Autore, ModelWrapper<Autore> {
	public AutoreWrapper(Autore autore) {
		_autore = autore;
	}

	@Override
	public Class<?> getModelClass() {
		return Autore.class;
	}

	@Override
	public String getModelClassName() {
		return Autore.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("autoreId", getAutoreId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("nome", getNome());
		attributes.put("cognome", getCognome());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long autoreId = (Long)attributes.get("autoreId");

		if (autoreId != null) {
			setAutoreId(autoreId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String nome = (String)attributes.get("nome");

		if (nome != null) {
			setNome(nome);
		}

		String cognome = (String)attributes.get("cognome");

		if (cognome != null) {
			setCognome(cognome);
		}
	}

	/**
	* Returns the primary key of this autore.
	*
	* @return the primary key of this autore
	*/
	@Override
	public long getPrimaryKey() {
		return _autore.getPrimaryKey();
	}

	/**
	* Sets the primary key of this autore.
	*
	* @param primaryKey the primary key of this autore
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_autore.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the autore ID of this autore.
	*
	* @return the autore ID of this autore
	*/
	@Override
	public long getAutoreId() {
		return _autore.getAutoreId();
	}

	/**
	* Sets the autore ID of this autore.
	*
	* @param autoreId the autore ID of this autore
	*/
	@Override
	public void setAutoreId(long autoreId) {
		_autore.setAutoreId(autoreId);
	}

	/**
	* Returns the company ID of this autore.
	*
	* @return the company ID of this autore
	*/
	@Override
	public long getCompanyId() {
		return _autore.getCompanyId();
	}

	/**
	* Sets the company ID of this autore.
	*
	* @param companyId the company ID of this autore
	*/
	@Override
	public void setCompanyId(long companyId) {
		_autore.setCompanyId(companyId);
	}

	/**
	* Returns the group ID of this autore.
	*
	* @return the group ID of this autore
	*/
	@Override
	public long getGroupId() {
		return _autore.getGroupId();
	}

	/**
	* Sets the group ID of this autore.
	*
	* @param groupId the group ID of this autore
	*/
	@Override
	public void setGroupId(long groupId) {
		_autore.setGroupId(groupId);
	}

	/**
	* Returns the user ID of this autore.
	*
	* @return the user ID of this autore
	*/
	@Override
	public long getUserId() {
		return _autore.getUserId();
	}

	/**
	* Sets the user ID of this autore.
	*
	* @param userId the user ID of this autore
	*/
	@Override
	public void setUserId(long userId) {
		_autore.setUserId(userId);
	}

	/**
	* Returns the user uuid of this autore.
	*
	* @return the user uuid of this autore
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _autore.getUserUuid();
	}

	/**
	* Sets the user uuid of this autore.
	*
	* @param userUuid the user uuid of this autore
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_autore.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this autore.
	*
	* @return the user name of this autore
	*/
	@Override
	public java.lang.String getUserName() {
		return _autore.getUserName();
	}

	/**
	* Sets the user name of this autore.
	*
	* @param userName the user name of this autore
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_autore.setUserName(userName);
	}

	/**
	* Returns the create date of this autore.
	*
	* @return the create date of this autore
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _autore.getCreateDate();
	}

	/**
	* Sets the create date of this autore.
	*
	* @param createDate the create date of this autore
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_autore.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this autore.
	*
	* @return the modified date of this autore
	*/
	@Override
	public java.util.Date getModifiedDate() {
		return _autore.getModifiedDate();
	}

	/**
	* Sets the modified date of this autore.
	*
	* @param modifiedDate the modified date of this autore
	*/
	@Override
	public void setModifiedDate(java.util.Date modifiedDate) {
		_autore.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the nome of this autore.
	*
	* @return the nome of this autore
	*/
	@Override
	public java.lang.String getNome() {
		return _autore.getNome();
	}

	/**
	* Sets the nome of this autore.
	*
	* @param nome the nome of this autore
	*/
	@Override
	public void setNome(java.lang.String nome) {
		_autore.setNome(nome);
	}

	/**
	* Returns the cognome of this autore.
	*
	* @return the cognome of this autore
	*/
	@Override
	public java.lang.String getCognome() {
		return _autore.getCognome();
	}

	/**
	* Sets the cognome of this autore.
	*
	* @param cognome the cognome of this autore
	*/
	@Override
	public void setCognome(java.lang.String cognome) {
		_autore.setCognome(cognome);
	}

	@Override
	public boolean isNew() {
		return _autore.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_autore.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _autore.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_autore.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _autore.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _autore.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_autore.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _autore.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_autore.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_autore.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_autore.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AutoreWrapper((Autore)_autore.clone());
	}

	@Override
	public int compareTo(
		it.planetek.liferay.service.builder.model.Autore autore) {
		return _autore.compareTo(autore);
	}

	@Override
	public int hashCode() {
		return _autore.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.planetek.liferay.service.builder.model.Autore> toCacheModel() {
		return _autore.toCacheModel();
	}

	@Override
	public it.planetek.liferay.service.builder.model.Autore toEscapedModel() {
		return new AutoreWrapper(_autore.toEscapedModel());
	}

	@Override
	public it.planetek.liferay.service.builder.model.Autore toUnescapedModel() {
		return new AutoreWrapper(_autore.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _autore.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _autore.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_autore.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AutoreWrapper)) {
			return false;
		}

		AutoreWrapper autoreWrapper = (AutoreWrapper)obj;

		if (Validator.equals(_autore, autoreWrapper._autore)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Autore getWrappedAutore() {
		return _autore;
	}

	@Override
	public Autore getWrappedModel() {
		return _autore;
	}

	@Override
	public void resetOriginalValues() {
		_autore.resetOriginalValues();
	}

	private Autore _autore;
}