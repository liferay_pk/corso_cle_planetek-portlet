/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Libro_Autore. This utility wraps
 * {@link it.planetek.liferay.service.builder.service.impl.Libro_AutoreLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author jfrancia
 * @see Libro_AutoreLocalService
 * @see it.planetek.liferay.service.builder.service.base.Libro_AutoreLocalServiceBaseImpl
 * @see it.planetek.liferay.service.builder.service.impl.Libro_AutoreLocalServiceImpl
 * @generated
 */
public class Libro_AutoreLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.planetek.liferay.service.builder.service.impl.Libro_AutoreLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the libro_ autore to the database. Also notifies the appropriate model listeners.
	*
	* @param libro_Autore the libro_ autore
	* @return the libro_ autore that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libro_Autore addLibro_Autore(
		it.planetek.liferay.service.builder.model.Libro_Autore libro_Autore)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addLibro_Autore(libro_Autore);
	}

	/**
	* Creates a new libro_ autore with the primary key. Does not add the libro_ autore to the database.
	*
	* @param libro_AutorePK the primary key for the new libro_ autore
	* @return the new libro_ autore
	*/
	public static it.planetek.liferay.service.builder.model.Libro_Autore createLibro_Autore(
		it.planetek.liferay.service.builder.service.persistence.Libro_AutorePK libro_AutorePK) {
		return getService().createLibro_Autore(libro_AutorePK);
	}

	/**
	* Deletes the libro_ autore with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param libro_AutorePK the primary key of the libro_ autore
	* @return the libro_ autore that was removed
	* @throws PortalException if a libro_ autore with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libro_Autore deleteLibro_Autore(
		it.planetek.liferay.service.builder.service.persistence.Libro_AutorePK libro_AutorePK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteLibro_Autore(libro_AutorePK);
	}

	/**
	* Deletes the libro_ autore from the database. Also notifies the appropriate model listeners.
	*
	* @param libro_Autore the libro_ autore
	* @return the libro_ autore that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libro_Autore deleteLibro_Autore(
		it.planetek.liferay.service.builder.model.Libro_Autore libro_Autore)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteLibro_Autore(libro_Autore);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.Libro_AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.Libro_AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.planetek.liferay.service.builder.model.Libro_Autore fetchLibro_Autore(
		it.planetek.liferay.service.builder.service.persistence.Libro_AutorePK libro_AutorePK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchLibro_Autore(libro_AutorePK);
	}

	/**
	* Returns the libro_ autore with the primary key.
	*
	* @param libro_AutorePK the primary key of the libro_ autore
	* @return the libro_ autore
	* @throws PortalException if a libro_ autore with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libro_Autore getLibro_Autore(
		it.planetek.liferay.service.builder.service.persistence.Libro_AutorePK libro_AutorePK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getLibro_Autore(libro_AutorePK);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the libro_ autores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.Libro_AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of libro_ autores
	* @param end the upper bound of the range of libro_ autores (not inclusive)
	* @return the range of libro_ autores
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.planetek.liferay.service.builder.model.Libro_Autore> getLibro_Autores(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLibro_Autores(start, end);
	}

	/**
	* Returns the number of libro_ autores.
	*
	* @return the number of libro_ autores
	* @throws SystemException if a system exception occurred
	*/
	public static int getLibro_AutoresCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLibro_AutoresCount();
	}

	/**
	* Updates the libro_ autore in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param libro_Autore the libro_ autore
	* @return the libro_ autore that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.planetek.liferay.service.builder.model.Libro_Autore updateLibro_Autore(
		it.planetek.liferay.service.builder.model.Libro_Autore libro_Autore)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateLibro_Autore(libro_Autore);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static void clearService() {
		_service = null;
	}

	public static Libro_AutoreLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					Libro_AutoreLocalService.class.getName());

			if (invokableLocalService instanceof Libro_AutoreLocalService) {
				_service = (Libro_AutoreLocalService)invokableLocalService;
			}
			else {
				_service = new Libro_AutoreLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(Libro_AutoreLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(Libro_AutoreLocalService service) {
	}

	private static Libro_AutoreLocalService _service;
}