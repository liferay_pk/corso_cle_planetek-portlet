/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author jfrancia
 * @generated
 */
public class AutoreSoap implements Serializable {
	public static AutoreSoap toSoapModel(Autore model) {
		AutoreSoap soapModel = new AutoreSoap();

		soapModel.setAutoreId(model.getAutoreId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setNome(model.getNome());
		soapModel.setCognome(model.getCognome());

		return soapModel;
	}

	public static AutoreSoap[] toSoapModels(Autore[] models) {
		AutoreSoap[] soapModels = new AutoreSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AutoreSoap[][] toSoapModels(Autore[][] models) {
		AutoreSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AutoreSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AutoreSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AutoreSoap[] toSoapModels(List<Autore> models) {
		List<AutoreSoap> soapModels = new ArrayList<AutoreSoap>(models.size());

		for (Autore model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AutoreSoap[soapModels.size()]);
	}

	public AutoreSoap() {
	}

	public long getPrimaryKey() {
		return _autoreId;
	}

	public void setPrimaryKey(long pk) {
		setAutoreId(pk);
	}

	public long getAutoreId() {
		return _autoreId;
	}

	public void setAutoreId(long autoreId) {
		_autoreId = autoreId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getNome() {
		return _nome;
	}

	public void setNome(String nome) {
		_nome = nome;
	}

	public String getCognome() {
		return _cognome;
	}

	public void setCognome(String cognome) {
		_cognome = cognome;
	}

	private long _autoreId;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _nome;
	private String _cognome;
}