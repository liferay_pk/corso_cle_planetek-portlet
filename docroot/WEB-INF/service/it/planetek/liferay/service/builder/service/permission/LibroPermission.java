package it.planetek.liferay.service.builder.service.permission;

import it.planetek.liferay.service.builder.model.Libro;
import it.planetek.liferay.service.builder.service.LibroLocalServiceUtil;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.security.auth.PrincipalException;
import com.liferay.portal.security.permission.PermissionChecker;

public class LibroPermission {

	public static void check(PermissionChecker permissionChecker, Libro entry,String actionId)
			throws PortalException {

			if (!contains(permissionChecker, entry, actionId)) {
				throw new PrincipalException();
			}
	}

	public static void check(PermissionChecker permissionChecker, long entryId, String actionId)
			throws PortalException, SystemException {

			if (!contains(permissionChecker, entryId, actionId)) {
				throw new PrincipalException();
			}
	}

	public static boolean contains(PermissionChecker permissionChecker, Libro entry,String actionId) {

			if (permissionChecker.hasOwnerPermission(
					entry.getCompanyId(), Libro.class.getName(),
					entry.getLibroId(), entry.getUserId(),
					actionId)) {

				return true;
			}
			return permissionChecker.hasPermission(entry.getGroupId(), Libro.class.getName(),
					entry.getLibroId(), actionId);
	}

	public static boolean contains(PermissionChecker permissionChecker, long entryId, String actionId)
			throws PortalException, SystemException {

			Libro entry = LibroLocalServiceUtil.getLibro(entryId);
			return contains(permissionChecker, entry, actionId);
	}
	
}
