/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.planetek.liferay.service.builder.model.Libro;

/**
 * The persistence interface for the libro service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author jfrancia
 * @see LibroPersistenceImpl
 * @see LibroUtil
 * @generated
 */
public interface LibroPersistence extends BasePersistence<Libro> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LibroUtil} to access the libro persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the libros where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching libros
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro> findByUuid(
		java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the libros where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of libros
	* @param end the upper bound of the range of libros (not inclusive)
	* @return the range of matching libros
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro> findByUuid(
		java.lang.String uuid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the libros where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of libros
	* @param end the upper bound of the range of libros (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching libros
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first libro in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching libro
	* @throws it.planetek.liferay.service.builder.NoSuchLibroException if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro findByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Returns the first libro in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching libro, or <code>null</code> if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro fetchByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last libro in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching libro
	* @throws it.planetek.liferay.service.builder.NoSuchLibroException if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro findByUuid_Last(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Returns the last libro in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching libro, or <code>null</code> if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro fetchByUuid_Last(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the libros before and after the current libro in the ordered set where uuid = &#63;.
	*
	* @param libroId the primary key of the current libro
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next libro
	* @throws it.planetek.liferay.service.builder.NoSuchLibroException if a libro with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro[] findByUuid_PrevAndNext(
		long libroId, java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Removes all the libros where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUuid(java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of libros where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching libros
	* @throws SystemException if a system exception occurred
	*/
	public int countByUuid(java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the libro where uuid = &#63; and groupId = &#63; or throws a {@link it.planetek.liferay.service.builder.NoSuchLibroException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching libro
	* @throws it.planetek.liferay.service.builder.NoSuchLibroException if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro findByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Returns the libro where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching libro, or <code>null</code> if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro fetchByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the libro where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching libro, or <code>null</code> if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro fetchByUUID_G(
		java.lang.String uuid, long groupId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the libro where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the libro that was removed
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro removeByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Returns the number of libros where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching libros
	* @throws SystemException if a system exception occurred
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the libros where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching libros
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro> findByUuid_C(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the libros where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of libros
	* @param end the upper bound of the range of libros (not inclusive)
	* @return the range of matching libros
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the libros where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of libros
	* @param end the upper bound of the range of libros (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching libros
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first libro in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching libro
	* @throws it.planetek.liferay.service.builder.NoSuchLibroException if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro findByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Returns the first libro in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching libro, or <code>null</code> if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro fetchByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last libro in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching libro
	* @throws it.planetek.liferay.service.builder.NoSuchLibroException if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro findByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Returns the last libro in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching libro, or <code>null</code> if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro fetchByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the libros before and after the current libro in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param libroId the primary key of the current libro
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next libro
	* @throws it.planetek.liferay.service.builder.NoSuchLibroException if a libro with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro[] findByUuid_C_PrevAndNext(
		long libroId, java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Removes all the libros where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of libros where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching libros
	* @throws SystemException if a system exception occurred
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the libros where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching libros
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro> findByGroup(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the libros where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of libros
	* @param end the upper bound of the range of libros (not inclusive)
	* @return the range of matching libros
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro> findByGroup(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the libros where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of libros
	* @param end the upper bound of the range of libros (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching libros
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro> findByGroup(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first libro in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching libro
	* @throws it.planetek.liferay.service.builder.NoSuchLibroException if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro findByGroup_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Returns the first libro in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching libro, or <code>null</code> if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro fetchByGroup_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last libro in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching libro
	* @throws it.planetek.liferay.service.builder.NoSuchLibroException if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro findByGroup_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Returns the last libro in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching libro, or <code>null</code> if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro fetchByGroup_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the libros before and after the current libro in the ordered set where groupId = &#63;.
	*
	* @param libroId the primary key of the current libro
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next libro
	* @throws it.planetek.liferay.service.builder.NoSuchLibroException if a libro with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro[] findByGroup_PrevAndNext(
		long libroId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Returns all the libros that the user has permission to view where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching libros that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro> filterFindByGroup(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the libros that the user has permission to view where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of libros
	* @param end the upper bound of the range of libros (not inclusive)
	* @return the range of matching libros that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro> filterFindByGroup(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the libros that the user has permissions to view where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of libros
	* @param end the upper bound of the range of libros (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching libros that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro> filterFindByGroup(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the libros before and after the current libro in the ordered set of libros that the user has permission to view where groupId = &#63;.
	*
	* @param libroId the primary key of the current libro
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next libro
	* @throws it.planetek.liferay.service.builder.NoSuchLibroException if a libro with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro[] filterFindByGroup_PrevAndNext(
		long libroId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Removes all the libros where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of libros where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching libros
	* @throws SystemException if a system exception occurred
	*/
	public int countByGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of libros that the user has permission to view where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching libros that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public int filterCountByGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the libro where groupId = &#63; and nome = &#63; or throws a {@link it.planetek.liferay.service.builder.NoSuchLibroException} if it could not be found.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @return the matching libro
	* @throws it.planetek.liferay.service.builder.NoSuchLibroException if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro findByG_N(
		long groupId, java.lang.String nome)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Returns the libro where groupId = &#63; and nome = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @return the matching libro, or <code>null</code> if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro fetchByG_N(
		long groupId, java.lang.String nome)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the libro where groupId = &#63; and nome = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching libro, or <code>null</code> if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro fetchByG_N(
		long groupId, java.lang.String nome, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the libro where groupId = &#63; and nome = &#63; from the database.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @return the libro that was removed
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro removeByG_N(
		long groupId, java.lang.String nome)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Returns the number of libros where groupId = &#63; and nome = &#63;.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @return the number of matching libros
	* @throws SystemException if a system exception occurred
	*/
	public int countByG_N(long groupId, java.lang.String nome)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the libro where groupId = &#63; and isbn = &#63; or throws a {@link it.planetek.liferay.service.builder.NoSuchLibroException} if it could not be found.
	*
	* @param groupId the group ID
	* @param isbn the isbn
	* @return the matching libro
	* @throws it.planetek.liferay.service.builder.NoSuchLibroException if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro findByG_I(
		long groupId, java.lang.String isbn)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Returns the libro where groupId = &#63; and isbn = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param groupId the group ID
	* @param isbn the isbn
	* @return the matching libro, or <code>null</code> if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro fetchByG_I(
		long groupId, java.lang.String isbn)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the libro where groupId = &#63; and isbn = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param groupId the group ID
	* @param isbn the isbn
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching libro, or <code>null</code> if a matching libro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro fetchByG_I(
		long groupId, java.lang.String isbn, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the libro where groupId = &#63; and isbn = &#63; from the database.
	*
	* @param groupId the group ID
	* @param isbn the isbn
	* @return the libro that was removed
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro removeByG_I(
		long groupId, java.lang.String isbn)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Returns the number of libros where groupId = &#63; and isbn = &#63;.
	*
	* @param groupId the group ID
	* @param isbn the isbn
	* @return the number of matching libros
	* @throws SystemException if a system exception occurred
	*/
	public int countByG_I(long groupId, java.lang.String isbn)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the libro in the entity cache if it is enabled.
	*
	* @param libro the libro
	*/
	public void cacheResult(
		it.planetek.liferay.service.builder.model.Libro libro);

	/**
	* Caches the libros in the entity cache if it is enabled.
	*
	* @param libros the libros
	*/
	public void cacheResult(
		java.util.List<it.planetek.liferay.service.builder.model.Libro> libros);

	/**
	* Creates a new libro with the primary key. Does not add the libro to the database.
	*
	* @param libroId the primary key for the new libro
	* @return the new libro
	*/
	public it.planetek.liferay.service.builder.model.Libro create(long libroId);

	/**
	* Removes the libro with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param libroId the primary key of the libro
	* @return the libro that was removed
	* @throws it.planetek.liferay.service.builder.NoSuchLibroException if a libro with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro remove(long libroId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	public it.planetek.liferay.service.builder.model.Libro updateImpl(
		it.planetek.liferay.service.builder.model.Libro libro)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the libro with the primary key or throws a {@link it.planetek.liferay.service.builder.NoSuchLibroException} if it could not be found.
	*
	* @param libroId the primary key of the libro
	* @return the libro
	* @throws it.planetek.liferay.service.builder.NoSuchLibroException if a libro with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro findByPrimaryKey(
		long libroId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibroException;

	/**
	* Returns the libro with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param libroId the primary key of the libro
	* @return the libro, or <code>null</code> if a libro with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro fetchByPrimaryKey(
		long libroId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the libros.
	*
	* @return the libros
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the libros.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of libros
	* @param end the upper bound of the range of libros (not inclusive)
	* @return the range of libros
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the libros.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of libros
	* @param end the upper bound of the range of libros (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of libros
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the libros from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of libros.
	*
	* @return the number of libros
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}