/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link LibreriaLocalService}.
 *
 * @author jfrancia
 * @see LibreriaLocalService
 * @generated
 */
public class LibreriaLocalServiceWrapper implements LibreriaLocalService,
	ServiceWrapper<LibreriaLocalService> {
	public LibreriaLocalServiceWrapper(
		LibreriaLocalService libreriaLocalService) {
		_libreriaLocalService = libreriaLocalService;
	}

	/**
	* Adds the libreria to the database. Also notifies the appropriate model listeners.
	*
	* @param libreria the libreria
	* @return the libreria that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.planetek.liferay.service.builder.model.Libreria addLibreria(
		it.planetek.liferay.service.builder.model.Libreria libreria)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _libreriaLocalService.addLibreria(libreria);
	}

	/**
	* Creates a new libreria with the primary key. Does not add the libreria to the database.
	*
	* @param libreriaId the primary key for the new libreria
	* @return the new libreria
	*/
	@Override
	public it.planetek.liferay.service.builder.model.Libreria createLibreria(
		long libreriaId) {
		return _libreriaLocalService.createLibreria(libreriaId);
	}

	/**
	* Deletes the libreria with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param libreriaId the primary key of the libreria
	* @return the libreria that was removed
	* @throws PortalException if a libreria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.planetek.liferay.service.builder.model.Libreria deleteLibreria(
		long libreriaId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _libreriaLocalService.deleteLibreria(libreriaId);
	}

	/**
	* Deletes the libreria from the database. Also notifies the appropriate model listeners.
	*
	* @param libreria the libreria
	* @return the libreria that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.planetek.liferay.service.builder.model.Libreria deleteLibreria(
		it.planetek.liferay.service.builder.model.Libreria libreria)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _libreriaLocalService.deleteLibreria(libreria);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _libreriaLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _libreriaLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _libreriaLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _libreriaLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _libreriaLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _libreriaLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public it.planetek.liferay.service.builder.model.Libreria fetchLibreria(
		long libreriaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _libreriaLocalService.fetchLibreria(libreriaId);
	}

	/**
	* Returns the libreria with the primary key.
	*
	* @param libreriaId the primary key of the libreria
	* @return the libreria
	* @throws PortalException if a libreria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.planetek.liferay.service.builder.model.Libreria getLibreria(
		long libreriaId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _libreriaLocalService.getLibreria(libreriaId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _libreriaLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the librerias.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of librerias
	* @param end the upper bound of the range of librerias (not inclusive)
	* @return the range of librerias
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.planetek.liferay.service.builder.model.Libreria> getLibrerias(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _libreriaLocalService.getLibrerias(start, end);
	}

	/**
	* Returns the number of librerias.
	*
	* @return the number of librerias
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getLibreriasCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _libreriaLocalService.getLibreriasCount();
	}

	/**
	* Updates the libreria in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param libreria the libreria
	* @return the libreria that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.planetek.liferay.service.builder.model.Libreria updateLibreria(
		it.planetek.liferay.service.builder.model.Libreria libreria)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _libreriaLocalService.updateLibreria(libreria);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _libreriaLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_libreriaLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _libreriaLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public LibreriaLocalService getWrappedLibreriaLocalService() {
		return _libreriaLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedLibreriaLocalService(
		LibreriaLocalService libreriaLocalService) {
		_libreriaLocalService = libreriaLocalService;
	}

	@Override
	public LibreriaLocalService getWrappedService() {
		return _libreriaLocalService;
	}

	@Override
	public void setWrappedService(LibreriaLocalService libreriaLocalService) {
		_libreriaLocalService = libreriaLocalService;
	}

	private LibreriaLocalService _libreriaLocalService;
}