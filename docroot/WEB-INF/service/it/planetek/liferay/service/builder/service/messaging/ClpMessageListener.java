/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service.messaging;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;

import it.planetek.liferay.service.builder.service.AutoreLocalServiceUtil;
import it.planetek.liferay.service.builder.service.ClpSerializer;
import it.planetek.liferay.service.builder.service.LibreriaLocalServiceUtil;
import it.planetek.liferay.service.builder.service.LibroLocalServiceUtil;
import it.planetek.liferay.service.builder.service.LibroServiceUtil;
import it.planetek.liferay.service.builder.service.Libro_AutoreLocalServiceUtil;
import it.planetek.liferay.service.builder.service.TaskServiceUtil;

/**
 * @author jfrancia
 */
public class ClpMessageListener extends BaseMessageListener {
	public static String getServletContextName() {
		return ClpSerializer.getServletContextName();
	}

	@Override
	protected void doReceive(Message message) throws Exception {
		String command = message.getString("command");
		String servletContextName = message.getString("servletContextName");

		if (command.equals("undeploy") &&
				servletContextName.equals(getServletContextName())) {
			AutoreLocalServiceUtil.clearService();

			LibreriaLocalServiceUtil.clearService();

			LibroLocalServiceUtil.clearService();

			LibroServiceUtil.clearService();
			Libro_AutoreLocalServiceUtil.clearService();

			TaskServiceUtil.clearService();
		}
	}
}