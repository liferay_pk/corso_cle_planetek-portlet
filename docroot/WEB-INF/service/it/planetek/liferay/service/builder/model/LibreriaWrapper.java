/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Libreria}.
 * </p>
 *
 * @author jfrancia
 * @see Libreria
 * @generated
 */
public class LibreriaWrapper implements Libreria, ModelWrapper<Libreria> {
	public LibreriaWrapper(Libreria libreria) {
		_libreria = libreria;
	}

	@Override
	public Class<?> getModelClass() {
		return Libreria.class;
	}

	@Override
	public String getModelClassName() {
		return Libreria.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("libreriaId", getLibreriaId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("nome", getNome());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long libreriaId = (Long)attributes.get("libreriaId");

		if (libreriaId != null) {
			setLibreriaId(libreriaId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String nome = (String)attributes.get("nome");

		if (nome != null) {
			setNome(nome);
		}
	}

	/**
	* Returns the primary key of this libreria.
	*
	* @return the primary key of this libreria
	*/
	@Override
	public long getPrimaryKey() {
		return _libreria.getPrimaryKey();
	}

	/**
	* Sets the primary key of this libreria.
	*
	* @param primaryKey the primary key of this libreria
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_libreria.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the libreria ID of this libreria.
	*
	* @return the libreria ID of this libreria
	*/
	@Override
	public long getLibreriaId() {
		return _libreria.getLibreriaId();
	}

	/**
	* Sets the libreria ID of this libreria.
	*
	* @param libreriaId the libreria ID of this libreria
	*/
	@Override
	public void setLibreriaId(long libreriaId) {
		_libreria.setLibreriaId(libreriaId);
	}

	/**
	* Returns the company ID of this libreria.
	*
	* @return the company ID of this libreria
	*/
	@Override
	public long getCompanyId() {
		return _libreria.getCompanyId();
	}

	/**
	* Sets the company ID of this libreria.
	*
	* @param companyId the company ID of this libreria
	*/
	@Override
	public void setCompanyId(long companyId) {
		_libreria.setCompanyId(companyId);
	}

	/**
	* Returns the group ID of this libreria.
	*
	* @return the group ID of this libreria
	*/
	@Override
	public long getGroupId() {
		return _libreria.getGroupId();
	}

	/**
	* Sets the group ID of this libreria.
	*
	* @param groupId the group ID of this libreria
	*/
	@Override
	public void setGroupId(long groupId) {
		_libreria.setGroupId(groupId);
	}

	/**
	* Returns the user ID of this libreria.
	*
	* @return the user ID of this libreria
	*/
	@Override
	public long getUserId() {
		return _libreria.getUserId();
	}

	/**
	* Sets the user ID of this libreria.
	*
	* @param userId the user ID of this libreria
	*/
	@Override
	public void setUserId(long userId) {
		_libreria.setUserId(userId);
	}

	/**
	* Returns the user uuid of this libreria.
	*
	* @return the user uuid of this libreria
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _libreria.getUserUuid();
	}

	/**
	* Sets the user uuid of this libreria.
	*
	* @param userUuid the user uuid of this libreria
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_libreria.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this libreria.
	*
	* @return the user name of this libreria
	*/
	@Override
	public java.lang.String getUserName() {
		return _libreria.getUserName();
	}

	/**
	* Sets the user name of this libreria.
	*
	* @param userName the user name of this libreria
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_libreria.setUserName(userName);
	}

	/**
	* Returns the create date of this libreria.
	*
	* @return the create date of this libreria
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _libreria.getCreateDate();
	}

	/**
	* Sets the create date of this libreria.
	*
	* @param createDate the create date of this libreria
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_libreria.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this libreria.
	*
	* @return the modified date of this libreria
	*/
	@Override
	public java.util.Date getModifiedDate() {
		return _libreria.getModifiedDate();
	}

	/**
	* Sets the modified date of this libreria.
	*
	* @param modifiedDate the modified date of this libreria
	*/
	@Override
	public void setModifiedDate(java.util.Date modifiedDate) {
		_libreria.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the nome of this libreria.
	*
	* @return the nome of this libreria
	*/
	@Override
	public java.lang.String getNome() {
		return _libreria.getNome();
	}

	/**
	* Sets the nome of this libreria.
	*
	* @param nome the nome of this libreria
	*/
	@Override
	public void setNome(java.lang.String nome) {
		_libreria.setNome(nome);
	}

	@Override
	public boolean isNew() {
		return _libreria.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_libreria.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _libreria.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_libreria.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _libreria.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _libreria.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_libreria.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _libreria.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_libreria.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_libreria.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_libreria.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new LibreriaWrapper((Libreria)_libreria.clone());
	}

	@Override
	public int compareTo(
		it.planetek.liferay.service.builder.model.Libreria libreria) {
		return _libreria.compareTo(libreria);
	}

	@Override
	public int hashCode() {
		return _libreria.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.planetek.liferay.service.builder.model.Libreria> toCacheModel() {
		return _libreria.toCacheModel();
	}

	@Override
	public it.planetek.liferay.service.builder.model.Libreria toEscapedModel() {
		return new LibreriaWrapper(_libreria.toEscapedModel());
	}

	@Override
	public it.planetek.liferay.service.builder.model.Libreria toUnescapedModel() {
		return new LibreriaWrapper(_libreria.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _libreria.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _libreria.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_libreria.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LibreriaWrapper)) {
			return false;
		}

		LibreriaWrapper libreriaWrapper = (LibreriaWrapper)obj;

		if (Validator.equals(_libreria, libreriaWrapper._libreria)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Libreria getWrappedLibreria() {
		return _libreria;
	}

	@Override
	public Libreria getWrappedModel() {
		return _libreria;
	}

	@Override
	public void resetOriginalValues() {
		_libreria.resetOriginalValues();
	}

	private Libreria _libreria;
}