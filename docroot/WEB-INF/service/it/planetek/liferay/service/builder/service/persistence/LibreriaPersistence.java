/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.planetek.liferay.service.builder.model.Libreria;

/**
 * The persistence interface for the libreria service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author jfrancia
 * @see LibreriaPersistenceImpl
 * @see LibreriaUtil
 * @generated
 */
public interface LibreriaPersistence extends BasePersistence<Libreria> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LibreriaUtil} to access the libreria persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the librerias where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libreria> findByGroup(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the librerias where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of librerias
	* @param end the upper bound of the range of librerias (not inclusive)
	* @return the range of matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libreria> findByGroup(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the librerias where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of librerias
	* @param end the upper bound of the range of librerias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libreria> findByGroup(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first libreria in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching libreria
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libreria findByGroup_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException;

	/**
	* Returns the first libreria in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching libreria, or <code>null</code> if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libreria fetchByGroup_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last libreria in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching libreria
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libreria findByGroup_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException;

	/**
	* Returns the last libreria in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching libreria, or <code>null</code> if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libreria fetchByGroup_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the librerias before and after the current libreria in the ordered set where groupId = &#63;.
	*
	* @param libreriaId the primary key of the current libreria
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next libreria
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a libreria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libreria[] findByGroup_PrevAndNext(
		long libreriaId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException;

	/**
	* Removes all the librerias where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of librerias where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public int countByGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the librerias where groupId = &#63; and nome = &#63;.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @return the matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libreria> findByG_N(
		long groupId, java.lang.String nome)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the librerias where groupId = &#63; and nome = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param nome the nome
	* @param start the lower bound of the range of librerias
	* @param end the upper bound of the range of librerias (not inclusive)
	* @return the range of matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libreria> findByG_N(
		long groupId, java.lang.String nome, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the librerias where groupId = &#63; and nome = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param nome the nome
	* @param start the lower bound of the range of librerias
	* @param end the upper bound of the range of librerias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libreria> findByG_N(
		long groupId, java.lang.String nome, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first libreria in the ordered set where groupId = &#63; and nome = &#63;.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching libreria
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libreria findByG_N_First(
		long groupId, java.lang.String nome,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException;

	/**
	* Returns the first libreria in the ordered set where groupId = &#63; and nome = &#63;.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching libreria, or <code>null</code> if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libreria fetchByG_N_First(
		long groupId, java.lang.String nome,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last libreria in the ordered set where groupId = &#63; and nome = &#63;.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching libreria
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libreria findByG_N_Last(
		long groupId, java.lang.String nome,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException;

	/**
	* Returns the last libreria in the ordered set where groupId = &#63; and nome = &#63;.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching libreria, or <code>null</code> if a matching libreria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libreria fetchByG_N_Last(
		long groupId, java.lang.String nome,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the librerias before and after the current libreria in the ordered set where groupId = &#63; and nome = &#63;.
	*
	* @param libreriaId the primary key of the current libreria
	* @param groupId the group ID
	* @param nome the nome
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next libreria
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a libreria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libreria[] findByG_N_PrevAndNext(
		long libreriaId, long groupId, java.lang.String nome,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException;

	/**
	* Removes all the librerias where groupId = &#63; and nome = &#63; from the database.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @throws SystemException if a system exception occurred
	*/
	public void removeByG_N(long groupId, java.lang.String nome)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of librerias where groupId = &#63; and nome = &#63;.
	*
	* @param groupId the group ID
	* @param nome the nome
	* @return the number of matching librerias
	* @throws SystemException if a system exception occurred
	*/
	public int countByG_N(long groupId, java.lang.String nome)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the libreria in the entity cache if it is enabled.
	*
	* @param libreria the libreria
	*/
	public void cacheResult(
		it.planetek.liferay.service.builder.model.Libreria libreria);

	/**
	* Caches the librerias in the entity cache if it is enabled.
	*
	* @param librerias the librerias
	*/
	public void cacheResult(
		java.util.List<it.planetek.liferay.service.builder.model.Libreria> librerias);

	/**
	* Creates a new libreria with the primary key. Does not add the libreria to the database.
	*
	* @param libreriaId the primary key for the new libreria
	* @return the new libreria
	*/
	public it.planetek.liferay.service.builder.model.Libreria create(
		long libreriaId);

	/**
	* Removes the libreria with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param libreriaId the primary key of the libreria
	* @return the libreria that was removed
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a libreria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libreria remove(
		long libreriaId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException;

	public it.planetek.liferay.service.builder.model.Libreria updateImpl(
		it.planetek.liferay.service.builder.model.Libreria libreria)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the libreria with the primary key or throws a {@link it.planetek.liferay.service.builder.NoSuchLibreriaException} if it could not be found.
	*
	* @param libreriaId the primary key of the libreria
	* @return the libreria
	* @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a libreria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libreria findByPrimaryKey(
		long libreriaId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibreriaException;

	/**
	* Returns the libreria with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param libreriaId the primary key of the libreria
	* @return the libreria, or <code>null</code> if a libreria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libreria fetchByPrimaryKey(
		long libreriaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the librerias.
	*
	* @return the librerias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libreria> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the librerias.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of librerias
	* @param end the upper bound of the range of librerias (not inclusive)
	* @return the range of librerias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libreria> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the librerias.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of librerias
	* @param end the upper bound of the range of librerias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of librerias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libreria> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the librerias from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of librerias.
	*
	* @return the number of librerias
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}