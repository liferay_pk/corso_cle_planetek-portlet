/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author jfrancia
 */
public class Libro_AutorePK implements Comparable<Libro_AutorePK>, Serializable {
	public long libroId;
	public long autoreId;

	public Libro_AutorePK() {
	}

	public Libro_AutorePK(long libroId, long autoreId) {
		this.libroId = libroId;
		this.autoreId = autoreId;
	}

	public long getLibroId() {
		return libroId;
	}

	public void setLibroId(long libroId) {
		this.libroId = libroId;
	}

	public long getAutoreId() {
		return autoreId;
	}

	public void setAutoreId(long autoreId) {
		this.autoreId = autoreId;
	}

	@Override
	public int compareTo(Libro_AutorePK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (libroId < pk.libroId) {
			value = -1;
		}
		else if (libroId > pk.libroId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (autoreId < pk.autoreId) {
			value = -1;
		}
		else if (autoreId > pk.autoreId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Libro_AutorePK)) {
			return false;
		}

		Libro_AutorePK pk = (Libro_AutorePK)obj;

		if ((libroId == pk.libroId) && (autoreId == pk.autoreId)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(libroId) + String.valueOf(autoreId)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("libroId");
		sb.append(StringPool.EQUAL);
		sb.append(libroId);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("autoreId");
		sb.append(StringPool.EQUAL);
		sb.append(autoreId);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}