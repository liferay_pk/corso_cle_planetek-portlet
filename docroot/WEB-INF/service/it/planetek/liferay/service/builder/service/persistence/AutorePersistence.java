/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.planetek.liferay.service.builder.model.Autore;

/**
 * The persistence interface for the autore service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author jfrancia
 * @see AutorePersistenceImpl
 * @see AutoreUtil
 * @generated
 */
public interface AutorePersistence extends BasePersistence<Autore> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AutoreUtil} to access the autore persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the autores where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching autores
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Autore> findByGroup(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the autores where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of autores
	* @param end the upper bound of the range of autores (not inclusive)
	* @return the range of matching autores
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Autore> findByGroup(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the autores where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of autores
	* @param end the upper bound of the range of autores (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching autores
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Autore> findByGroup(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first autore in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching autore
	* @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a matching autore could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Autore findByGroup_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchAutoreException;

	/**
	* Returns the first autore in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching autore, or <code>null</code> if a matching autore could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Autore fetchByGroup_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last autore in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching autore
	* @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a matching autore could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Autore findByGroup_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchAutoreException;

	/**
	* Returns the last autore in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching autore, or <code>null</code> if a matching autore could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Autore fetchByGroup_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the autores before and after the current autore in the ordered set where groupId = &#63;.
	*
	* @param autoreId the primary key of the current autore
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next autore
	* @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a autore with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Autore[] findByGroup_PrevAndNext(
		long autoreId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchAutoreException;

	/**
	* Removes all the autores where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of autores where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching autores
	* @throws SystemException if a system exception occurred
	*/
	public int countByGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the autores where groupId = &#63; and cognome LIKE &#63;.
	*
	* @param groupId the group ID
	* @param cognome the cognome
	* @return the matching autores
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Autore> findByG_C(
		long groupId, java.lang.String cognome)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the autores where groupId = &#63; and cognome LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param cognome the cognome
	* @param start the lower bound of the range of autores
	* @param end the upper bound of the range of autores (not inclusive)
	* @return the range of matching autores
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Autore> findByG_C(
		long groupId, java.lang.String cognome, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the autores where groupId = &#63; and cognome LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param cognome the cognome
	* @param start the lower bound of the range of autores
	* @param end the upper bound of the range of autores (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching autores
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Autore> findByG_C(
		long groupId, java.lang.String cognome, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first autore in the ordered set where groupId = &#63; and cognome LIKE &#63;.
	*
	* @param groupId the group ID
	* @param cognome the cognome
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching autore
	* @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a matching autore could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Autore findByG_C_First(
		long groupId, java.lang.String cognome,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchAutoreException;

	/**
	* Returns the first autore in the ordered set where groupId = &#63; and cognome LIKE &#63;.
	*
	* @param groupId the group ID
	* @param cognome the cognome
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching autore, or <code>null</code> if a matching autore could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Autore fetchByG_C_First(
		long groupId, java.lang.String cognome,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last autore in the ordered set where groupId = &#63; and cognome LIKE &#63;.
	*
	* @param groupId the group ID
	* @param cognome the cognome
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching autore
	* @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a matching autore could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Autore findByG_C_Last(
		long groupId, java.lang.String cognome,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchAutoreException;

	/**
	* Returns the last autore in the ordered set where groupId = &#63; and cognome LIKE &#63;.
	*
	* @param groupId the group ID
	* @param cognome the cognome
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching autore, or <code>null</code> if a matching autore could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Autore fetchByG_C_Last(
		long groupId, java.lang.String cognome,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the autores before and after the current autore in the ordered set where groupId = &#63; and cognome LIKE &#63;.
	*
	* @param autoreId the primary key of the current autore
	* @param groupId the group ID
	* @param cognome the cognome
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next autore
	* @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a autore with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Autore[] findByG_C_PrevAndNext(
		long autoreId, long groupId, java.lang.String cognome,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchAutoreException;

	/**
	* Removes all the autores where groupId = &#63; and cognome LIKE &#63; from the database.
	*
	* @param groupId the group ID
	* @param cognome the cognome
	* @throws SystemException if a system exception occurred
	*/
	public void removeByG_C(long groupId, java.lang.String cognome)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of autores where groupId = &#63; and cognome LIKE &#63;.
	*
	* @param groupId the group ID
	* @param cognome the cognome
	* @return the number of matching autores
	* @throws SystemException if a system exception occurred
	*/
	public int countByG_C(long groupId, java.lang.String cognome)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the autore in the entity cache if it is enabled.
	*
	* @param autore the autore
	*/
	public void cacheResult(
		it.planetek.liferay.service.builder.model.Autore autore);

	/**
	* Caches the autores in the entity cache if it is enabled.
	*
	* @param autores the autores
	*/
	public void cacheResult(
		java.util.List<it.planetek.liferay.service.builder.model.Autore> autores);

	/**
	* Creates a new autore with the primary key. Does not add the autore to the database.
	*
	* @param autoreId the primary key for the new autore
	* @return the new autore
	*/
	public it.planetek.liferay.service.builder.model.Autore create(
		long autoreId);

	/**
	* Removes the autore with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param autoreId the primary key of the autore
	* @return the autore that was removed
	* @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a autore with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Autore remove(
		long autoreId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchAutoreException;

	public it.planetek.liferay.service.builder.model.Autore updateImpl(
		it.planetek.liferay.service.builder.model.Autore autore)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the autore with the primary key or throws a {@link it.planetek.liferay.service.builder.NoSuchAutoreException} if it could not be found.
	*
	* @param autoreId the primary key of the autore
	* @return the autore
	* @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a autore with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Autore findByPrimaryKey(
		long autoreId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchAutoreException;

	/**
	* Returns the autore with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param autoreId the primary key of the autore
	* @return the autore, or <code>null</code> if a autore with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Autore fetchByPrimaryKey(
		long autoreId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the autores.
	*
	* @return the autores
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Autore> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the autores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of autores
	* @param end the upper bound of the range of autores (not inclusive)
	* @return the range of autores
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Autore> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the autores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of autores
	* @param end the upper bound of the range of autores (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of autores
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Autore> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the autores from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of autores.
	*
	* @return the number of autores
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}