/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model;

import com.liferay.portal.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Libro}.
 * </p>
 *
 * @author jfrancia
 * @see Libro
 * @generated
 */
public class LibroWrapper implements Libro, ModelWrapper<Libro> {
	public LibroWrapper(Libro libro) {
		_libro = libro;
	}

	@Override
	public Class<?> getModelClass() {
		return Libro.class;
	}

	@Override
	public String getModelClassName() {
		return Libro.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("libroId", getLibroId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("nome", getNome());
		attributes.put("pubblicato", getPubblicato());
		attributes.put("numeroPagine", getNumeroPagine());
		attributes.put("dataPubblicazione", getDataPubblicazione());
		attributes.put("descrizione", getDescrizione());
		attributes.put("isbn", getIsbn());
		attributes.put("status", getStatus());
		attributes.put("statusByUserId", getStatusByUserId());
		attributes.put("statusByUserName", getStatusByUserName());
		attributes.put("statusDate", getStatusDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long libroId = (Long)attributes.get("libroId");

		if (libroId != null) {
			setLibroId(libroId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String nome = (String)attributes.get("nome");

		if (nome != null) {
			setNome(nome);
		}

		Boolean pubblicato = (Boolean)attributes.get("pubblicato");

		if (pubblicato != null) {
			setPubblicato(pubblicato);
		}

		Integer numeroPagine = (Integer)attributes.get("numeroPagine");

		if (numeroPagine != null) {
			setNumeroPagine(numeroPagine);
		}

		Date dataPubblicazione = (Date)attributes.get("dataPubblicazione");

		if (dataPubblicazione != null) {
			setDataPubblicazione(dataPubblicazione);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		String isbn = (String)attributes.get("isbn");

		if (isbn != null) {
			setIsbn(isbn);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long statusByUserId = (Long)attributes.get("statusByUserId");

		if (statusByUserId != null) {
			setStatusByUserId(statusByUserId);
		}

		String statusByUserName = (String)attributes.get("statusByUserName");

		if (statusByUserName != null) {
			setStatusByUserName(statusByUserName);
		}

		Date statusDate = (Date)attributes.get("statusDate");

		if (statusDate != null) {
			setStatusDate(statusDate);
		}
	}

	/**
	* Returns the primary key of this libro.
	*
	* @return the primary key of this libro
	*/
	@Override
	public long getPrimaryKey() {
		return _libro.getPrimaryKey();
	}

	/**
	* Sets the primary key of this libro.
	*
	* @param primaryKey the primary key of this libro
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_libro.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the uuid of this libro.
	*
	* @return the uuid of this libro
	*/
	@Override
	public java.lang.String getUuid() {
		return _libro.getUuid();
	}

	/**
	* Sets the uuid of this libro.
	*
	* @param uuid the uuid of this libro
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_libro.setUuid(uuid);
	}

	/**
	* Returns the libro ID of this libro.
	*
	* @return the libro ID of this libro
	*/
	@Override
	public long getLibroId() {
		return _libro.getLibroId();
	}

	/**
	* Sets the libro ID of this libro.
	*
	* @param libroId the libro ID of this libro
	*/
	@Override
	public void setLibroId(long libroId) {
		_libro.setLibroId(libroId);
	}

	/**
	* Returns the company ID of this libro.
	*
	* @return the company ID of this libro
	*/
	@Override
	public long getCompanyId() {
		return _libro.getCompanyId();
	}

	/**
	* Sets the company ID of this libro.
	*
	* @param companyId the company ID of this libro
	*/
	@Override
	public void setCompanyId(long companyId) {
		_libro.setCompanyId(companyId);
	}

	/**
	* Returns the group ID of this libro.
	*
	* @return the group ID of this libro
	*/
	@Override
	public long getGroupId() {
		return _libro.getGroupId();
	}

	/**
	* Sets the group ID of this libro.
	*
	* @param groupId the group ID of this libro
	*/
	@Override
	public void setGroupId(long groupId) {
		_libro.setGroupId(groupId);
	}

	/**
	* Returns the user ID of this libro.
	*
	* @return the user ID of this libro
	*/
	@Override
	public long getUserId() {
		return _libro.getUserId();
	}

	/**
	* Sets the user ID of this libro.
	*
	* @param userId the user ID of this libro
	*/
	@Override
	public void setUserId(long userId) {
		_libro.setUserId(userId);
	}

	/**
	* Returns the user uuid of this libro.
	*
	* @return the user uuid of this libro
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _libro.getUserUuid();
	}

	/**
	* Sets the user uuid of this libro.
	*
	* @param userUuid the user uuid of this libro
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_libro.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this libro.
	*
	* @return the user name of this libro
	*/
	@Override
	public java.lang.String getUserName() {
		return _libro.getUserName();
	}

	/**
	* Sets the user name of this libro.
	*
	* @param userName the user name of this libro
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_libro.setUserName(userName);
	}

	/**
	* Returns the create date of this libro.
	*
	* @return the create date of this libro
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _libro.getCreateDate();
	}

	/**
	* Sets the create date of this libro.
	*
	* @param createDate the create date of this libro
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_libro.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this libro.
	*
	* @return the modified date of this libro
	*/
	@Override
	public java.util.Date getModifiedDate() {
		return _libro.getModifiedDate();
	}

	/**
	* Sets the modified date of this libro.
	*
	* @param modifiedDate the modified date of this libro
	*/
	@Override
	public void setModifiedDate(java.util.Date modifiedDate) {
		_libro.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the nome of this libro.
	*
	* @return the nome of this libro
	*/
	@Override
	public java.lang.String getNome() {
		return _libro.getNome();
	}

	/**
	* Sets the nome of this libro.
	*
	* @param nome the nome of this libro
	*/
	@Override
	public void setNome(java.lang.String nome) {
		_libro.setNome(nome);
	}

	/**
	* Returns the pubblicato of this libro.
	*
	* @return the pubblicato of this libro
	*/
	@Override
	public boolean getPubblicato() {
		return _libro.getPubblicato();
	}

	/**
	* Returns <code>true</code> if this libro is pubblicato.
	*
	* @return <code>true</code> if this libro is pubblicato; <code>false</code> otherwise
	*/
	@Override
	public boolean isPubblicato() {
		return _libro.isPubblicato();
	}

	/**
	* Sets whether this libro is pubblicato.
	*
	* @param pubblicato the pubblicato of this libro
	*/
	@Override
	public void setPubblicato(boolean pubblicato) {
		_libro.setPubblicato(pubblicato);
	}

	/**
	* Returns the numero pagine of this libro.
	*
	* @return the numero pagine of this libro
	*/
	@Override
	public int getNumeroPagine() {
		return _libro.getNumeroPagine();
	}

	/**
	* Sets the numero pagine of this libro.
	*
	* @param numeroPagine the numero pagine of this libro
	*/
	@Override
	public void setNumeroPagine(int numeroPagine) {
		_libro.setNumeroPagine(numeroPagine);
	}

	/**
	* Returns the data pubblicazione of this libro.
	*
	* @return the data pubblicazione of this libro
	*/
	@Override
	public java.util.Date getDataPubblicazione() {
		return _libro.getDataPubblicazione();
	}

	/**
	* Sets the data pubblicazione of this libro.
	*
	* @param dataPubblicazione the data pubblicazione of this libro
	*/
	@Override
	public void setDataPubblicazione(java.util.Date dataPubblicazione) {
		_libro.setDataPubblicazione(dataPubblicazione);
	}

	/**
	* Returns the descrizione of this libro.
	*
	* @return the descrizione of this libro
	*/
	@Override
	public java.lang.String getDescrizione() {
		return _libro.getDescrizione();
	}

	/**
	* Sets the descrizione of this libro.
	*
	* @param descrizione the descrizione of this libro
	*/
	@Override
	public void setDescrizione(java.lang.String descrizione) {
		_libro.setDescrizione(descrizione);
	}

	/**
	* Returns the isbn of this libro.
	*
	* @return the isbn of this libro
	*/
	@Override
	public java.lang.String getIsbn() {
		return _libro.getIsbn();
	}

	/**
	* Sets the isbn of this libro.
	*
	* @param isbn the isbn of this libro
	*/
	@Override
	public void setIsbn(java.lang.String isbn) {
		_libro.setIsbn(isbn);
	}

	/**
	* Returns the status of this libro.
	*
	* @return the status of this libro
	*/
	@Override
	public int getStatus() {
		return _libro.getStatus();
	}

	/**
	* Sets the status of this libro.
	*
	* @param status the status of this libro
	*/
	@Override
	public void setStatus(int status) {
		_libro.setStatus(status);
	}

	/**
	* Returns the status by user ID of this libro.
	*
	* @return the status by user ID of this libro
	*/
	@Override
	public long getStatusByUserId() {
		return _libro.getStatusByUserId();
	}

	/**
	* Sets the status by user ID of this libro.
	*
	* @param statusByUserId the status by user ID of this libro
	*/
	@Override
	public void setStatusByUserId(long statusByUserId) {
		_libro.setStatusByUserId(statusByUserId);
	}

	/**
	* Returns the status by user uuid of this libro.
	*
	* @return the status by user uuid of this libro
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getStatusByUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _libro.getStatusByUserUuid();
	}

	/**
	* Sets the status by user uuid of this libro.
	*
	* @param statusByUserUuid the status by user uuid of this libro
	*/
	@Override
	public void setStatusByUserUuid(java.lang.String statusByUserUuid) {
		_libro.setStatusByUserUuid(statusByUserUuid);
	}

	/**
	* Returns the status by user name of this libro.
	*
	* @return the status by user name of this libro
	*/
	@Override
	public java.lang.String getStatusByUserName() {
		return _libro.getStatusByUserName();
	}

	/**
	* Sets the status by user name of this libro.
	*
	* @param statusByUserName the status by user name of this libro
	*/
	@Override
	public void setStatusByUserName(java.lang.String statusByUserName) {
		_libro.setStatusByUserName(statusByUserName);
	}

	/**
	* Returns the status date of this libro.
	*
	* @return the status date of this libro
	*/
	@Override
	public java.util.Date getStatusDate() {
		return _libro.getStatusDate();
	}

	/**
	* Sets the status date of this libro.
	*
	* @param statusDate the status date of this libro
	*/
	@Override
	public void setStatusDate(java.util.Date statusDate) {
		_libro.setStatusDate(statusDate);
	}

	/**
	* @deprecated As of 6.1.0, replaced by {@link #isApproved()}
	*/
	@Override
	public boolean getApproved() {
		return _libro.getApproved();
	}

	/**
	* Returns <code>true</code> if this libro is approved.
	*
	* @return <code>true</code> if this libro is approved; <code>false</code> otherwise
	*/
	@Override
	public boolean isApproved() {
		return _libro.isApproved();
	}

	/**
	* Returns <code>true</code> if this libro is denied.
	*
	* @return <code>true</code> if this libro is denied; <code>false</code> otherwise
	*/
	@Override
	public boolean isDenied() {
		return _libro.isDenied();
	}

	/**
	* Returns <code>true</code> if this libro is a draft.
	*
	* @return <code>true</code> if this libro is a draft; <code>false</code> otherwise
	*/
	@Override
	public boolean isDraft() {
		return _libro.isDraft();
	}

	/**
	* Returns <code>true</code> if this libro is expired.
	*
	* @return <code>true</code> if this libro is expired; <code>false</code> otherwise
	*/
	@Override
	public boolean isExpired() {
		return _libro.isExpired();
	}

	/**
	* Returns <code>true</code> if this libro is inactive.
	*
	* @return <code>true</code> if this libro is inactive; <code>false</code> otherwise
	*/
	@Override
	public boolean isInactive() {
		return _libro.isInactive();
	}

	/**
	* Returns <code>true</code> if this libro is incomplete.
	*
	* @return <code>true</code> if this libro is incomplete; <code>false</code> otherwise
	*/
	@Override
	public boolean isIncomplete() {
		return _libro.isIncomplete();
	}

	/**
	* Returns <code>true</code> if this libro is pending.
	*
	* @return <code>true</code> if this libro is pending; <code>false</code> otherwise
	*/
	@Override
	public boolean isPending() {
		return _libro.isPending();
	}

	/**
	* Returns <code>true</code> if this libro is scheduled.
	*
	* @return <code>true</code> if this libro is scheduled; <code>false</code> otherwise
	*/
	@Override
	public boolean isScheduled() {
		return _libro.isScheduled();
	}

	@Override
	public boolean isNew() {
		return _libro.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_libro.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _libro.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_libro.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _libro.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _libro.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_libro.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _libro.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_libro.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_libro.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_libro.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new LibroWrapper((Libro)_libro.clone());
	}

	@Override
	public int compareTo(it.planetek.liferay.service.builder.model.Libro libro) {
		return _libro.compareTo(libro);
	}

	@Override
	public int hashCode() {
		return _libro.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.planetek.liferay.service.builder.model.Libro> toCacheModel() {
		return _libro.toCacheModel();
	}

	@Override
	public it.planetek.liferay.service.builder.model.Libro toEscapedModel() {
		return new LibroWrapper(_libro.toEscapedModel());
	}

	@Override
	public it.planetek.liferay.service.builder.model.Libro toUnescapedModel() {
		return new LibroWrapper(_libro.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _libro.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _libro.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_libro.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LibroWrapper)) {
			return false;
		}

		LibroWrapper libroWrapper = (LibroWrapper)obj;

		if (Validator.equals(_libro, libroWrapper._libro)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _libro.getStagedModelType();
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Libro getWrappedLibro() {
		return _libro;
	}

	@Override
	public Libro getWrappedModel() {
		return _libro;
	}

	@Override
	public void resetOriginalValues() {
		_libro.resetOriginalValues();
	}

	private Libro _libro;
}