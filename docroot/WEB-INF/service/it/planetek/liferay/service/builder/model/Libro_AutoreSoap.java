/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model;

import it.planetek.liferay.service.builder.service.persistence.Libro_AutorePK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author jfrancia
 * @generated
 */
public class Libro_AutoreSoap implements Serializable {
	public static Libro_AutoreSoap toSoapModel(Libro_Autore model) {
		Libro_AutoreSoap soapModel = new Libro_AutoreSoap();

		soapModel.setLibroId(model.getLibroId());
		soapModel.setAutoreId(model.getAutoreId());

		return soapModel;
	}

	public static Libro_AutoreSoap[] toSoapModels(Libro_Autore[] models) {
		Libro_AutoreSoap[] soapModels = new Libro_AutoreSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static Libro_AutoreSoap[][] toSoapModels(Libro_Autore[][] models) {
		Libro_AutoreSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new Libro_AutoreSoap[models.length][models[0].length];
		}
		else {
			soapModels = new Libro_AutoreSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static Libro_AutoreSoap[] toSoapModels(List<Libro_Autore> models) {
		List<Libro_AutoreSoap> soapModels = new ArrayList<Libro_AutoreSoap>(models.size());

		for (Libro_Autore model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new Libro_AutoreSoap[soapModels.size()]);
	}

	public Libro_AutoreSoap() {
	}

	public Libro_AutorePK getPrimaryKey() {
		return new Libro_AutorePK(_libroId, _autoreId);
	}

	public void setPrimaryKey(Libro_AutorePK pk) {
		setLibroId(pk.libroId);
		setAutoreId(pk.autoreId);
	}

	public long getLibroId() {
		return _libroId;
	}

	public void setLibroId(long libroId) {
		_libroId = libroId;
	}

	public long getAutoreId() {
		return _autoreId;
	}

	public void setAutoreId(long autoreId) {
		_autoreId = autoreId;
	}

	private long _libroId;
	private long _autoreId;
}