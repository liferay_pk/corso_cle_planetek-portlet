/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.planetek.liferay.service.builder.service.http.LibroServiceSoap}.
 *
 * @author jfrancia
 * @see it.planetek.liferay.service.builder.service.http.LibroServiceSoap
 * @generated
 */
public class LibroSoap implements Serializable {
	public static LibroSoap toSoapModel(Libro model) {
		LibroSoap soapModel = new LibroSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setLibroId(model.getLibroId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setNome(model.getNome());
		soapModel.setPubblicato(model.getPubblicato());
		soapModel.setNumeroPagine(model.getNumeroPagine());
		soapModel.setDataPubblicazione(model.getDataPubblicazione());
		soapModel.setDescrizione(model.getDescrizione());
		soapModel.setIsbn(model.getIsbn());
		soapModel.setStatus(model.getStatus());
		soapModel.setStatusByUserId(model.getStatusByUserId());
		soapModel.setStatusByUserName(model.getStatusByUserName());
		soapModel.setStatusDate(model.getStatusDate());

		return soapModel;
	}

	public static LibroSoap[] toSoapModels(Libro[] models) {
		LibroSoap[] soapModels = new LibroSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LibroSoap[][] toSoapModels(Libro[][] models) {
		LibroSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LibroSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LibroSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LibroSoap[] toSoapModels(List<Libro> models) {
		List<LibroSoap> soapModels = new ArrayList<LibroSoap>(models.size());

		for (Libro model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LibroSoap[soapModels.size()]);
	}

	public LibroSoap() {
	}

	public long getPrimaryKey() {
		return _libroId;
	}

	public void setPrimaryKey(long pk) {
		setLibroId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getLibroId() {
		return _libroId;
	}

	public void setLibroId(long libroId) {
		_libroId = libroId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getNome() {
		return _nome;
	}

	public void setNome(String nome) {
		_nome = nome;
	}

	public boolean getPubblicato() {
		return _pubblicato;
	}

	public boolean isPubblicato() {
		return _pubblicato;
	}

	public void setPubblicato(boolean pubblicato) {
		_pubblicato = pubblicato;
	}

	public int getNumeroPagine() {
		return _numeroPagine;
	}

	public void setNumeroPagine(int numeroPagine) {
		_numeroPagine = numeroPagine;
	}

	public Date getDataPubblicazione() {
		return _dataPubblicazione;
	}

	public void setDataPubblicazione(Date dataPubblicazione) {
		_dataPubblicazione = dataPubblicazione;
	}

	public String getDescrizione() {
		return _descrizione;
	}

	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;
	}

	public String getIsbn() {
		return _isbn;
	}

	public void setIsbn(String isbn) {
		_isbn = isbn;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public long getStatusByUserId() {
		return _statusByUserId;
	}

	public void setStatusByUserId(long statusByUserId) {
		_statusByUserId = statusByUserId;
	}

	public String getStatusByUserName() {
		return _statusByUserName;
	}

	public void setStatusByUserName(String statusByUserName) {
		_statusByUserName = statusByUserName;
	}

	public Date getStatusDate() {
		return _statusDate;
	}

	public void setStatusDate(Date statusDate) {
		_statusDate = statusDate;
	}

	private String _uuid;
	private long _libroId;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _nome;
	private boolean _pubblicato;
	private int _numeroPagine;
	private Date _dataPubblicazione;
	private String _descrizione;
	private String _isbn;
	private int _status;
	private long _statusByUserId;
	private String _statusByUserName;
	private Date _statusDate;
}