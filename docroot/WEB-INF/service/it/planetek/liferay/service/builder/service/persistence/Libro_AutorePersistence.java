/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.planetek.liferay.service.builder.model.Libro_Autore;

/**
 * The persistence interface for the libro_ autore service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author jfrancia
 * @see Libro_AutorePersistenceImpl
 * @see Libro_AutoreUtil
 * @generated
 */
public interface Libro_AutorePersistence extends BasePersistence<Libro_Autore> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link Libro_AutoreUtil} to access the libro_ autore persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the libro_ autore in the entity cache if it is enabled.
	*
	* @param libro_Autore the libro_ autore
	*/
	public void cacheResult(
		it.planetek.liferay.service.builder.model.Libro_Autore libro_Autore);

	/**
	* Caches the libro_ autores in the entity cache if it is enabled.
	*
	* @param libro_Autores the libro_ autores
	*/
	public void cacheResult(
		java.util.List<it.planetek.liferay.service.builder.model.Libro_Autore> libro_Autores);

	/**
	* Creates a new libro_ autore with the primary key. Does not add the libro_ autore to the database.
	*
	* @param libro_AutorePK the primary key for the new libro_ autore
	* @return the new libro_ autore
	*/
	public it.planetek.liferay.service.builder.model.Libro_Autore create(
		it.planetek.liferay.service.builder.service.persistence.Libro_AutorePK libro_AutorePK);

	/**
	* Removes the libro_ autore with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param libro_AutorePK the primary key of the libro_ autore
	* @return the libro_ autore that was removed
	* @throws it.planetek.liferay.service.builder.NoSuchLibro_AutoreException if a libro_ autore with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro_Autore remove(
		it.planetek.liferay.service.builder.service.persistence.Libro_AutorePK libro_AutorePK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibro_AutoreException;

	public it.planetek.liferay.service.builder.model.Libro_Autore updateImpl(
		it.planetek.liferay.service.builder.model.Libro_Autore libro_Autore)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the libro_ autore with the primary key or throws a {@link it.planetek.liferay.service.builder.NoSuchLibro_AutoreException} if it could not be found.
	*
	* @param libro_AutorePK the primary key of the libro_ autore
	* @return the libro_ autore
	* @throws it.planetek.liferay.service.builder.NoSuchLibro_AutoreException if a libro_ autore with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro_Autore findByPrimaryKey(
		it.planetek.liferay.service.builder.service.persistence.Libro_AutorePK libro_AutorePK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.planetek.liferay.service.builder.NoSuchLibro_AutoreException;

	/**
	* Returns the libro_ autore with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param libro_AutorePK the primary key of the libro_ autore
	* @return the libro_ autore, or <code>null</code> if a libro_ autore with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.planetek.liferay.service.builder.model.Libro_Autore fetchByPrimaryKey(
		it.planetek.liferay.service.builder.service.persistence.Libro_AutorePK libro_AutorePK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the libro_ autores.
	*
	* @return the libro_ autores
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro_Autore> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the libro_ autores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.Libro_AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of libro_ autores
	* @param end the upper bound of the range of libro_ autores (not inclusive)
	* @return the range of libro_ autores
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro_Autore> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the libro_ autores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.Libro_AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of libro_ autores
	* @param end the upper bound of the range of libro_ autores (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of libro_ autores
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.planetek.liferay.service.builder.model.Libro_Autore> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the libro_ autores from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of libro_ autores.
	*
	* @return the number of libro_ autores
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}