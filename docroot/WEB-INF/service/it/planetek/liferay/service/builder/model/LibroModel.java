/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.StagedGroupedModel;
import com.liferay.portal.model.WorkflowedModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the Libro service. Represents a row in the &quot;planetek_Libro&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link it.planetek.liferay.service.builder.model.impl.LibroModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link it.planetek.liferay.service.builder.model.impl.LibroImpl}.
 * </p>
 *
 * @author jfrancia
 * @see Libro
 * @see it.planetek.liferay.service.builder.model.impl.LibroImpl
 * @see it.planetek.liferay.service.builder.model.impl.LibroModelImpl
 * @generated
 */
public interface LibroModel extends BaseModel<Libro>, StagedGroupedModel,
	WorkflowedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a libro model instance should use the {@link Libro} interface instead.
	 */

	/**
	 * Returns the primary key of this libro.
	 *
	 * @return the primary key of this libro
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this libro.
	 *
	 * @param primaryKey the primary key of this libro
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the uuid of this libro.
	 *
	 * @return the uuid of this libro
	 */
	@AutoEscape
	@Override
	public String getUuid();

	/**
	 * Sets the uuid of this libro.
	 *
	 * @param uuid the uuid of this libro
	 */
	@Override
	public void setUuid(String uuid);

	/**
	 * Returns the libro ID of this libro.
	 *
	 * @return the libro ID of this libro
	 */
	public long getLibroId();

	/**
	 * Sets the libro ID of this libro.
	 *
	 * @param libroId the libro ID of this libro
	 */
	public void setLibroId(long libroId);

	/**
	 * Returns the company ID of this libro.
	 *
	 * @return the company ID of this libro
	 */
	@Override
	public long getCompanyId();

	/**
	 * Sets the company ID of this libro.
	 *
	 * @param companyId the company ID of this libro
	 */
	@Override
	public void setCompanyId(long companyId);

	/**
	 * Returns the group ID of this libro.
	 *
	 * @return the group ID of this libro
	 */
	@Override
	public long getGroupId();

	/**
	 * Sets the group ID of this libro.
	 *
	 * @param groupId the group ID of this libro
	 */
	@Override
	public void setGroupId(long groupId);

	/**
	 * Returns the user ID of this libro.
	 *
	 * @return the user ID of this libro
	 */
	@Override
	public long getUserId();

	/**
	 * Sets the user ID of this libro.
	 *
	 * @param userId the user ID of this libro
	 */
	@Override
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this libro.
	 *
	 * @return the user uuid of this libro
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public String getUserUuid() throws SystemException;

	/**
	 * Sets the user uuid of this libro.
	 *
	 * @param userUuid the user uuid of this libro
	 */
	@Override
	public void setUserUuid(String userUuid);

	/**
	 * Returns the user name of this libro.
	 *
	 * @return the user name of this libro
	 */
	@AutoEscape
	@Override
	public String getUserName();

	/**
	 * Sets the user name of this libro.
	 *
	 * @param userName the user name of this libro
	 */
	@Override
	public void setUserName(String userName);

	/**
	 * Returns the create date of this libro.
	 *
	 * @return the create date of this libro
	 */
	@Override
	public Date getCreateDate();

	/**
	 * Sets the create date of this libro.
	 *
	 * @param createDate the create date of this libro
	 */
	@Override
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this libro.
	 *
	 * @return the modified date of this libro
	 */
	@Override
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this libro.
	 *
	 * @param modifiedDate the modified date of this libro
	 */
	@Override
	public void setModifiedDate(Date modifiedDate);

	/**
	 * Returns the nome of this libro.
	 *
	 * @return the nome of this libro
	 */
	@AutoEscape
	public String getNome();

	/**
	 * Sets the nome of this libro.
	 *
	 * @param nome the nome of this libro
	 */
	public void setNome(String nome);

	/**
	 * Returns the pubblicato of this libro.
	 *
	 * @return the pubblicato of this libro
	 */
	public boolean getPubblicato();

	/**
	 * Returns <code>true</code> if this libro is pubblicato.
	 *
	 * @return <code>true</code> if this libro is pubblicato; <code>false</code> otherwise
	 */
	public boolean isPubblicato();

	/**
	 * Sets whether this libro is pubblicato.
	 *
	 * @param pubblicato the pubblicato of this libro
	 */
	public void setPubblicato(boolean pubblicato);

	/**
	 * Returns the numero pagine of this libro.
	 *
	 * @return the numero pagine of this libro
	 */
	public int getNumeroPagine();

	/**
	 * Sets the numero pagine of this libro.
	 *
	 * @param numeroPagine the numero pagine of this libro
	 */
	public void setNumeroPagine(int numeroPagine);

	/**
	 * Returns the data pubblicazione of this libro.
	 *
	 * @return the data pubblicazione of this libro
	 */
	public Date getDataPubblicazione();

	/**
	 * Sets the data pubblicazione of this libro.
	 *
	 * @param dataPubblicazione the data pubblicazione of this libro
	 */
	public void setDataPubblicazione(Date dataPubblicazione);

	/**
	 * Returns the descrizione of this libro.
	 *
	 * @return the descrizione of this libro
	 */
	@AutoEscape
	public String getDescrizione();

	/**
	 * Sets the descrizione of this libro.
	 *
	 * @param descrizione the descrizione of this libro
	 */
	public void setDescrizione(String descrizione);

	/**
	 * Returns the isbn of this libro.
	 *
	 * @return the isbn of this libro
	 */
	@AutoEscape
	public String getIsbn();

	/**
	 * Sets the isbn of this libro.
	 *
	 * @param isbn the isbn of this libro
	 */
	public void setIsbn(String isbn);

	/**
	 * Returns the status of this libro.
	 *
	 * @return the status of this libro
	 */
	@Override
	public int getStatus();

	/**
	 * Sets the status of this libro.
	 *
	 * @param status the status of this libro
	 */
	@Override
	public void setStatus(int status);

	/**
	 * Returns the status by user ID of this libro.
	 *
	 * @return the status by user ID of this libro
	 */
	@Override
	public long getStatusByUserId();

	/**
	 * Sets the status by user ID of this libro.
	 *
	 * @param statusByUserId the status by user ID of this libro
	 */
	@Override
	public void setStatusByUserId(long statusByUserId);

	/**
	 * Returns the status by user uuid of this libro.
	 *
	 * @return the status by user uuid of this libro
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public String getStatusByUserUuid() throws SystemException;

	/**
	 * Sets the status by user uuid of this libro.
	 *
	 * @param statusByUserUuid the status by user uuid of this libro
	 */
	@Override
	public void setStatusByUserUuid(String statusByUserUuid);

	/**
	 * Returns the status by user name of this libro.
	 *
	 * @return the status by user name of this libro
	 */
	@AutoEscape
	@Override
	public String getStatusByUserName();

	/**
	 * Sets the status by user name of this libro.
	 *
	 * @param statusByUserName the status by user name of this libro
	 */
	@Override
	public void setStatusByUserName(String statusByUserName);

	/**
	 * Returns the status date of this libro.
	 *
	 * @return the status date of this libro
	 */
	@Override
	public Date getStatusDate();

	/**
	 * Sets the status date of this libro.
	 *
	 * @param statusDate the status date of this libro
	 */
	@Override
	public void setStatusDate(Date statusDate);

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #isApproved()}
	 */
	@Override
	public boolean getApproved();

	/**
	 * Returns <code>true</code> if this libro is approved.
	 *
	 * @return <code>true</code> if this libro is approved; <code>false</code> otherwise
	 */
	@Override
	public boolean isApproved();

	/**
	 * Returns <code>true</code> if this libro is denied.
	 *
	 * @return <code>true</code> if this libro is denied; <code>false</code> otherwise
	 */
	@Override
	public boolean isDenied();

	/**
	 * Returns <code>true</code> if this libro is a draft.
	 *
	 * @return <code>true</code> if this libro is a draft; <code>false</code> otherwise
	 */
	@Override
	public boolean isDraft();

	/**
	 * Returns <code>true</code> if this libro is expired.
	 *
	 * @return <code>true</code> if this libro is expired; <code>false</code> otherwise
	 */
	@Override
	public boolean isExpired();

	/**
	 * Returns <code>true</code> if this libro is inactive.
	 *
	 * @return <code>true</code> if this libro is inactive; <code>false</code> otherwise
	 */
	@Override
	public boolean isInactive();

	/**
	 * Returns <code>true</code> if this libro is incomplete.
	 *
	 * @return <code>true</code> if this libro is incomplete; <code>false</code> otherwise
	 */
	@Override
	public boolean isIncomplete();

	/**
	 * Returns <code>true</code> if this libro is pending.
	 *
	 * @return <code>true</code> if this libro is pending; <code>false</code> otherwise
	 */
	@Override
	public boolean isPending();

	/**
	 * Returns <code>true</code> if this libro is scheduled.
	 *
	 * @return <code>true</code> if this libro is scheduled; <code>false</code> otherwise
	 */
	@Override
	public boolean isScheduled();

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(Libro libro);

	@Override
	public int hashCode();

	@Override
	public CacheModel<Libro> toCacheModel();

	@Override
	public Libro toEscapedModel();

	@Override
	public Libro toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}