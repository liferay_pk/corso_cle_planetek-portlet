/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import it.planetek.liferay.service.builder.model.AutoreClp;
import it.planetek.liferay.service.builder.model.LibreriaClp;
import it.planetek.liferay.service.builder.model.LibroClp;
import it.planetek.liferay.service.builder.model.Libro_AutoreClp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jfrancia
 */
public class ClpSerializer {
	public static String getServletContextName() {
		if (Validator.isNotNull(_servletContextName)) {
			return _servletContextName;
		}

		synchronized (ClpSerializer.class) {
			if (Validator.isNotNull(_servletContextName)) {
				return _servletContextName;
			}

			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Class<?> portletPropsClass = classLoader.loadClass(
						"com.liferay.util.portlet.PortletProps");

				Method getMethod = portletPropsClass.getMethod("get",
						new Class<?>[] { String.class });

				String portletPropsServletContextName = (String)getMethod.invoke(null,
						"corso_cle_planetek-portlet-deployment-context");

				if (Validator.isNotNull(portletPropsServletContextName)) {
					_servletContextName = portletPropsServletContextName;
				}
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info(
						"Unable to locate deployment context from portlet properties");
				}
			}

			if (Validator.isNull(_servletContextName)) {
				try {
					String propsUtilServletContextName = PropsUtil.get(
							"corso_cle_planetek-portlet-deployment-context");

					if (Validator.isNotNull(propsUtilServletContextName)) {
						_servletContextName = propsUtilServletContextName;
					}
				}
				catch (Throwable t) {
					if (_log.isInfoEnabled()) {
						_log.info(
							"Unable to locate deployment context from portal properties");
					}
				}
			}

			if (Validator.isNull(_servletContextName)) {
				_servletContextName = "corso_cle_planetek-portlet";
			}

			return _servletContextName;
		}
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(AutoreClp.class.getName())) {
			return translateInputAutore(oldModel);
		}

		if (oldModelClassName.equals(LibreriaClp.class.getName())) {
			return translateInputLibreria(oldModel);
		}

		if (oldModelClassName.equals(LibroClp.class.getName())) {
			return translateInputLibro(oldModel);
		}

		if (oldModelClassName.equals(Libro_AutoreClp.class.getName())) {
			return translateInputLibro_Autore(oldModel);
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInputAutore(BaseModel<?> oldModel) {
		AutoreClp oldClpModel = (AutoreClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAutoreRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLibreria(BaseModel<?> oldModel) {
		LibreriaClp oldClpModel = (LibreriaClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLibreriaRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLibro(BaseModel<?> oldModel) {
		LibroClp oldClpModel = (LibroClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLibroRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLibro_Autore(BaseModel<?> oldModel) {
		Libro_AutoreClp oldClpModel = (Libro_AutoreClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLibro_AutoreRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(
					"it.planetek.liferay.service.builder.model.impl.AutoreImpl")) {
			return translateOutputAutore(oldModel);
		}

		if (oldModelClassName.equals(
					"it.planetek.liferay.service.builder.model.impl.LibreriaImpl")) {
			return translateOutputLibreria(oldModel);
		}

		if (oldModelClassName.equals(
					"it.planetek.liferay.service.builder.model.impl.LibroImpl")) {
			return translateOutputLibro(oldModel);
		}

		if (oldModelClassName.equals(
					"it.planetek.liferay.service.builder.model.impl.Libro_AutoreImpl")) {
			return translateOutputLibro_Autore(oldModel);
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Throwable translateThrowable(Throwable throwable) {
		if (_useReflectionToTranslateThrowable) {
			try {
				UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

				objectOutputStream.writeObject(throwable);

				objectOutputStream.flush();
				objectOutputStream.close();

				UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
						0, unsyncByteArrayOutputStream.size());

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader = currentThread.getContextClassLoader();

				ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
						contextClassLoader);

				throwable = (Throwable)objectInputStream.readObject();

				objectInputStream.close();

				return throwable;
			}
			catch (SecurityException se) {
				if (_log.isInfoEnabled()) {
					_log.info("Do not use reflection to translate throwable");
				}

				_useReflectionToTranslateThrowable = false;
			}
			catch (Throwable throwable2) {
				_log.error(throwable2, throwable2);

				return throwable2;
			}
		}

		Class<?> clazz = throwable.getClass();

		String className = clazz.getName();

		if (className.equals(PortalException.class.getName())) {
			return new PortalException();
		}

		if (className.equals(SystemException.class.getName())) {
			return new SystemException();
		}

		if (className.equals(
					"it.planetek.liferay.service.builder.DuplicateLibroException")) {
			return new it.planetek.liferay.service.builder.DuplicateLibroException();
		}

		if (className.equals(
					"it.planetek.liferay.service.builder.LibroNameException")) {
			return new it.planetek.liferay.service.builder.LibroNameException();
		}

		if (className.equals(
					"it.planetek.liferay.service.builder.NoSuchAutoreException")) {
			return new it.planetek.liferay.service.builder.NoSuchAutoreException();
		}

		if (className.equals(
					"it.planetek.liferay.service.builder.NoSuchLibreriaException")) {
			return new it.planetek.liferay.service.builder.NoSuchLibreriaException();
		}

		if (className.equals(
					"it.planetek.liferay.service.builder.NoSuchLibroException")) {
			return new it.planetek.liferay.service.builder.NoSuchLibroException();
		}

		if (className.equals(
					"it.planetek.liferay.service.builder.NoSuchLibro_AutoreException")) {
			return new it.planetek.liferay.service.builder.NoSuchLibro_AutoreException();
		}

		return throwable;
	}

	public static Object translateOutputAutore(BaseModel<?> oldModel) {
		AutoreClp newModel = new AutoreClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAutoreRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLibreria(BaseModel<?> oldModel) {
		LibreriaClp newModel = new LibreriaClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLibreriaRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLibro(BaseModel<?> oldModel) {
		LibroClp newModel = new LibroClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLibroRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLibro_Autore(BaseModel<?> oldModel) {
		Libro_AutoreClp newModel = new Libro_AutoreClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLibro_AutoreRemoteModel(oldModel);

		return newModel;
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static String _servletContextName;
	private static boolean _useReflectionToTranslateThrowable = true;
}