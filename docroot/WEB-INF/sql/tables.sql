create table planetek_Autore (
	autoreId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	nome VARCHAR(75) null,
	cognome VARCHAR(75) null
);

create table planetek_Libreria (
	libreriaId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	nome VARCHAR(75) null
);

create table planetek_Libro (
	uuid_ VARCHAR(75) null,
	libroId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	nome VARCHAR(75) null,
	pubblicato BOOLEAN,
	numeroPagine INTEGER,
	dataPubblicazione DATE null,
	descrizione VARCHAR(75) null,
	isbn VARCHAR(16) null,
	status INTEGER,
	statusByUserId LONG,
	statusByUserName VARCHAR(75) null,
	statusDate DATE null
);

create table planetek_Libro_Autore (
	libroId LONG not null,
	autoreId LONG not null,
	primary key (libroId, autoreId)
);