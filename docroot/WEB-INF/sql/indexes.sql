create index IX_ED18D861 on planetek_Autore (groupId);
create index IX_3BDCBA03 on planetek_Autore (groupId, cognome);

create index IX_99056031 on planetek_Libreria (groupId);
create index IX_DCEE503E on planetek_Libreria (groupId, nome);

create index IX_7F2E38E3 on planetek_Libro (groupId);
create index IX_86D5A00D on planetek_Libro (groupId, isbn);
create index IX_8F2A5B70 on planetek_Libro (groupId, nome);
create index IX_F932A82D on planetek_Libro (uuid_);
create index IX_7FC86DDB on planetek_Libro (uuid_, companyId);
create unique index IX_4B8AF1D on planetek_Libro (uuid_, groupId);