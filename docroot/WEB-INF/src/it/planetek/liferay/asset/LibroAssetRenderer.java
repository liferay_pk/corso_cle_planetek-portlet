package it.planetek.liferay.asset;

import it.planetek.liferay.service.builder.model.Libro;

import java.util.Locale;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portlet.asset.model.AssetRenderer;
import com.liferay.portlet.asset.model.BaseAssetRenderer;

public class LibroAssetRenderer extends BaseAssetRenderer {

	private Libro libro;

	public LibroAssetRenderer(Libro libro) {
		this.libro = libro;
	}

	@Override
	public String getClassName() {
		return Libro.class.getName();
	}

	@Override
	public long getClassPK() {
		return libro.getLibroId();
	}

	@Override
	public long getGroupId() {
		return libro.getGroupId();
	}

	@Override
	public String getSummary(Locale locale) {
		return libro.getDescrizione();
	}

	@Override
	public String getTitle(Locale locale) {
		return libro.getNome();
	}

	@Override
	public long getUserId() {
		return libro.getUserId();
	}

	@Override
	public String getUserName() {
		return libro.getUserName();
	}

	@Override
	public String getUuid() {
		return libro.getUuid();
	}

	@Override
	public String render(RenderRequest renderRequest,
			RenderResponse renderResponse, String template) throws Exception {
		if (AssetRenderer.TEMPLATE_FULL_CONTENT.equals(template)) {
			return "/html/libro/asset/full_content.jsp";
		} // else {
		// Significa che voglio utilizzare un template particolare
//			return "/html/libro/asset/list_content.jsp";
//		}
		
		// Significa che voglio utilizzare il template di default
		return null;
	}

}
