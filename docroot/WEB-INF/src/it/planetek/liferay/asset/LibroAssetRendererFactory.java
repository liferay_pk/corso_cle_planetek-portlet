package it.planetek.liferay.asset;

import it.planetek.liferay.service.builder.model.Libro;
import it.planetek.liferay.service.builder.service.LibroLocalServiceUtil;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portlet.asset.model.AssetRenderer;
import com.liferay.portlet.asset.model.BaseAssetRendererFactory;

public class LibroAssetRendererFactory extends BaseAssetRendererFactory {

	@Override
	public AssetRenderer getAssetRenderer(long classPK, int type)
			throws PortalException, SystemException {

		Libro libro = LibroLocalServiceUtil.getLibro(classPK);
		LibroAssetRenderer aRenderer = new LibroAssetRenderer(libro);
		aRenderer.setAssetRendererType(type);
		return aRenderer;
	}

	@Override
	public String getClassName() {
		return Libro.class.getName();
	}

	@Override
	public String getType() {
		return "libro";
	}

}
