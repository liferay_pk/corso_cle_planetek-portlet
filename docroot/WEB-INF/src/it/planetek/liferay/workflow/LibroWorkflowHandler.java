package it.planetek.liferay.workflow;

import it.planetek.liferay.service.builder.model.Libro;
import it.planetek.liferay.service.builder.service.LibroLocalServiceUtil;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.workflow.BaseWorkflowHandler;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.service.ServiceContext;

public class LibroWorkflowHandler extends BaseWorkflowHandler {

	@Override
	public String getClassName() {
		return Libro.class.getName();
	}

	@Override
	public String getType(Locale locale) {
		return "libro";
	}

	@Override
	public Object updateStatus(int status,
			Map<String, Serializable> workflowContext) throws PortalException,
			SystemException {
		// Dal contesto leggiamo l'id utente..
		long userId = GetterUtil.getLong(
				workflowContext.get(WorkflowConstants.CONTEXT_USER_ID));


		// Sempre dal contesto leggiamo la primary key del nostro oggetto..
		long resourcePrimKey = GetterUtil.getLong(
				workflowContext.get(WorkflowConstants.CONTEXT_ENTRY_CLASS_PK));


		// Estraiamo il ServiceContext che serve sempre.. :)
		ServiceContext serviceContext =
				(ServiceContext) workflowContext.get(WorkflowConstants.CONTEXT_SERVICE_CONTEXT);


		// E richiamiamo il nostro metodo che farà update dello status sul nostro asset!
		return LibroLocalServiceUtil.updateStatus(
				userId, resourcePrimKey, status, serviceContext);

	}

}
