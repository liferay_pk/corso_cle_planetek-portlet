/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.planetek.liferay.service.builder.service.LibroServiceUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link it.planetek.liferay.service.builder.service.LibroServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link it.planetek.liferay.service.builder.model.LibroSoap}.
 * If the method in the service utility returns a
 * {@link it.planetek.liferay.service.builder.model.Libro}, that is translated to a
 * {@link it.planetek.liferay.service.builder.model.LibroSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author jfrancia
 * @see LibroServiceHttp
 * @see it.planetek.liferay.service.builder.model.LibroSoap
 * @see it.planetek.liferay.service.builder.service.LibroServiceUtil
 * @generated
 */
public class LibroServiceSoap {
	public static it.planetek.liferay.service.builder.model.LibroSoap addLibro(
		long userId, java.lang.String nome, boolean pubblicato,
		int numeroPagine, java.util.Date dataPubblicazione,
		java.lang.String descrizione, java.lang.String isbn,
		com.liferay.portal.service.ServiceContext ctx)
		throws RemoteException {
		try {
			it.planetek.liferay.service.builder.model.Libro returnValue = LibroServiceUtil.addLibro(userId,
					nome, pubblicato, numeroPagine, dataPubblicazione,
					descrizione, isbn, ctx);

			return it.planetek.liferay.service.builder.model.LibroSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static it.planetek.liferay.service.builder.model.LibroSoap getLibro(
		long libroId) throws RemoteException {
		try {
			it.planetek.liferay.service.builder.model.LibroSoap returnValue = LibroServiceUtil.getLibro(libroId);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(LibroServiceSoap.class);
}