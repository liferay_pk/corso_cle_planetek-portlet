/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service.impl;

import it.planetek.liferay.service.builder.service.base.AutoreLocalServiceBaseImpl;

/**
 * The implementation of the autore local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.planetek.liferay.service.builder.service.AutoreLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author jfrancia
 * @see it.planetek.liferay.service.builder.service.base.AutoreLocalServiceBaseImpl
 * @see it.planetek.liferay.service.builder.service.AutoreLocalServiceUtil
 */
public class AutoreLocalServiceImpl extends AutoreLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.planetek.liferay.service.builder.service.AutoreLocalServiceUtil} to access the autore local service.
	 */
	
	public String sayHelloTo(String name) {
		return "Hello " + name;
	}
}