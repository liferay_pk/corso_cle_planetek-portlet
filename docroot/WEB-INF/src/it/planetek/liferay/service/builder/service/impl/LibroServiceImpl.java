/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service.impl;

import it.planetek.liferay.service.builder.model.Libro;
import it.planetek.liferay.service.builder.model.LibroSoap;
import it.planetek.liferay.service.builder.service.base.LibroServiceBaseImpl;
import it.planetek.liferay.service.builder.service.permission.ModulePermission;

import java.util.Date;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.ServiceContext;

/**
 * The implementation of the libro remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.planetek.liferay.service.builder.service.LibroService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author jfrancia
 * @see it.planetek.liferay.service.builder.service.base.LibroServiceBaseImpl
 * @see it.planetek.liferay.service.builder.service.LibroServiceUtil
 */
public class LibroServiceImpl extends LibroServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.planetek.liferay.service.builder.service.LibroServiceUtil} to access the libro remote service.
	 */
	
	public Libro addLibro(long userId, String nome, boolean pubblicato,
			int numeroPagine, Date dataPubblicazione, String descrizione, 
			String isbn, ServiceContext ctx)
					throws PortalException, SystemException {	
	
		ModulePermission.check(getPermissionChecker(), ctx.getScopeGroupId(), ActionKeys.ADD_ENTRY);
		
		return libroLocalService.addLibro(userId, nome, pubblicato, numeroPagine, dataPubblicazione, descrizione, isbn, ctx);		
	}
	
	public LibroSoap getLibro(long libroId) throws Exception {		
		Libro libro = libroLocalService.getLibro(libroId);		
		return LibroSoap.toSoapModel(libro);
	}
	
}