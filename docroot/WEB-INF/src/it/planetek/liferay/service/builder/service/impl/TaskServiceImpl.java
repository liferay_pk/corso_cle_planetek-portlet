/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service.impl;

import it.planetek.liferay.service.builder.model.MyTask;
import it.planetek.liferay.service.builder.service.base.TaskServiceBaseImpl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;

/**
 * The implementation of the task remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.planetek.liferay.service.builder.service.TaskService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author jfrancia
 * @see it.planetek.liferay.service.builder.service.base.TaskServiceBaseImpl
 * @see it.planetek.liferay.service.builder.service.TaskServiceUtil
 */
public class TaskServiceImpl extends TaskServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.planetek.liferay.service.builder.service.TaskServiceUtil} to access the task remote service.
	 */
	
	public MyTask[] getTaskList(long userId, ServiceContext ctx) throws Exception {
		User user = userLocalService.getUser(userId);
		MyTask myTask = new MyTask();
		myTask.setNome(user.getFirstName());
		myTask.setCognome(user.getLastName());
		List<MyTask> list = new ArrayList<MyTask>();
		list.add(myTask);
		return (MyTask[]) list.toArray(new MyTask[list.size()]);
	}
	
}