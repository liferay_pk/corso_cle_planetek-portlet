/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.util.PortalUtil;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import it.planetek.liferay.service.builder.model.Libreria;
import it.planetek.liferay.service.builder.model.LibreriaModel;

import java.io.Serializable;

import java.sql.Types;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the Libreria service. Represents a row in the &quot;planetek_Libreria&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link it.planetek.liferay.service.builder.model.LibreriaModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link LibreriaImpl}.
 * </p>
 *
 * @author jfrancia
 * @see LibreriaImpl
 * @see it.planetek.liferay.service.builder.model.Libreria
 * @see it.planetek.liferay.service.builder.model.LibreriaModel
 * @generated
 */
public class LibreriaModelImpl extends BaseModelImpl<Libreria>
	implements LibreriaModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a libreria model instance should use the {@link it.planetek.liferay.service.builder.model.Libreria} interface instead.
	 */
	public static final String TABLE_NAME = "planetek_Libreria";
	public static final Object[][] TABLE_COLUMNS = {
			{ "libreriaId", Types.BIGINT },
			{ "companyId", Types.BIGINT },
			{ "groupId", Types.BIGINT },
			{ "userId", Types.BIGINT },
			{ "userName", Types.VARCHAR },
			{ "createDate", Types.TIMESTAMP },
			{ "modifiedDate", Types.TIMESTAMP },
			{ "nome", Types.VARCHAR }
		};
	public static final String TABLE_SQL_CREATE = "create table planetek_Libreria (libreriaId LONG not null primary key,companyId LONG,groupId LONG,userId LONG,userName VARCHAR(75) null,createDate DATE null,modifiedDate DATE null,nome VARCHAR(75) null)";
	public static final String TABLE_SQL_DROP = "drop table planetek_Libreria";
	public static final String ORDER_BY_JPQL = " ORDER BY libreria.nome ASC";
	public static final String ORDER_BY_SQL = " ORDER BY planetek_Libreria.nome ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.it.planetek.liferay.service.builder.model.Libreria"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.it.planetek.liferay.service.builder.model.Libreria"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.column.bitmask.enabled.it.planetek.liferay.service.builder.model.Libreria"),
			true);
	public static long GROUPID_COLUMN_BITMASK = 1L;
	public static long NOME_COLUMN_BITMASK = 2L;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.it.planetek.liferay.service.builder.model.Libreria"));

	public LibreriaModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _libreriaId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setLibreriaId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _libreriaId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return Libreria.class;
	}

	@Override
	public String getModelClassName() {
		return Libreria.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("libreriaId", getLibreriaId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("nome", getNome());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long libreriaId = (Long)attributes.get("libreriaId");

		if (libreriaId != null) {
			setLibreriaId(libreriaId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String nome = (String)attributes.get("nome");

		if (nome != null) {
			setNome(nome);
		}
	}

	@Override
	public long getLibreriaId() {
		return _libreriaId;
	}

	@Override
	public void setLibreriaId(long libreriaId) {
		_libreriaId = libreriaId;
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	@Override
	public long getGroupId() {
		return _groupId;
	}

	@Override
	public void setGroupId(long groupId) {
		_columnBitmask |= GROUPID_COLUMN_BITMASK;

		if (!_setOriginalGroupId) {
			_setOriginalGroupId = true;

			_originalGroupId = _groupId;
		}

		_groupId = groupId;
	}

	public long getOriginalGroupId() {
		return _originalGroupId;
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public String getUserName() {
		if (_userName == null) {
			return StringPool.BLANK;
		}
		else {
			return _userName;
		}
	}

	@Override
	public void setUserName(String userName) {
		_userName = userName;
	}

	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	@Override
	public Date getModifiedDate() {
		return _modifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	@Override
	public String getNome() {
		if (_nome == null) {
			return StringPool.BLANK;
		}
		else {
			return _nome;
		}
	}

	@Override
	public void setNome(String nome) {
		_columnBitmask = -1L;

		if (_originalNome == null) {
			_originalNome = _nome;
		}

		_nome = nome;
	}

	public String getOriginalNome() {
		return GetterUtil.getString(_originalNome);
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(getCompanyId(),
			Libreria.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public Libreria toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (Libreria)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		LibreriaImpl libreriaImpl = new LibreriaImpl();

		libreriaImpl.setLibreriaId(getLibreriaId());
		libreriaImpl.setCompanyId(getCompanyId());
		libreriaImpl.setGroupId(getGroupId());
		libreriaImpl.setUserId(getUserId());
		libreriaImpl.setUserName(getUserName());
		libreriaImpl.setCreateDate(getCreateDate());
		libreriaImpl.setModifiedDate(getModifiedDate());
		libreriaImpl.setNome(getNome());

		libreriaImpl.resetOriginalValues();

		return libreriaImpl;
	}

	@Override
	public int compareTo(Libreria libreria) {
		int value = 0;

		value = getNome().compareTo(libreria.getNome());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Libreria)) {
			return false;
		}

		Libreria libreria = (Libreria)obj;

		long primaryKey = libreria.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public void resetOriginalValues() {
		LibreriaModelImpl libreriaModelImpl = this;

		libreriaModelImpl._originalGroupId = libreriaModelImpl._groupId;

		libreriaModelImpl._setOriginalGroupId = false;

		libreriaModelImpl._originalNome = libreriaModelImpl._nome;

		libreriaModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<Libreria> toCacheModel() {
		LibreriaCacheModel libreriaCacheModel = new LibreriaCacheModel();

		libreriaCacheModel.libreriaId = getLibreriaId();

		libreriaCacheModel.companyId = getCompanyId();

		libreriaCacheModel.groupId = getGroupId();

		libreriaCacheModel.userId = getUserId();

		libreriaCacheModel.userName = getUserName();

		String userName = libreriaCacheModel.userName;

		if ((userName != null) && (userName.length() == 0)) {
			libreriaCacheModel.userName = null;
		}

		Date createDate = getCreateDate();

		if (createDate != null) {
			libreriaCacheModel.createDate = createDate.getTime();
		}
		else {
			libreriaCacheModel.createDate = Long.MIN_VALUE;
		}

		Date modifiedDate = getModifiedDate();

		if (modifiedDate != null) {
			libreriaCacheModel.modifiedDate = modifiedDate.getTime();
		}
		else {
			libreriaCacheModel.modifiedDate = Long.MIN_VALUE;
		}

		libreriaCacheModel.nome = getNome();

		String nome = libreriaCacheModel.nome;

		if ((nome != null) && (nome.length() == 0)) {
			libreriaCacheModel.nome = null;
		}

		return libreriaCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{libreriaId=");
		sb.append(getLibreriaId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", nome=");
		sb.append(getNome());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(28);

		sb.append("<model><model-name>");
		sb.append("it.planetek.liferay.service.builder.model.Libreria");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>libreriaId</column-name><column-value><![CDATA[");
		sb.append(getLibreriaId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nome</column-name><column-value><![CDATA[");
		sb.append(getNome());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = Libreria.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			Libreria.class
		};
	private long _libreriaId;
	private long _companyId;
	private long _groupId;
	private long _originalGroupId;
	private boolean _setOriginalGroupId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _nome;
	private String _originalNome;
	private long _columnBitmask;
	private Libreria _escapedModel;
}