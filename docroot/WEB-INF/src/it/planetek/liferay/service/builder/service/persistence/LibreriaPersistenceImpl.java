/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.planetek.liferay.service.builder.NoSuchLibreriaException;
import it.planetek.liferay.service.builder.model.Libreria;
import it.planetek.liferay.service.builder.model.impl.LibreriaImpl;
import it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the libreria service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author jfrancia
 * @see LibreriaPersistence
 * @see LibreriaUtil
 * @generated
 */
public class LibreriaPersistenceImpl extends BasePersistenceImpl<Libreria>
	implements LibreriaPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LibreriaUtil} to access the libreria persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LibreriaImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
			LibreriaModelImpl.FINDER_CACHE_ENABLED, LibreriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
			LibreriaModelImpl.FINDER_CACHE_ENABLED, LibreriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
			LibreriaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUP = new FinderPath(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
			LibreriaModelImpl.FINDER_CACHE_ENABLED, LibreriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroup",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUP = new FinderPath(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
			LibreriaModelImpl.FINDER_CACHE_ENABLED, LibreriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroup",
			new String[] { Long.class.getName() },
			LibreriaModelImpl.GROUPID_COLUMN_BITMASK |
			LibreriaModelImpl.NOME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUP = new FinderPath(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
			LibreriaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroup",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the librerias where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching librerias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Libreria> findByGroup(long groupId) throws SystemException {
		return findByGroup(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the librerias where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of librerias
	 * @param end the upper bound of the range of librerias (not inclusive)
	 * @return the range of matching librerias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Libreria> findByGroup(long groupId, int start, int end)
		throws SystemException {
		return findByGroup(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the librerias where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of librerias
	 * @param end the upper bound of the range of librerias (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching librerias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Libreria> findByGroup(long groupId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUP;
			finderArgs = new Object[] { groupId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUP;
			finderArgs = new Object[] { groupId, start, end, orderByComparator };
		}

		List<Libreria> list = (List<Libreria>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Libreria libreria : list) {
				if ((groupId != libreria.getGroupId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LIBRERIA_WHERE);

			query.append(_FINDER_COLUMN_GROUP_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(LibreriaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (!pagination) {
					list = (List<Libreria>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Libreria>(list);
				}
				else {
					list = (List<Libreria>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first libreria in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching libreria
	 * @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a matching libreria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria findByGroup_First(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchLibreriaException, SystemException {
		Libreria libreria = fetchByGroup_First(groupId, orderByComparator);

		if (libreria != null) {
			return libreria;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLibreriaException(msg.toString());
	}

	/**
	 * Returns the first libreria in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching libreria, or <code>null</code> if a matching libreria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria fetchByGroup_First(long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Libreria> list = findByGroup(groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last libreria in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching libreria
	 * @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a matching libreria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria findByGroup_Last(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchLibreriaException, SystemException {
		Libreria libreria = fetchByGroup_Last(groupId, orderByComparator);

		if (libreria != null) {
			return libreria;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLibreriaException(msg.toString());
	}

	/**
	 * Returns the last libreria in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching libreria, or <code>null</code> if a matching libreria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria fetchByGroup_Last(long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByGroup(groupId);

		if (count == 0) {
			return null;
		}

		List<Libreria> list = findByGroup(groupId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the librerias before and after the current libreria in the ordered set where groupId = &#63;.
	 *
	 * @param libreriaId the primary key of the current libreria
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next libreria
	 * @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a libreria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria[] findByGroup_PrevAndNext(long libreriaId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchLibreriaException, SystemException {
		Libreria libreria = findByPrimaryKey(libreriaId);

		Session session = null;

		try {
			session = openSession();

			Libreria[] array = new LibreriaImpl[3];

			array[0] = getByGroup_PrevAndNext(session, libreria, groupId,
					orderByComparator, true);

			array[1] = libreria;

			array[2] = getByGroup_PrevAndNext(session, libreria, groupId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Libreria getByGroup_PrevAndNext(Session session,
		Libreria libreria, long groupId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LIBRERIA_WHERE);

		query.append(_FINDER_COLUMN_GROUP_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LibreriaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(libreria);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Libreria> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the librerias where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByGroup(long groupId) throws SystemException {
		for (Libreria libreria : findByGroup(groupId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(libreria);
		}
	}

	/**
	 * Returns the number of librerias where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching librerias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByGroup(long groupId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GROUP;

		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LIBRERIA_WHERE);

			query.append(_FINDER_COLUMN_GROUP_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUP_GROUPID_2 = "libreria.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_G_N = new FinderPath(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
			LibreriaModelImpl.FINDER_CACHE_ENABLED, LibreriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByG_N",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_G_N = new FinderPath(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
			LibreriaModelImpl.FINDER_CACHE_ENABLED, LibreriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByG_N",
			new String[] { Long.class.getName(), String.class.getName() },
			LibreriaModelImpl.GROUPID_COLUMN_BITMASK |
			LibreriaModelImpl.NOME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_G_N = new FinderPath(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
			LibreriaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByG_N",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the librerias where groupId = &#63; and nome = &#63;.
	 *
	 * @param groupId the group ID
	 * @param nome the nome
	 * @return the matching librerias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Libreria> findByG_N(long groupId, String nome)
		throws SystemException {
		return findByG_N(groupId, nome, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the librerias where groupId = &#63; and nome = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param nome the nome
	 * @param start the lower bound of the range of librerias
	 * @param end the upper bound of the range of librerias (not inclusive)
	 * @return the range of matching librerias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Libreria> findByG_N(long groupId, String nome, int start,
		int end) throws SystemException {
		return findByG_N(groupId, nome, start, end, null);
	}

	/**
	 * Returns an ordered range of all the librerias where groupId = &#63; and nome = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param nome the nome
	 * @param start the lower bound of the range of librerias
	 * @param end the upper bound of the range of librerias (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching librerias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Libreria> findByG_N(long groupId, String nome, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_G_N;
			finderArgs = new Object[] { groupId, nome };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_G_N;
			finderArgs = new Object[] {
					groupId, nome,
					
					start, end, orderByComparator
				};
		}

		List<Libreria> list = (List<Libreria>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Libreria libreria : list) {
				if ((groupId != libreria.getGroupId()) ||
						!Validator.equals(nome, libreria.getNome())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_LIBRERIA_WHERE);

			query.append(_FINDER_COLUMN_G_N_GROUPID_2);

			boolean bindNome = false;

			if (nome == null) {
				query.append(_FINDER_COLUMN_G_N_NOME_1);
			}
			else if (nome.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_G_N_NOME_3);
			}
			else {
				bindNome = true;

				query.append(_FINDER_COLUMN_G_N_NOME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(LibreriaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (bindNome) {
					qPos.add(nome);
				}

				if (!pagination) {
					list = (List<Libreria>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Libreria>(list);
				}
				else {
					list = (List<Libreria>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first libreria in the ordered set where groupId = &#63; and nome = &#63;.
	 *
	 * @param groupId the group ID
	 * @param nome the nome
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching libreria
	 * @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a matching libreria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria findByG_N_First(long groupId, String nome,
		OrderByComparator orderByComparator)
		throws NoSuchLibreriaException, SystemException {
		Libreria libreria = fetchByG_N_First(groupId, nome, orderByComparator);

		if (libreria != null) {
			return libreria;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(", nome=");
		msg.append(nome);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLibreriaException(msg.toString());
	}

	/**
	 * Returns the first libreria in the ordered set where groupId = &#63; and nome = &#63;.
	 *
	 * @param groupId the group ID
	 * @param nome the nome
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching libreria, or <code>null</code> if a matching libreria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria fetchByG_N_First(long groupId, String nome,
		OrderByComparator orderByComparator) throws SystemException {
		List<Libreria> list = findByG_N(groupId, nome, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last libreria in the ordered set where groupId = &#63; and nome = &#63;.
	 *
	 * @param groupId the group ID
	 * @param nome the nome
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching libreria
	 * @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a matching libreria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria findByG_N_Last(long groupId, String nome,
		OrderByComparator orderByComparator)
		throws NoSuchLibreriaException, SystemException {
		Libreria libreria = fetchByG_N_Last(groupId, nome, orderByComparator);

		if (libreria != null) {
			return libreria;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(", nome=");
		msg.append(nome);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLibreriaException(msg.toString());
	}

	/**
	 * Returns the last libreria in the ordered set where groupId = &#63; and nome = &#63;.
	 *
	 * @param groupId the group ID
	 * @param nome the nome
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching libreria, or <code>null</code> if a matching libreria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria fetchByG_N_Last(long groupId, String nome,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByG_N(groupId, nome);

		if (count == 0) {
			return null;
		}

		List<Libreria> list = findByG_N(groupId, nome, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the librerias before and after the current libreria in the ordered set where groupId = &#63; and nome = &#63;.
	 *
	 * @param libreriaId the primary key of the current libreria
	 * @param groupId the group ID
	 * @param nome the nome
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next libreria
	 * @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a libreria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria[] findByG_N_PrevAndNext(long libreriaId, long groupId,
		String nome, OrderByComparator orderByComparator)
		throws NoSuchLibreriaException, SystemException {
		Libreria libreria = findByPrimaryKey(libreriaId);

		Session session = null;

		try {
			session = openSession();

			Libreria[] array = new LibreriaImpl[3];

			array[0] = getByG_N_PrevAndNext(session, libreria, groupId, nome,
					orderByComparator, true);

			array[1] = libreria;

			array[2] = getByG_N_PrevAndNext(session, libreria, groupId, nome,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Libreria getByG_N_PrevAndNext(Session session, Libreria libreria,
		long groupId, String nome, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LIBRERIA_WHERE);

		query.append(_FINDER_COLUMN_G_N_GROUPID_2);

		boolean bindNome = false;

		if (nome == null) {
			query.append(_FINDER_COLUMN_G_N_NOME_1);
		}
		else if (nome.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_G_N_NOME_3);
		}
		else {
			bindNome = true;

			query.append(_FINDER_COLUMN_G_N_NOME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LibreriaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (bindNome) {
			qPos.add(nome);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(libreria);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Libreria> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the librerias where groupId = &#63; and nome = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param nome the nome
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByG_N(long groupId, String nome)
		throws SystemException {
		for (Libreria libreria : findByG_N(groupId, nome, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(libreria);
		}
	}

	/**
	 * Returns the number of librerias where groupId = &#63; and nome = &#63;.
	 *
	 * @param groupId the group ID
	 * @param nome the nome
	 * @return the number of matching librerias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByG_N(long groupId, String nome) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_G_N;

		Object[] finderArgs = new Object[] { groupId, nome };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_LIBRERIA_WHERE);

			query.append(_FINDER_COLUMN_G_N_GROUPID_2);

			boolean bindNome = false;

			if (nome == null) {
				query.append(_FINDER_COLUMN_G_N_NOME_1);
			}
			else if (nome.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_G_N_NOME_3);
			}
			else {
				bindNome = true;

				query.append(_FINDER_COLUMN_G_N_NOME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (bindNome) {
					qPos.add(nome);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_G_N_GROUPID_2 = "libreria.groupId = ? AND ";
	private static final String _FINDER_COLUMN_G_N_NOME_1 = "libreria.nome IS NULL";
	private static final String _FINDER_COLUMN_G_N_NOME_2 = "libreria.nome = ?";
	private static final String _FINDER_COLUMN_G_N_NOME_3 = "(libreria.nome IS NULL OR libreria.nome = '')";

	public LibreriaPersistenceImpl() {
		setModelClass(Libreria.class);
	}

	/**
	 * Caches the libreria in the entity cache if it is enabled.
	 *
	 * @param libreria the libreria
	 */
	@Override
	public void cacheResult(Libreria libreria) {
		EntityCacheUtil.putResult(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
			LibreriaImpl.class, libreria.getPrimaryKey(), libreria);

		libreria.resetOriginalValues();
	}

	/**
	 * Caches the librerias in the entity cache if it is enabled.
	 *
	 * @param librerias the librerias
	 */
	@Override
	public void cacheResult(List<Libreria> librerias) {
		for (Libreria libreria : librerias) {
			if (EntityCacheUtil.getResult(
						LibreriaModelImpl.ENTITY_CACHE_ENABLED,
						LibreriaImpl.class, libreria.getPrimaryKey()) == null) {
				cacheResult(libreria);
			}
			else {
				libreria.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all librerias.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(LibreriaImpl.class.getName());
		}

		EntityCacheUtil.clearCache(LibreriaImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the libreria.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Libreria libreria) {
		EntityCacheUtil.removeResult(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
			LibreriaImpl.class, libreria.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Libreria> librerias) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Libreria libreria : librerias) {
			EntityCacheUtil.removeResult(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
				LibreriaImpl.class, libreria.getPrimaryKey());
		}
	}

	/**
	 * Creates a new libreria with the primary key. Does not add the libreria to the database.
	 *
	 * @param libreriaId the primary key for the new libreria
	 * @return the new libreria
	 */
	@Override
	public Libreria create(long libreriaId) {
		Libreria libreria = new LibreriaImpl();

		libreria.setNew(true);
		libreria.setPrimaryKey(libreriaId);

		return libreria;
	}

	/**
	 * Removes the libreria with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param libreriaId the primary key of the libreria
	 * @return the libreria that was removed
	 * @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a libreria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria remove(long libreriaId)
		throws NoSuchLibreriaException, SystemException {
		return remove((Serializable)libreriaId);
	}

	/**
	 * Removes the libreria with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the libreria
	 * @return the libreria that was removed
	 * @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a libreria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria remove(Serializable primaryKey)
		throws NoSuchLibreriaException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Libreria libreria = (Libreria)session.get(LibreriaImpl.class,
					primaryKey);

			if (libreria == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLibreriaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(libreria);
		}
		catch (NoSuchLibreriaException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Libreria removeImpl(Libreria libreria) throws SystemException {
		libreria = toUnwrappedModel(libreria);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(libreria)) {
				libreria = (Libreria)session.get(LibreriaImpl.class,
						libreria.getPrimaryKeyObj());
			}

			if (libreria != null) {
				session.delete(libreria);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (libreria != null) {
			clearCache(libreria);
		}

		return libreria;
	}

	@Override
	public Libreria updateImpl(
		it.planetek.liferay.service.builder.model.Libreria libreria)
		throws SystemException {
		libreria = toUnwrappedModel(libreria);

		boolean isNew = libreria.isNew();

		LibreriaModelImpl libreriaModelImpl = (LibreriaModelImpl)libreria;

		Session session = null;

		try {
			session = openSession();

			if (libreria.isNew()) {
				session.save(libreria);

				libreria.setNew(false);
			}
			else {
				session.merge(libreria);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !LibreriaModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((libreriaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUP.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						libreriaModelImpl.getOriginalGroupId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUP, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUP,
					args);

				args = new Object[] { libreriaModelImpl.getGroupId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUP, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUP,
					args);
			}

			if ((libreriaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_G_N.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						libreriaModelImpl.getOriginalGroupId(),
						libreriaModelImpl.getOriginalNome()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_G_N, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_G_N,
					args);

				args = new Object[] {
						libreriaModelImpl.getGroupId(),
						libreriaModelImpl.getNome()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_G_N, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_G_N,
					args);
			}
		}

		EntityCacheUtil.putResult(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
			LibreriaImpl.class, libreria.getPrimaryKey(), libreria);

		return libreria;
	}

	protected Libreria toUnwrappedModel(Libreria libreria) {
		if (libreria instanceof LibreriaImpl) {
			return libreria;
		}

		LibreriaImpl libreriaImpl = new LibreriaImpl();

		libreriaImpl.setNew(libreria.isNew());
		libreriaImpl.setPrimaryKey(libreria.getPrimaryKey());

		libreriaImpl.setLibreriaId(libreria.getLibreriaId());
		libreriaImpl.setCompanyId(libreria.getCompanyId());
		libreriaImpl.setGroupId(libreria.getGroupId());
		libreriaImpl.setUserId(libreria.getUserId());
		libreriaImpl.setUserName(libreria.getUserName());
		libreriaImpl.setCreateDate(libreria.getCreateDate());
		libreriaImpl.setModifiedDate(libreria.getModifiedDate());
		libreriaImpl.setNome(libreria.getNome());

		return libreriaImpl;
	}

	/**
	 * Returns the libreria with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the libreria
	 * @return the libreria
	 * @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a libreria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria findByPrimaryKey(Serializable primaryKey)
		throws NoSuchLibreriaException, SystemException {
		Libreria libreria = fetchByPrimaryKey(primaryKey);

		if (libreria == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchLibreriaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return libreria;
	}

	/**
	 * Returns the libreria with the primary key or throws a {@link it.planetek.liferay.service.builder.NoSuchLibreriaException} if it could not be found.
	 *
	 * @param libreriaId the primary key of the libreria
	 * @return the libreria
	 * @throws it.planetek.liferay.service.builder.NoSuchLibreriaException if a libreria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria findByPrimaryKey(long libreriaId)
		throws NoSuchLibreriaException, SystemException {
		return findByPrimaryKey((Serializable)libreriaId);
	}

	/**
	 * Returns the libreria with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the libreria
	 * @return the libreria, or <code>null</code> if a libreria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Libreria libreria = (Libreria)EntityCacheUtil.getResult(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
				LibreriaImpl.class, primaryKey);

		if (libreria == _nullLibreria) {
			return null;
		}

		if (libreria == null) {
			Session session = null;

			try {
				session = openSession();

				libreria = (Libreria)session.get(LibreriaImpl.class, primaryKey);

				if (libreria != null) {
					cacheResult(libreria);
				}
				else {
					EntityCacheUtil.putResult(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
						LibreriaImpl.class, primaryKey, _nullLibreria);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(LibreriaModelImpl.ENTITY_CACHE_ENABLED,
					LibreriaImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return libreria;
	}

	/**
	 * Returns the libreria with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param libreriaId the primary key of the libreria
	 * @return the libreria, or <code>null</code> if a libreria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libreria fetchByPrimaryKey(long libreriaId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)libreriaId);
	}

	/**
	 * Returns all the librerias.
	 *
	 * @return the librerias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Libreria> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the librerias.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of librerias
	 * @param end the upper bound of the range of librerias (not inclusive)
	 * @return the range of librerias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Libreria> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the librerias.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.LibreriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of librerias
	 * @param end the upper bound of the range of librerias (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of librerias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Libreria> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Libreria> list = (List<Libreria>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_LIBRERIA);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LIBRERIA;

				if (pagination) {
					sql = sql.concat(LibreriaModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Libreria>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Libreria>(list);
				}
				else {
					list = (List<Libreria>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the librerias from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Libreria libreria : findAll()) {
			remove(libreria);
		}
	}

	/**
	 * Returns the number of librerias.
	 *
	 * @return the number of librerias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LIBRERIA);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the libreria persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.planetek.liferay.service.builder.model.Libreria")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Libreria>> listenersList = new ArrayList<ModelListener<Libreria>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Libreria>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(LibreriaImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_LIBRERIA = "SELECT libreria FROM Libreria libreria";
	private static final String _SQL_SELECT_LIBRERIA_WHERE = "SELECT libreria FROM Libreria libreria WHERE ";
	private static final String _SQL_COUNT_LIBRERIA = "SELECT COUNT(libreria) FROM Libreria libreria";
	private static final String _SQL_COUNT_LIBRERIA_WHERE = "SELECT COUNT(libreria) FROM Libreria libreria WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "libreria.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Libreria exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Libreria exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(LibreriaPersistenceImpl.class);
	private static Libreria _nullLibreria = new LibreriaImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Libreria> toCacheModel() {
				return _nullLibreriaCacheModel;
			}
		};

	private static CacheModel<Libreria> _nullLibreriaCacheModel = new CacheModel<Libreria>() {
			@Override
			public Libreria toEntityModel() {
				return _nullLibreria;
			}
		};
}