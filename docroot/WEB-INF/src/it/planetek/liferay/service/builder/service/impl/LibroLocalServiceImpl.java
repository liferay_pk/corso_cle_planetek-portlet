/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service.impl;

import it.planetek.liferay.service.builder.DuplicateLibroException;
import it.planetek.liferay.service.builder.LibroNameException;
import it.planetek.liferay.service.builder.model.Libro;
import it.planetek.liferay.service.builder.service.base.LibroLocalServiceBaseImpl;

import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.workflow.WorkflowHandlerRegistryUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.asset.NoSuchEntryException;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.model.AssetLinkConstants;
import com.liferay.portlet.blogs.model.BlogsEntry;

/**
 * The implementation of the libro local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.planetek.liferay.service.builder.service.LibroLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author jfrancia
 * @see it.planetek.liferay.service.builder.service.base.LibroLocalServiceBaseImpl
 * @see it.planetek.liferay.service.builder.service.LibroLocalServiceUtil
 */
public class LibroLocalServiceImpl extends LibroLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.planetek.liferay.service.builder.service.LibroLocalServiceUtil} to access the libro local service.
	 */

	public List<Libro> findByGroup(long groupId, int start, int end) throws SystemException {
		return libroPersistence.findByGroup(groupId, start, end);
	}

	public int countByGroup(long groupId) throws SystemException {
		return libroPersistence.countByGroup(groupId);
	}

	public Libro addLibro(long userId, String nome, boolean pubblicato,
			int numeroPagine, Date dataPubblicazione, String descrizione, 
			String isbn, ServiceContext ctx)
					throws PortalException, SystemException {		
		
		User user = userPersistence.findByPrimaryKey(userId);

		long groupId = ctx.getScopeGroupId();

		Date now = new Date();

		validate(0, groupId, nome);

		long entryId = counterLocalService.increment(Libro.class.getName());

		Libro entry = libroPersistence.create(entryId);

		entry.setCompanyId(ctx.getCompanyId());
		entry.setGroupId(groupId);
		entry.setUserId(user.getUserId());
		entry.setUserName(user.getFullName());
		entry.setCreateDate(ctx.getCreateDate(now));
		entry.setModifiedDate(ctx.getModifiedDate(now));

		entry.setNome(nome);
		entry.setDescrizione(descrizione);
		entry.setNumeroPagine(numeroPagine);
		entry.setPubblicato(pubblicato);
		entry.setDataPubblicazione(dataPubblicazione);
		entry.setIsbn(isbn);
		
		entry.setExpandoBridgeAttributes(ctx);
		
        // Workflow
        entry.setStatus(WorkflowConstants.STATUS_DRAFT);

		
		libroPersistence.update(entry);

		// Resources
		resourceLocalService.addModelResources(entry, ctx);
		
        //Asset
        updateAsset(
                        userId, entry, ctx.getAssetCategoryIds(),
                        ctx.getAssetTagNames(),
                        ctx.getAssetLinkEntryIds(), now);

        // Workflow
        WorkflowHandlerRegistryUtil.startWorkflowInstance(entry.getCompanyId(),
                        entry.getGroupId(), entry.getUserId(), Libro.class.getName(),
                        entry.getPrimaryKey(), entry, ctx);


		return entry;

	}

    public void updateAsset(
            long userId, Libro entry, long[] assetCategoryIds,
            String[] assetTagNames, long[] assetLinkEntryIds, Date now)
    throws PortalException, SystemException {

    boolean visible = false;

    try{

            AssetEntry assetEntry = assetEntryLocalService.getEntry(
                            Libro.class.getName(), entry.getLibroId());

            if(assetEntry!=null)
                    visible =  assetEntry.isVisible();

    }catch(NoSuchEntryException e){
            //do nothing
    }

    AssetEntry assetEntry = assetEntryLocalService.updateEntry(
            userId, entry.getGroupId(), now, now, Libro.class.getName(),
            entry.getLibroId(), entry.getUuid(), 0, assetCategoryIds,
            assetTagNames, visible, null, null, null, null,
            ContentTypes.TEXT_PLAIN, entry.getNome(), entry.getDescrizione(),
            null, null, 0, 0, null, false);

    assetLinkLocalService.updateLinks(
            userId, assetEntry.getEntryId(), assetLinkEntryIds,
            AssetLinkConstants.TYPE_RELATED);
}

	public Libro updateLibro(long entryId, String nome, boolean pubblicato,
			int numeroPagine, Date dataPubblicazione, String descrizione, String isbn, ServiceContext ctx)
					throws PortalException, SystemException {

		Libro entry = libroPersistence.findByPrimaryKey(entryId);	

		validate(entryId, entry.getGroupId(), nome);

		User user = userPersistence.findByPrimaryKey(ctx.getUserId());

		entry.setModifiedDate(ctx.getModifiedDate(null));
		entry.setUserId(user.getUserId());
		entry.setUserName(user.getFullName());

		entry.setNome(nome);
		entry.setDescrizione(descrizione);
		entry.setNumeroPagine(numeroPagine);
		entry.setPubblicato(pubblicato);
		entry.setDataPubblicazione(dataPubblicazione);
		entry.setIsbn(isbn);
		
		entry.setExpandoBridgeAttributes(ctx);
		
		libroPersistence.update(entry);

        Indexer indexer = IndexerRegistryUtil.nullSafeGetIndexer(Libro.class);
        indexer.reindex(entry);
		
		return entry;
	}

	public Libro updateStatus(
			  long userId, long resourcePk, int status, ServiceContext ctx) 
			  throws PortalException, SystemException {
			     // Estraiamo l'utente che sta aggiornando l'asset
			     User user = userLocalService.getUser(userId);

			 
			     // Recuperiamo la nostra entity usando la primary key
			     Libro entry = getLibro(resourcePk);

			 
			     // Aggiorniamo i field di audit che abbiamo mappato prima nel service.xml
			     entry.setStatus(status);
			     entry.setStatusByUserId(userId);
			     entry.setStatusByUserName(user.getFullName());
			     entry.setStatusDate(ctx.getModifiedDate());

			 
			     if (status == WorkflowConstants.STATUS_APPROVED) {
			       // Se lo stato è approvato allora rendiamo l'asset visibile, così che anche l'asset
			       // publisher lo mostrerà
			       assetEntryLocalService.updateVisible(Libro.class.getName(), resourcePk, true);
			     } else {
			       // altriment lo lasciamo invisibile, 
			       //così che non sia nemmeno estratto dall'asset publisher
			       assetEntryLocalService.updateVisible(Libro.class.getName(), resourcePk, false);
			     }

			 
			     // facciamo update sul db del nuovo stato
			     libroPersistence.update(entry);

			     Indexer indexer = IndexerRegistryUtil.nullSafeGetIndexer(Libro.class);
		         indexer.reindex(entry);

			 
			     return entry;
			}
	
	protected void validate(long libroId, long groupId, String nome)
			throws PortalException, SystemException{

		//Name can not be empty
		if (Validator.isNull(nome)) {
			throw new LibroNameException();
		}

		//The issue can be defined only once per group
		Libro entry = null;
		try {
			entry = libroPersistence.findByG_N(groupId, nome);
		} catch (Exception e) {}
		
		if (entry != null && entry.getLibroId() != libroId) {
			throw new DuplicateLibroException();
		}

	}

}