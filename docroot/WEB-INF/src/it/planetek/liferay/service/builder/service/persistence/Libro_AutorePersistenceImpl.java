/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.planetek.liferay.service.builder.NoSuchLibro_AutoreException;
import it.planetek.liferay.service.builder.model.Libro_Autore;
import it.planetek.liferay.service.builder.model.impl.Libro_AutoreImpl;
import it.planetek.liferay.service.builder.model.impl.Libro_AutoreModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the libro_ autore service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author jfrancia
 * @see Libro_AutorePersistence
 * @see Libro_AutoreUtil
 * @generated
 */
public class Libro_AutorePersistenceImpl extends BasePersistenceImpl<Libro_Autore>
	implements Libro_AutorePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link Libro_AutoreUtil} to access the libro_ autore persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = Libro_AutoreImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(Libro_AutoreModelImpl.ENTITY_CACHE_ENABLED,
			Libro_AutoreModelImpl.FINDER_CACHE_ENABLED, Libro_AutoreImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(Libro_AutoreModelImpl.ENTITY_CACHE_ENABLED,
			Libro_AutoreModelImpl.FINDER_CACHE_ENABLED, Libro_AutoreImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(Libro_AutoreModelImpl.ENTITY_CACHE_ENABLED,
			Libro_AutoreModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public Libro_AutorePersistenceImpl() {
		setModelClass(Libro_Autore.class);
	}

	/**
	 * Caches the libro_ autore in the entity cache if it is enabled.
	 *
	 * @param libro_Autore the libro_ autore
	 */
	@Override
	public void cacheResult(Libro_Autore libro_Autore) {
		EntityCacheUtil.putResult(Libro_AutoreModelImpl.ENTITY_CACHE_ENABLED,
			Libro_AutoreImpl.class, libro_Autore.getPrimaryKey(), libro_Autore);

		libro_Autore.resetOriginalValues();
	}

	/**
	 * Caches the libro_ autores in the entity cache if it is enabled.
	 *
	 * @param libro_Autores the libro_ autores
	 */
	@Override
	public void cacheResult(List<Libro_Autore> libro_Autores) {
		for (Libro_Autore libro_Autore : libro_Autores) {
			if (EntityCacheUtil.getResult(
						Libro_AutoreModelImpl.ENTITY_CACHE_ENABLED,
						Libro_AutoreImpl.class, libro_Autore.getPrimaryKey()) == null) {
				cacheResult(libro_Autore);
			}
			else {
				libro_Autore.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all libro_ autores.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(Libro_AutoreImpl.class.getName());
		}

		EntityCacheUtil.clearCache(Libro_AutoreImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the libro_ autore.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Libro_Autore libro_Autore) {
		EntityCacheUtil.removeResult(Libro_AutoreModelImpl.ENTITY_CACHE_ENABLED,
			Libro_AutoreImpl.class, libro_Autore.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Libro_Autore> libro_Autores) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Libro_Autore libro_Autore : libro_Autores) {
			EntityCacheUtil.removeResult(Libro_AutoreModelImpl.ENTITY_CACHE_ENABLED,
				Libro_AutoreImpl.class, libro_Autore.getPrimaryKey());
		}
	}

	/**
	 * Creates a new libro_ autore with the primary key. Does not add the libro_ autore to the database.
	 *
	 * @param libro_AutorePK the primary key for the new libro_ autore
	 * @return the new libro_ autore
	 */
	@Override
	public Libro_Autore create(Libro_AutorePK libro_AutorePK) {
		Libro_Autore libro_Autore = new Libro_AutoreImpl();

		libro_Autore.setNew(true);
		libro_Autore.setPrimaryKey(libro_AutorePK);

		return libro_Autore;
	}

	/**
	 * Removes the libro_ autore with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param libro_AutorePK the primary key of the libro_ autore
	 * @return the libro_ autore that was removed
	 * @throws it.planetek.liferay.service.builder.NoSuchLibro_AutoreException if a libro_ autore with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libro_Autore remove(Libro_AutorePK libro_AutorePK)
		throws NoSuchLibro_AutoreException, SystemException {
		return remove((Serializable)libro_AutorePK);
	}

	/**
	 * Removes the libro_ autore with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the libro_ autore
	 * @return the libro_ autore that was removed
	 * @throws it.planetek.liferay.service.builder.NoSuchLibro_AutoreException if a libro_ autore with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libro_Autore remove(Serializable primaryKey)
		throws NoSuchLibro_AutoreException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Libro_Autore libro_Autore = (Libro_Autore)session.get(Libro_AutoreImpl.class,
					primaryKey);

			if (libro_Autore == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLibro_AutoreException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(libro_Autore);
		}
		catch (NoSuchLibro_AutoreException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Libro_Autore removeImpl(Libro_Autore libro_Autore)
		throws SystemException {
		libro_Autore = toUnwrappedModel(libro_Autore);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(libro_Autore)) {
				libro_Autore = (Libro_Autore)session.get(Libro_AutoreImpl.class,
						libro_Autore.getPrimaryKeyObj());
			}

			if (libro_Autore != null) {
				session.delete(libro_Autore);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (libro_Autore != null) {
			clearCache(libro_Autore);
		}

		return libro_Autore;
	}

	@Override
	public Libro_Autore updateImpl(
		it.planetek.liferay.service.builder.model.Libro_Autore libro_Autore)
		throws SystemException {
		libro_Autore = toUnwrappedModel(libro_Autore);

		boolean isNew = libro_Autore.isNew();

		Session session = null;

		try {
			session = openSession();

			if (libro_Autore.isNew()) {
				session.save(libro_Autore);

				libro_Autore.setNew(false);
			}
			else {
				session.merge(libro_Autore);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(Libro_AutoreModelImpl.ENTITY_CACHE_ENABLED,
			Libro_AutoreImpl.class, libro_Autore.getPrimaryKey(), libro_Autore);

		return libro_Autore;
	}

	protected Libro_Autore toUnwrappedModel(Libro_Autore libro_Autore) {
		if (libro_Autore instanceof Libro_AutoreImpl) {
			return libro_Autore;
		}

		Libro_AutoreImpl libro_AutoreImpl = new Libro_AutoreImpl();

		libro_AutoreImpl.setNew(libro_Autore.isNew());
		libro_AutoreImpl.setPrimaryKey(libro_Autore.getPrimaryKey());

		libro_AutoreImpl.setLibroId(libro_Autore.getLibroId());
		libro_AutoreImpl.setAutoreId(libro_Autore.getAutoreId());

		return libro_AutoreImpl;
	}

	/**
	 * Returns the libro_ autore with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the libro_ autore
	 * @return the libro_ autore
	 * @throws it.planetek.liferay.service.builder.NoSuchLibro_AutoreException if a libro_ autore with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libro_Autore findByPrimaryKey(Serializable primaryKey)
		throws NoSuchLibro_AutoreException, SystemException {
		Libro_Autore libro_Autore = fetchByPrimaryKey(primaryKey);

		if (libro_Autore == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchLibro_AutoreException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return libro_Autore;
	}

	/**
	 * Returns the libro_ autore with the primary key or throws a {@link it.planetek.liferay.service.builder.NoSuchLibro_AutoreException} if it could not be found.
	 *
	 * @param libro_AutorePK the primary key of the libro_ autore
	 * @return the libro_ autore
	 * @throws it.planetek.liferay.service.builder.NoSuchLibro_AutoreException if a libro_ autore with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libro_Autore findByPrimaryKey(Libro_AutorePK libro_AutorePK)
		throws NoSuchLibro_AutoreException, SystemException {
		return findByPrimaryKey((Serializable)libro_AutorePK);
	}

	/**
	 * Returns the libro_ autore with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the libro_ autore
	 * @return the libro_ autore, or <code>null</code> if a libro_ autore with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libro_Autore fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Libro_Autore libro_Autore = (Libro_Autore)EntityCacheUtil.getResult(Libro_AutoreModelImpl.ENTITY_CACHE_ENABLED,
				Libro_AutoreImpl.class, primaryKey);

		if (libro_Autore == _nullLibro_Autore) {
			return null;
		}

		if (libro_Autore == null) {
			Session session = null;

			try {
				session = openSession();

				libro_Autore = (Libro_Autore)session.get(Libro_AutoreImpl.class,
						primaryKey);

				if (libro_Autore != null) {
					cacheResult(libro_Autore);
				}
				else {
					EntityCacheUtil.putResult(Libro_AutoreModelImpl.ENTITY_CACHE_ENABLED,
						Libro_AutoreImpl.class, primaryKey, _nullLibro_Autore);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(Libro_AutoreModelImpl.ENTITY_CACHE_ENABLED,
					Libro_AutoreImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return libro_Autore;
	}

	/**
	 * Returns the libro_ autore with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param libro_AutorePK the primary key of the libro_ autore
	 * @return the libro_ autore, or <code>null</code> if a libro_ autore with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Libro_Autore fetchByPrimaryKey(Libro_AutorePK libro_AutorePK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)libro_AutorePK);
	}

	/**
	 * Returns all the libro_ autores.
	 *
	 * @return the libro_ autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Libro_Autore> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the libro_ autores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.Libro_AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of libro_ autores
	 * @param end the upper bound of the range of libro_ autores (not inclusive)
	 * @return the range of libro_ autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Libro_Autore> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the libro_ autores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.Libro_AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of libro_ autores
	 * @param end the upper bound of the range of libro_ autores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of libro_ autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Libro_Autore> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Libro_Autore> list = (List<Libro_Autore>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_LIBRO_AUTORE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LIBRO_AUTORE;

				if (pagination) {
					sql = sql.concat(Libro_AutoreModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Libro_Autore>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Libro_Autore>(list);
				}
				else {
					list = (List<Libro_Autore>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the libro_ autores from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Libro_Autore libro_Autore : findAll()) {
			remove(libro_Autore);
		}
	}

	/**
	 * Returns the number of libro_ autores.
	 *
	 * @return the number of libro_ autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LIBRO_AUTORE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the libro_ autore persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.planetek.liferay.service.builder.model.Libro_Autore")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Libro_Autore>> listenersList = new ArrayList<ModelListener<Libro_Autore>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Libro_Autore>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(Libro_AutoreImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_LIBRO_AUTORE = "SELECT libro_Autore FROM Libro_Autore libro_Autore";
	private static final String _SQL_COUNT_LIBRO_AUTORE = "SELECT COUNT(libro_Autore) FROM Libro_Autore libro_Autore";
	private static final String _ORDER_BY_ENTITY_ALIAS = "libro_Autore.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Libro_Autore exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(Libro_AutorePersistenceImpl.class);
	private static Libro_Autore _nullLibro_Autore = new Libro_AutoreImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Libro_Autore> toCacheModel() {
				return _nullLibro_AutoreCacheModel;
			}
		};

	private static CacheModel<Libro_Autore> _nullLibro_AutoreCacheModel = new CacheModel<Libro_Autore>() {
			@Override
			public Libro_Autore toEntityModel() {
				return _nullLibro_Autore;
			}
		};
}