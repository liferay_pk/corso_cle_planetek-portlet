/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.planetek.liferay.service.builder.NoSuchAutoreException;
import it.planetek.liferay.service.builder.model.Autore;
import it.planetek.liferay.service.builder.model.impl.AutoreImpl;
import it.planetek.liferay.service.builder.model.impl.AutoreModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the autore service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author jfrancia
 * @see AutorePersistence
 * @see AutoreUtil
 * @generated
 */
public class AutorePersistenceImpl extends BasePersistenceImpl<Autore>
	implements AutorePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AutoreUtil} to access the autore persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AutoreImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AutoreModelImpl.ENTITY_CACHE_ENABLED,
			AutoreModelImpl.FINDER_CACHE_ENABLED, AutoreImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AutoreModelImpl.ENTITY_CACHE_ENABLED,
			AutoreModelImpl.FINDER_CACHE_ENABLED, AutoreImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AutoreModelImpl.ENTITY_CACHE_ENABLED,
			AutoreModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUP = new FinderPath(AutoreModelImpl.ENTITY_CACHE_ENABLED,
			AutoreModelImpl.FINDER_CACHE_ENABLED, AutoreImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroup",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUP = new FinderPath(AutoreModelImpl.ENTITY_CACHE_ENABLED,
			AutoreModelImpl.FINDER_CACHE_ENABLED, AutoreImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroup",
			new String[] { Long.class.getName() },
			AutoreModelImpl.GROUPID_COLUMN_BITMASK |
			AutoreModelImpl.COGNOME_COLUMN_BITMASK |
			AutoreModelImpl.NOME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUP = new FinderPath(AutoreModelImpl.ENTITY_CACHE_ENABLED,
			AutoreModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroup",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the autores where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Autore> findByGroup(long groupId) throws SystemException {
		return findByGroup(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the autores where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of autores
	 * @param end the upper bound of the range of autores (not inclusive)
	 * @return the range of matching autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Autore> findByGroup(long groupId, int start, int end)
		throws SystemException {
		return findByGroup(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the autores where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of autores
	 * @param end the upper bound of the range of autores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Autore> findByGroup(long groupId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUP;
			finderArgs = new Object[] { groupId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUP;
			finderArgs = new Object[] { groupId, start, end, orderByComparator };
		}

		List<Autore> list = (List<Autore>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Autore autore : list) {
				if ((groupId != autore.getGroupId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_AUTORE_WHERE);

			query.append(_FINDER_COLUMN_GROUP_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(AutoreModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (!pagination) {
					list = (List<Autore>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Autore>(list);
				}
				else {
					list = (List<Autore>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first autore in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching autore
	 * @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a matching autore could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore findByGroup_First(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchAutoreException, SystemException {
		Autore autore = fetchByGroup_First(groupId, orderByComparator);

		if (autore != null) {
			return autore;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAutoreException(msg.toString());
	}

	/**
	 * Returns the first autore in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching autore, or <code>null</code> if a matching autore could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore fetchByGroup_First(long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Autore> list = findByGroup(groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last autore in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching autore
	 * @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a matching autore could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore findByGroup_Last(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchAutoreException, SystemException {
		Autore autore = fetchByGroup_Last(groupId, orderByComparator);

		if (autore != null) {
			return autore;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAutoreException(msg.toString());
	}

	/**
	 * Returns the last autore in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching autore, or <code>null</code> if a matching autore could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore fetchByGroup_Last(long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByGroup(groupId);

		if (count == 0) {
			return null;
		}

		List<Autore> list = findByGroup(groupId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the autores before and after the current autore in the ordered set where groupId = &#63;.
	 *
	 * @param autoreId the primary key of the current autore
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next autore
	 * @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a autore with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore[] findByGroup_PrevAndNext(long autoreId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchAutoreException, SystemException {
		Autore autore = findByPrimaryKey(autoreId);

		Session session = null;

		try {
			session = openSession();

			Autore[] array = new AutoreImpl[3];

			array[0] = getByGroup_PrevAndNext(session, autore, groupId,
					orderByComparator, true);

			array[1] = autore;

			array[2] = getByGroup_PrevAndNext(session, autore, groupId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Autore getByGroup_PrevAndNext(Session session, Autore autore,
		long groupId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_AUTORE_WHERE);

		query.append(_FINDER_COLUMN_GROUP_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(AutoreModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(autore);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Autore> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the autores where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByGroup(long groupId) throws SystemException {
		for (Autore autore : findByGroup(groupId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(autore);
		}
	}

	/**
	 * Returns the number of autores where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByGroup(long groupId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GROUP;

		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_AUTORE_WHERE);

			query.append(_FINDER_COLUMN_GROUP_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUP_GROUPID_2 = "autore.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_G_C = new FinderPath(AutoreModelImpl.ENTITY_CACHE_ENABLED,
			AutoreModelImpl.FINDER_CACHE_ENABLED, AutoreImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByG_C",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_G_C = new FinderPath(AutoreModelImpl.ENTITY_CACHE_ENABLED,
			AutoreModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByG_C",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the autores where groupId = &#63; and cognome LIKE &#63;.
	 *
	 * @param groupId the group ID
	 * @param cognome the cognome
	 * @return the matching autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Autore> findByG_C(long groupId, String cognome)
		throws SystemException {
		return findByG_C(groupId, cognome, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the autores where groupId = &#63; and cognome LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param cognome the cognome
	 * @param start the lower bound of the range of autores
	 * @param end the upper bound of the range of autores (not inclusive)
	 * @return the range of matching autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Autore> findByG_C(long groupId, String cognome, int start,
		int end) throws SystemException {
		return findByG_C(groupId, cognome, start, end, null);
	}

	/**
	 * Returns an ordered range of all the autores where groupId = &#63; and cognome LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param cognome the cognome
	 * @param start the lower bound of the range of autores
	 * @param end the upper bound of the range of autores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Autore> findByG_C(long groupId, String cognome, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_G_C;
		finderArgs = new Object[] {
				groupId, cognome,
				
				start, end, orderByComparator
			};

		List<Autore> list = (List<Autore>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Autore autore : list) {
				if ((groupId != autore.getGroupId()) ||
						!StringUtil.wildcardMatches(autore.getCognome(),
							cognome, CharPool.UNDERLINE, CharPool.PERCENT,
							CharPool.BACK_SLASH, true)) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_AUTORE_WHERE);

			query.append(_FINDER_COLUMN_G_C_GROUPID_2);

			boolean bindCognome = false;

			if (cognome == null) {
				query.append(_FINDER_COLUMN_G_C_COGNOME_1);
			}
			else if (cognome.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_G_C_COGNOME_3);
			}
			else {
				bindCognome = true;

				query.append(_FINDER_COLUMN_G_C_COGNOME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(AutoreModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (bindCognome) {
					qPos.add(cognome);
				}

				if (!pagination) {
					list = (List<Autore>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Autore>(list);
				}
				else {
					list = (List<Autore>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first autore in the ordered set where groupId = &#63; and cognome LIKE &#63;.
	 *
	 * @param groupId the group ID
	 * @param cognome the cognome
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching autore
	 * @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a matching autore could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore findByG_C_First(long groupId, String cognome,
		OrderByComparator orderByComparator)
		throws NoSuchAutoreException, SystemException {
		Autore autore = fetchByG_C_First(groupId, cognome, orderByComparator);

		if (autore != null) {
			return autore;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(", cognome=");
		msg.append(cognome);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAutoreException(msg.toString());
	}

	/**
	 * Returns the first autore in the ordered set where groupId = &#63; and cognome LIKE &#63;.
	 *
	 * @param groupId the group ID
	 * @param cognome the cognome
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching autore, or <code>null</code> if a matching autore could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore fetchByG_C_First(long groupId, String cognome,
		OrderByComparator orderByComparator) throws SystemException {
		List<Autore> list = findByG_C(groupId, cognome, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last autore in the ordered set where groupId = &#63; and cognome LIKE &#63;.
	 *
	 * @param groupId the group ID
	 * @param cognome the cognome
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching autore
	 * @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a matching autore could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore findByG_C_Last(long groupId, String cognome,
		OrderByComparator orderByComparator)
		throws NoSuchAutoreException, SystemException {
		Autore autore = fetchByG_C_Last(groupId, cognome, orderByComparator);

		if (autore != null) {
			return autore;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(", cognome=");
		msg.append(cognome);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAutoreException(msg.toString());
	}

	/**
	 * Returns the last autore in the ordered set where groupId = &#63; and cognome LIKE &#63;.
	 *
	 * @param groupId the group ID
	 * @param cognome the cognome
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching autore, or <code>null</code> if a matching autore could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore fetchByG_C_Last(long groupId, String cognome,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByG_C(groupId, cognome);

		if (count == 0) {
			return null;
		}

		List<Autore> list = findByG_C(groupId, cognome, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the autores before and after the current autore in the ordered set where groupId = &#63; and cognome LIKE &#63;.
	 *
	 * @param autoreId the primary key of the current autore
	 * @param groupId the group ID
	 * @param cognome the cognome
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next autore
	 * @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a autore with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore[] findByG_C_PrevAndNext(long autoreId, long groupId,
		String cognome, OrderByComparator orderByComparator)
		throws NoSuchAutoreException, SystemException {
		Autore autore = findByPrimaryKey(autoreId);

		Session session = null;

		try {
			session = openSession();

			Autore[] array = new AutoreImpl[3];

			array[0] = getByG_C_PrevAndNext(session, autore, groupId, cognome,
					orderByComparator, true);

			array[1] = autore;

			array[2] = getByG_C_PrevAndNext(session, autore, groupId, cognome,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Autore getByG_C_PrevAndNext(Session session, Autore autore,
		long groupId, String cognome, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_AUTORE_WHERE);

		query.append(_FINDER_COLUMN_G_C_GROUPID_2);

		boolean bindCognome = false;

		if (cognome == null) {
			query.append(_FINDER_COLUMN_G_C_COGNOME_1);
		}
		else if (cognome.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_G_C_COGNOME_3);
		}
		else {
			bindCognome = true;

			query.append(_FINDER_COLUMN_G_C_COGNOME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(AutoreModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (bindCognome) {
			qPos.add(cognome);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(autore);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Autore> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the autores where groupId = &#63; and cognome LIKE &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param cognome the cognome
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByG_C(long groupId, String cognome)
		throws SystemException {
		for (Autore autore : findByG_C(groupId, cognome, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(autore);
		}
	}

	/**
	 * Returns the number of autores where groupId = &#63; and cognome LIKE &#63;.
	 *
	 * @param groupId the group ID
	 * @param cognome the cognome
	 * @return the number of matching autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByG_C(long groupId, String cognome)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_G_C;

		Object[] finderArgs = new Object[] { groupId, cognome };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_AUTORE_WHERE);

			query.append(_FINDER_COLUMN_G_C_GROUPID_2);

			boolean bindCognome = false;

			if (cognome == null) {
				query.append(_FINDER_COLUMN_G_C_COGNOME_1);
			}
			else if (cognome.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_G_C_COGNOME_3);
			}
			else {
				bindCognome = true;

				query.append(_FINDER_COLUMN_G_C_COGNOME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (bindCognome) {
					qPos.add(cognome);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_G_C_GROUPID_2 = "autore.groupId = ? AND ";
	private static final String _FINDER_COLUMN_G_C_COGNOME_1 = "autore.cognome LIKE NULL";
	private static final String _FINDER_COLUMN_G_C_COGNOME_2 = "autore.cognome LIKE ?";
	private static final String _FINDER_COLUMN_G_C_COGNOME_3 = "(autore.cognome IS NULL OR autore.cognome LIKE '')";

	public AutorePersistenceImpl() {
		setModelClass(Autore.class);
	}

	/**
	 * Caches the autore in the entity cache if it is enabled.
	 *
	 * @param autore the autore
	 */
	@Override
	public void cacheResult(Autore autore) {
		EntityCacheUtil.putResult(AutoreModelImpl.ENTITY_CACHE_ENABLED,
			AutoreImpl.class, autore.getPrimaryKey(), autore);

		autore.resetOriginalValues();
	}

	/**
	 * Caches the autores in the entity cache if it is enabled.
	 *
	 * @param autores the autores
	 */
	@Override
	public void cacheResult(List<Autore> autores) {
		for (Autore autore : autores) {
			if (EntityCacheUtil.getResult(
						AutoreModelImpl.ENTITY_CACHE_ENABLED, AutoreImpl.class,
						autore.getPrimaryKey()) == null) {
				cacheResult(autore);
			}
			else {
				autore.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all autores.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AutoreImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AutoreImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the autore.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Autore autore) {
		EntityCacheUtil.removeResult(AutoreModelImpl.ENTITY_CACHE_ENABLED,
			AutoreImpl.class, autore.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Autore> autores) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Autore autore : autores) {
			EntityCacheUtil.removeResult(AutoreModelImpl.ENTITY_CACHE_ENABLED,
				AutoreImpl.class, autore.getPrimaryKey());
		}
	}

	/**
	 * Creates a new autore with the primary key. Does not add the autore to the database.
	 *
	 * @param autoreId the primary key for the new autore
	 * @return the new autore
	 */
	@Override
	public Autore create(long autoreId) {
		Autore autore = new AutoreImpl();

		autore.setNew(true);
		autore.setPrimaryKey(autoreId);

		return autore;
	}

	/**
	 * Removes the autore with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param autoreId the primary key of the autore
	 * @return the autore that was removed
	 * @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a autore with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore remove(long autoreId)
		throws NoSuchAutoreException, SystemException {
		return remove((Serializable)autoreId);
	}

	/**
	 * Removes the autore with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the autore
	 * @return the autore that was removed
	 * @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a autore with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore remove(Serializable primaryKey)
		throws NoSuchAutoreException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Autore autore = (Autore)session.get(AutoreImpl.class, primaryKey);

			if (autore == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAutoreException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(autore);
		}
		catch (NoSuchAutoreException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Autore removeImpl(Autore autore) throws SystemException {
		autore = toUnwrappedModel(autore);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(autore)) {
				autore = (Autore)session.get(AutoreImpl.class,
						autore.getPrimaryKeyObj());
			}

			if (autore != null) {
				session.delete(autore);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (autore != null) {
			clearCache(autore);
		}

		return autore;
	}

	@Override
	public Autore updateImpl(
		it.planetek.liferay.service.builder.model.Autore autore)
		throws SystemException {
		autore = toUnwrappedModel(autore);

		boolean isNew = autore.isNew();

		AutoreModelImpl autoreModelImpl = (AutoreModelImpl)autore;

		Session session = null;

		try {
			session = openSession();

			if (autore.isNew()) {
				session.save(autore);

				autore.setNew(false);
			}
			else {
				session.merge(autore);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !AutoreModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((autoreModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUP.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						autoreModelImpl.getOriginalGroupId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUP, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUP,
					args);

				args = new Object[] { autoreModelImpl.getGroupId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUP, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUP,
					args);
			}
		}

		EntityCacheUtil.putResult(AutoreModelImpl.ENTITY_CACHE_ENABLED,
			AutoreImpl.class, autore.getPrimaryKey(), autore);

		return autore;
	}

	protected Autore toUnwrappedModel(Autore autore) {
		if (autore instanceof AutoreImpl) {
			return autore;
		}

		AutoreImpl autoreImpl = new AutoreImpl();

		autoreImpl.setNew(autore.isNew());
		autoreImpl.setPrimaryKey(autore.getPrimaryKey());

		autoreImpl.setAutoreId(autore.getAutoreId());
		autoreImpl.setCompanyId(autore.getCompanyId());
		autoreImpl.setGroupId(autore.getGroupId());
		autoreImpl.setUserId(autore.getUserId());
		autoreImpl.setUserName(autore.getUserName());
		autoreImpl.setCreateDate(autore.getCreateDate());
		autoreImpl.setModifiedDate(autore.getModifiedDate());
		autoreImpl.setNome(autore.getNome());
		autoreImpl.setCognome(autore.getCognome());

		return autoreImpl;
	}

	/**
	 * Returns the autore with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the autore
	 * @return the autore
	 * @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a autore with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore findByPrimaryKey(Serializable primaryKey)
		throws NoSuchAutoreException, SystemException {
		Autore autore = fetchByPrimaryKey(primaryKey);

		if (autore == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchAutoreException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return autore;
	}

	/**
	 * Returns the autore with the primary key or throws a {@link it.planetek.liferay.service.builder.NoSuchAutoreException} if it could not be found.
	 *
	 * @param autoreId the primary key of the autore
	 * @return the autore
	 * @throws it.planetek.liferay.service.builder.NoSuchAutoreException if a autore with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore findByPrimaryKey(long autoreId)
		throws NoSuchAutoreException, SystemException {
		return findByPrimaryKey((Serializable)autoreId);
	}

	/**
	 * Returns the autore with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the autore
	 * @return the autore, or <code>null</code> if a autore with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Autore autore = (Autore)EntityCacheUtil.getResult(AutoreModelImpl.ENTITY_CACHE_ENABLED,
				AutoreImpl.class, primaryKey);

		if (autore == _nullAutore) {
			return null;
		}

		if (autore == null) {
			Session session = null;

			try {
				session = openSession();

				autore = (Autore)session.get(AutoreImpl.class, primaryKey);

				if (autore != null) {
					cacheResult(autore);
				}
				else {
					EntityCacheUtil.putResult(AutoreModelImpl.ENTITY_CACHE_ENABLED,
						AutoreImpl.class, primaryKey, _nullAutore);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(AutoreModelImpl.ENTITY_CACHE_ENABLED,
					AutoreImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return autore;
	}

	/**
	 * Returns the autore with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param autoreId the primary key of the autore
	 * @return the autore, or <code>null</code> if a autore with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Autore fetchByPrimaryKey(long autoreId) throws SystemException {
		return fetchByPrimaryKey((Serializable)autoreId);
	}

	/**
	 * Returns all the autores.
	 *
	 * @return the autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Autore> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the autores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of autores
	 * @param end the upper bound of the range of autores (not inclusive)
	 * @return the range of autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Autore> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the autores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.planetek.liferay.service.builder.model.impl.AutoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of autores
	 * @param end the upper bound of the range of autores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Autore> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Autore> list = (List<Autore>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_AUTORE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_AUTORE;

				if (pagination) {
					sql = sql.concat(AutoreModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Autore>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Autore>(list);
				}
				else {
					list = (List<Autore>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the autores from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Autore autore : findAll()) {
			remove(autore);
		}
	}

	/**
	 * Returns the number of autores.
	 *
	 * @return the number of autores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_AUTORE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the autore persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.planetek.liferay.service.builder.model.Autore")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Autore>> listenersList = new ArrayList<ModelListener<Autore>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Autore>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AutoreImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_AUTORE = "SELECT autore FROM Autore autore";
	private static final String _SQL_SELECT_AUTORE_WHERE = "SELECT autore FROM Autore autore WHERE ";
	private static final String _SQL_COUNT_AUTORE = "SELECT COUNT(autore) FROM Autore autore";
	private static final String _SQL_COUNT_AUTORE_WHERE = "SELECT COUNT(autore) FROM Autore autore WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "autore.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Autore exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Autore exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AutorePersistenceImpl.class);
	private static Autore _nullAutore = new AutoreImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Autore> toCacheModel() {
				return _nullAutoreCacheModel;
			}
		};

	private static CacheModel<Autore> _nullAutoreCacheModel = new CacheModel<Autore>() {
			@Override
			public Autore toEntityModel() {
				return _nullAutore;
			}
		};
}