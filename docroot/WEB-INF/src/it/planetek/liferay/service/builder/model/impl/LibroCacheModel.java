/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.planetek.liferay.service.builder.model.Libro;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Libro in entity cache.
 *
 * @author jfrancia
 * @see Libro
 * @generated
 */
public class LibroCacheModel implements CacheModel<Libro>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(37);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", libroId=");
		sb.append(libroId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", nome=");
		sb.append(nome);
		sb.append(", pubblicato=");
		sb.append(pubblicato);
		sb.append(", numeroPagine=");
		sb.append(numeroPagine);
		sb.append(", dataPubblicazione=");
		sb.append(dataPubblicazione);
		sb.append(", descrizione=");
		sb.append(descrizione);
		sb.append(", isbn=");
		sb.append(isbn);
		sb.append(", status=");
		sb.append(status);
		sb.append(", statusByUserId=");
		sb.append(statusByUserId);
		sb.append(", statusByUserName=");
		sb.append(statusByUserName);
		sb.append(", statusDate=");
		sb.append(statusDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Libro toEntityModel() {
		LibroImpl libroImpl = new LibroImpl();

		if (uuid == null) {
			libroImpl.setUuid(StringPool.BLANK);
		}
		else {
			libroImpl.setUuid(uuid);
		}

		libroImpl.setLibroId(libroId);
		libroImpl.setCompanyId(companyId);
		libroImpl.setGroupId(groupId);
		libroImpl.setUserId(userId);

		if (userName == null) {
			libroImpl.setUserName(StringPool.BLANK);
		}
		else {
			libroImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			libroImpl.setCreateDate(null);
		}
		else {
			libroImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			libroImpl.setModifiedDate(null);
		}
		else {
			libroImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (nome == null) {
			libroImpl.setNome(StringPool.BLANK);
		}
		else {
			libroImpl.setNome(nome);
		}

		libroImpl.setPubblicato(pubblicato);
		libroImpl.setNumeroPagine(numeroPagine);

		if (dataPubblicazione == Long.MIN_VALUE) {
			libroImpl.setDataPubblicazione(null);
		}
		else {
			libroImpl.setDataPubblicazione(new Date(dataPubblicazione));
		}

		if (descrizione == null) {
			libroImpl.setDescrizione(StringPool.BLANK);
		}
		else {
			libroImpl.setDescrizione(descrizione);
		}

		if (isbn == null) {
			libroImpl.setIsbn(StringPool.BLANK);
		}
		else {
			libroImpl.setIsbn(isbn);
		}

		libroImpl.setStatus(status);
		libroImpl.setStatusByUserId(statusByUserId);

		if (statusByUserName == null) {
			libroImpl.setStatusByUserName(StringPool.BLANK);
		}
		else {
			libroImpl.setStatusByUserName(statusByUserName);
		}

		if (statusDate == Long.MIN_VALUE) {
			libroImpl.setStatusDate(null);
		}
		else {
			libroImpl.setStatusDate(new Date(statusDate));
		}

		libroImpl.resetOriginalValues();

		return libroImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();
		libroId = objectInput.readLong();
		companyId = objectInput.readLong();
		groupId = objectInput.readLong();
		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		nome = objectInput.readUTF();
		pubblicato = objectInput.readBoolean();
		numeroPagine = objectInput.readInt();
		dataPubblicazione = objectInput.readLong();
		descrizione = objectInput.readUTF();
		isbn = objectInput.readUTF();
		status = objectInput.readInt();
		statusByUserId = objectInput.readLong();
		statusByUserName = objectInput.readUTF();
		statusDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(libroId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (nome == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nome);
		}

		objectOutput.writeBoolean(pubblicato);
		objectOutput.writeInt(numeroPagine);
		objectOutput.writeLong(dataPubblicazione);

		if (descrizione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizione);
		}

		if (isbn == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(isbn);
		}

		objectOutput.writeInt(status);
		objectOutput.writeLong(statusByUserId);

		if (statusByUserName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(statusByUserName);
		}

		objectOutput.writeLong(statusDate);
	}

	public String uuid;
	public long libroId;
	public long companyId;
	public long groupId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String nome;
	public boolean pubblicato;
	public int numeroPagine;
	public long dataPubblicazione;
	public String descrizione;
	public String isbn;
	public int status;
	public long statusByUserId;
	public String statusByUserName;
	public long statusDate;
}