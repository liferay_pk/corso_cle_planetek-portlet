/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import it.planetek.liferay.service.builder.model.Libro_Autore;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Libro_Autore in entity cache.
 *
 * @author jfrancia
 * @see Libro_Autore
 * @generated
 */
public class Libro_AutoreCacheModel implements CacheModel<Libro_Autore>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{libroId=");
		sb.append(libroId);
		sb.append(", autoreId=");
		sb.append(autoreId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Libro_Autore toEntityModel() {
		Libro_AutoreImpl libro_AutoreImpl = new Libro_AutoreImpl();

		libro_AutoreImpl.setLibroId(libroId);
		libro_AutoreImpl.setAutoreId(autoreId);

		libro_AutoreImpl.resetOriginalValues();

		return libro_AutoreImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		libroId = objectInput.readLong();
		autoreId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(libroId);
		objectOutput.writeLong(autoreId);
	}

	public long libroId;
	public long autoreId;
}