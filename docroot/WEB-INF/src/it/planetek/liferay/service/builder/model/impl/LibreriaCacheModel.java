/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.planetek.liferay.service.builder.model.Libreria;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Libreria in entity cache.
 *
 * @author jfrancia
 * @see Libreria
 * @generated
 */
public class LibreriaCacheModel implements CacheModel<Libreria>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{libreriaId=");
		sb.append(libreriaId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", nome=");
		sb.append(nome);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Libreria toEntityModel() {
		LibreriaImpl libreriaImpl = new LibreriaImpl();

		libreriaImpl.setLibreriaId(libreriaId);
		libreriaImpl.setCompanyId(companyId);
		libreriaImpl.setGroupId(groupId);
		libreriaImpl.setUserId(userId);

		if (userName == null) {
			libreriaImpl.setUserName(StringPool.BLANK);
		}
		else {
			libreriaImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			libreriaImpl.setCreateDate(null);
		}
		else {
			libreriaImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			libreriaImpl.setModifiedDate(null);
		}
		else {
			libreriaImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (nome == null) {
			libreriaImpl.setNome(StringPool.BLANK);
		}
		else {
			libreriaImpl.setNome(nome);
		}

		libreriaImpl.resetOriginalValues();

		return libreriaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		libreriaId = objectInput.readLong();
		companyId = objectInput.readLong();
		groupId = objectInput.readLong();
		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		nome = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(libreriaId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (nome == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nome);
		}
	}

	public long libreriaId;
	public long companyId;
	public long groupId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String nome;
}