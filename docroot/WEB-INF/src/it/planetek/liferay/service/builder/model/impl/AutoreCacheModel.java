/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.planetek.liferay.service.builder.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.planetek.liferay.service.builder.model.Autore;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Autore in entity cache.
 *
 * @author jfrancia
 * @see Autore
 * @generated
 */
public class AutoreCacheModel implements CacheModel<Autore>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{autoreId=");
		sb.append(autoreId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", nome=");
		sb.append(nome);
		sb.append(", cognome=");
		sb.append(cognome);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Autore toEntityModel() {
		AutoreImpl autoreImpl = new AutoreImpl();

		autoreImpl.setAutoreId(autoreId);
		autoreImpl.setCompanyId(companyId);
		autoreImpl.setGroupId(groupId);
		autoreImpl.setUserId(userId);

		if (userName == null) {
			autoreImpl.setUserName(StringPool.BLANK);
		}
		else {
			autoreImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			autoreImpl.setCreateDate(null);
		}
		else {
			autoreImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			autoreImpl.setModifiedDate(null);
		}
		else {
			autoreImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (nome == null) {
			autoreImpl.setNome(StringPool.BLANK);
		}
		else {
			autoreImpl.setNome(nome);
		}

		if (cognome == null) {
			autoreImpl.setCognome(StringPool.BLANK);
		}
		else {
			autoreImpl.setCognome(cognome);
		}

		autoreImpl.resetOriginalValues();

		return autoreImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		autoreId = objectInput.readLong();
		companyId = objectInput.readLong();
		groupId = objectInput.readLong();
		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		nome = objectInput.readUTF();
		cognome = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(autoreId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (nome == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nome);
		}

		if (cognome == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(cognome);
		}
	}

	public long autoreId;
	public long companyId;
	public long groupId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String nome;
	public String cognome;
}