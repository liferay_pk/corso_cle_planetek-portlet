package it.planetek.liferay.indexer;

import it.planetek.liferay.service.builder.model.Libro;
import it.planetek.liferay.service.builder.service.LibroLocalServiceUtil;
import it.planetek.liferay.service.builder.service.persistence.LibroActionableDynamicQuery;

import java.util.Locale;

import javax.portlet.PortletURL;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.BaseIndexer;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.DocumentImpl;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchEngineUtil;
import com.liferay.portal.kernel.search.Summary;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

public class LibroIndexer extends BaseIndexer {

	public static final String[] CLASS_NAMES = {Libro.class.getName()};
	public static final String PORTLET_ID = "libro_WAR_corso_cle_planetekportlet";


	@Override
	public String[] getClassNames() {
		return CLASS_NAMES;
	}

	@Override
	public String getPortletId() {
		return PORTLET_ID;
	}

	@Override
	protected void doDelete(Object obj) throws Exception {
		Libro entry = (Libro) obj;
		Document document = new DocumentImpl();
		document.addUID(PORTLET_ID, entry.getPrimaryKey());
		SearchEngineUtil.deleteDocument(getSearchEngineId(), entry.getCompanyId(), document.get(Field.UID));
	}

	@Override
	protected Document doGetDocument(Object obj) throws Exception {
		Libro entry = (Libro) obj;
		Document document = getBaseModelDocument(getPortletId(), entry);
		document.addUID(PORTLET_ID, entry.getPrimaryKey(), Libro.class.getName());
		
		//imposto esplicitamente dei campi, li utilizzera' per effettuere la ricerca
		document.addText(Field.TITLE, entry.getNome());
		document.addText("nome", entry.getNome());
		document.addText("pubblicato", "" + entry.isPubblicato());
		document.addText("numeroPagine", "" + entry.getNumeroPagine());
		document.addDate("dataPubblicazione", entry.getDataPubblicazione());
		document.addText("descrizione", entry.getDescrizione());
		document.addText(Field.DESCRIPTION, entry.getDescrizione());
		document.addText(Field.CONTENT, entry.getDescrizione());		
		document.addText("isbn", entry.getIsbn());
		
		return document;
	}

	@Override
	protected Summary doGetSummary(Document document, Locale locale,
			String snippet, PortletURL portletURL) throws Exception {
		String title = document.get(Field.TITLE);
		String content = snippet;
		if (Validator.isNull(snippet)) {
			content = document.get(Field.DESCRIPTION);
			if (Validator.isNull(content)) {
				content = StringUtil.shorten(document.get(Field.CONTENT), 200);
			}
		}
		String resourcePrimKey = document.get(Field.ENTRY_CLASS_PK);
//		//da modificare quando si gestira' la pubblicazione degli asset tramite asset publisher
//		portletURL.setParameter("mvcPath", "/html/issues/view_issue.jsp");
		portletURL.setParameter("resourcePrimKey", resourcePrimKey);
		return new Summary(title, content, portletURL);
	}

	@Override
	protected void doReindex(Object obj) throws Exception {
		Libro entry = (Libro) obj;
		Document document = getDocument(entry);
		if (!entry.isApproved()) {
			return;
		}

		SearchEngineUtil.updateDocument(getSearchEngineId(), entry.getCompanyId(), document);
	}

	@Override
	protected void doReindex(String className, long classPK) throws Exception {
		Libro entry = LibroLocalServiceUtil.getLibro(classPK);
		if (!entry.isApproved()) {
			 return;
		}
		doReindex(entry);
	}

	@Override
	protected void doReindex(String[] ids) throws Exception {
		for (String companyIdString : ids) {
			long companyId = GetterUtil.getLong(companyIdString);
			reIndexEntry(companyId);			
		}
	}

	protected void reIndexEntry(long companyId) throws Exception {
		ActionableDynamicQuery actionableDynamicQuery = new LibroActionableDynamicQuery() {

			@Override
			protected void addCriteria(DynamicQuery dynamicQuery) {
				// Aggiungere solo se ci sono i campi del workflow
				Property statusProperty = PropertyFactoryUtil.forName("status");

				Integer[] statuses = { WorkflowConstants.STATUS_APPROVED,
						WorkflowConstants.STATUS_IN_TRASH };

				dynamicQuery.add(statusProperty.in(statuses));
			}

			@Override
			protected void performAction(Object object) throws PortalException {
				Libro entry = (Libro) object;

				Document document = getDocument(entry);

				if (document != null) {
					addDocument(document);
				}
			}
		};
		actionableDynamicQuery.setCompanyId(companyId);
		actionableDynamicQuery.setSearchEngineId(getSearchEngineId());
		actionableDynamicQuery.performActions();
	}


	@Override
	protected String getPortletId(SearchContext searchContext) {
		return PORTLET_ID;
	}

}
