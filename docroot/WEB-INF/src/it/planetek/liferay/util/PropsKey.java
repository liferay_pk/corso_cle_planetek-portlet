package it.planetek.liferay.util;

public interface PropsKey extends com.liferay.portal.kernel.util.PropsKeys {

	public static final String MY_CUSTOM_PROPERTY = "it.planetek.my.custom.property";
	
}
