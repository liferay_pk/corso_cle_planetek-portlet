package it.planetek.liferay.custom.attributes;

import it.planetek.liferay.service.builder.model.Libro;

import com.liferay.portlet.expando.model.BaseCustomAttributesDisplay;

public class LibroCustomAttributesDisplay extends BaseCustomAttributesDisplay {

	@Override
	public String getClassName() {
		return Libro.class.getName();
	}

}
