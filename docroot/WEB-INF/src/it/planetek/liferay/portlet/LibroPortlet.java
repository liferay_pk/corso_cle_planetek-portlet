package it.planetek.liferay.portlet;

import it.planetek.liferay.service.builder.model.Libro;
import it.planetek.liferay.service.builder.service.LibroLocalServiceUtil;
import it.planetek.liferay.service.builder.service.LibroServiceUtil;
import it.planetek.liferay.service.builder.service.TaskServiceUtil;
import it.planetek.liferay.util.WebKeys;

import java.util.Date;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class LibroPortlet
 */
public class LibroPortlet extends MVCPortlet {

	protected String editLibroJSP = "/html/libro/edit_entry.jsp";

	public void editEntry(ActionRequest actionRequest,
			ActionResponse actionResponse) throws PortalException, SystemException {
		long entryId = ParamUtil.getLong(actionRequest, "entryId");
		Libro libro = null;
		
		try {
			if (Validator.isNotNull(entryId)) {
				libro = LibroLocalServiceUtil.getLibro(entryId);
			}
		} catch(PortalException e) {
			actionResponse.setRenderParameter("mvcPath", viewTemplate);
			throw e;
		} catch(SystemException e) {
			actionResponse.setRenderParameter("mvcPath", viewTemplate);
			throw e;
		}

		actionRequest.setAttribute(WebKeys.LIBRO_DEFINITION, libro);
		actionResponse.setRenderParameter("mvcPath", editLibroJSP);

	}

	public void insertUpdateEntry(ActionRequest actionRequest,
			ActionResponse actionResponse) throws Exception {
		long entryId = ParamUtil.getLong(actionRequest, "entryId");
		long userId = PortalUtil.getUserId(actionRequest);

		String nome = ParamUtil.getString(actionRequest, "nome");
		boolean pubblicato = ParamUtil.getBoolean(actionRequest, "pubblicato");
		int numeroPagine = ParamUtil.getInteger(actionRequest, "numeroPagine");
		Date dataPubblicazione = ParamUtil.getDate(actionRequest, "dataPubblicazione", 
				DateFormatFactoryUtil.getDateTime(actionRequest.getLocale()));
		String descrizione = ParamUtil.getString(actionRequest, "descrizione");
		String isbn = ParamUtil.getString(actionRequest, "isbn");

		ServiceContext serviceContext = 
				ServiceContextFactory.getInstance(Libro.class.getName(), actionRequest);

		Libro entry = null;

		try {
			if (entryId <= 0) {
				entry = 
						LibroServiceUtil.addLibro(userId, nome, pubblicato,
								numeroPagine, dataPubblicazione, descrizione, isbn, serviceContext);
				SessionMessages.add(actionRequest, "the-selected-entry-has-been-added");
			} else {
				entry =
						LibroLocalServiceUtil.updateLibro(entryId, nome, pubblicato,
								numeroPagine, dataPubblicazione, descrizione, isbn, serviceContext);								
				SessionMessages.add(actionRequest, "the-selected-entry-has-been-updated");
			}
			actionRequest.setAttribute(WebKeys.LIBRO_DEFINITION, entry);
			actionResponse.setRenderParameter("mvcPath", viewTemplate);
		} catch (PortalException e) {
			actionResponse.setRenderParameter("mvcPath", editLibroJSP);			
			throw e;
		}
	}	

	public void deleteEntry(ActionRequest actionRequest,
			ActionResponse actionResponse) throws Exception {
		long entryId = ParamUtil.getLong(actionRequest, "entryId");

		try {
			LibroLocalServiceUtil.deleteLibro(entryId);
		} catch(Exception e) {
			actionResponse.setRenderParameter("mvcPath", viewTemplate);
			throw e;
		}

		SessionMessages.add(actionRequest,
				"the-selected-entry-has-been-deleted");
		actionResponse.setRenderParameter("mvcPath", viewTemplate);
	}

	public void editLibro(ActionRequest actionRequest,
			ActionResponse actionResponse) throws PortalException, SystemException {
		long entryId = ParamUtil.getLong(actionRequest, "entryId");
		Libro entry = null;

		try {
			if (Validator.isNotNull(entryId)) {
				entry = LibroLocalServiceUtil.getLibro(entryId);
			}
		} catch(PortalException e) {
			actionResponse.setRenderParameter("mvcPath", viewTemplate);
			throw e;
		} catch(SystemException e) {
			actionResponse.setRenderParameter("mvcPath", viewTemplate);
			throw e;
		}
		actionRequest.setAttribute(WebKeys.LIBRO_DEFINITION, entry);
		actionResponse.setRenderParameter("mvcPath", editLibroJSP);

	}
	
}