package it.planetek.liferay.hooks.listeners;

import com.liferay.portal.ModelListenerException;
import com.liferay.portal.model.BaseModelListener;
import com.liferay.portal.model.User;

public class UserModelListener extends BaseModelListener<User> {

	@Override
	public void onAfterCreate(User model) throws ModelListenerException {
		System.out.println("Utente creato!!");
	}

	@Override
	public void onAfterRemove(User model) throws ModelListenerException {
		System.out.println("Utente cancellato!!");
	}

}
