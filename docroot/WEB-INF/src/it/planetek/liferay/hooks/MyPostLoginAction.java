package it.planetek.liferay.hooks;

import com.liferay.portal.kernel.events.Action;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.liferay.portal.kernel.events.ActionException;

public class MyPostLoginAction extends Action {
	/* (non-Java-doc)
	 * @see com.liferay.portal.kernel.events.Action#Action()
	 */
	public MyPostLoginAction() {
		super();
	}

	/* (non-Java-doc)
	 * @see com.liferay.portal.kernel.events.Action#run(HttpServletRequest request, HttpServletResponse response)
	 */
	public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException {
		System.out.println("Funziona! Sono chiamato post login!");
	}

}